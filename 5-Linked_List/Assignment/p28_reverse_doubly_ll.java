/*
    reverse Doubly linked list
 */

import java.util.*;

class Node {

    int data;
    Node prev = null;
    Node next = null;

    Node(int data) {
        this.data = data;
    }
}

class DoublyLL {

    Node head = null;

    void addLast(int data) {

        Node newNode = new Node(data);
        if(head == null) {
            head = newNode;
            return;
        }

        Node temp = head;
        while(temp.next != null) {
            temp = temp.next;
        }

        newNode.prev = temp;
        temp.next = newNode;
    }

    void printLL() {

        if(head == null) {

            System.out.println("\nLinked list is empty...!!");
            return;
        }

        System.out.println();
        Node temp = head;

        while(temp != null) {
            System.out.print(temp.data + " -> ");
            temp = temp.next;
        }

        System.out.println("null");
    }

    int countNode() {

        int cnt = 0;
        Node temp = head;

        while(temp != null) {

            cnt++;
            temp = temp.next;
        }

        return cnt;
    }

    void reverseLL() {

        if(head == null)
            return;

        int cnt = countNode();

        Node end = head;
        while(end.next != null) {

            end = end.next;
        }
        
        int i = 1;
        Node start = head;
        while(i <= (cnt+1)/2) {

            int temp = start.data;
            start.data = end.data;
            end.data = temp;

            start = start.next;
            end = end.prev;
            i++;
        }
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        DoublyLL dll = new DoublyLL();

        boolean flag = true;

        do {

            System.out.println("\n1 : Add Nodes in LL");
            System.out.println("2 : Print LL");
            System.out.println("3 : Reverse LL");
            System.out.println("4 : Exit");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1: {
                    System.out.print("\nEnter data :: ");
                    int data = sc.nextInt();

                    dll.addLast(data);
                }
                    break;

                case 2:
                    dll.printLL();
                    break;

                case 3:
                    dll.reverseLL();
                    break;

                case 4:
                    flag = false;
                    break;

                default:
                    System.out.println("\nWrong Choice ...!!");
                    break;
            }

        } while (flag);

        sc.close();
    }
}