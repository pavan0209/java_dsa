/*
    13. Nth node from end of linked list
    
        Given a linked list consisting of L nodes and given a number N. The task is to find the Nth node
    from the end of the linked list.
    
    Example 1:
    Input:
        N = 2
        LinkedList: 1->2->3->4->5->6->7->8->9
    Output: 8
    Explanation: 
        In the first example, there are 9 nodes in the linked list and we need to find the 2nd node from the
        end. the 2nd node from the end is 8.

    Example 2:
    Input:
        N = 5
        LinkedList: 10->5->100->5
    Output: -1
    Explanation: 
        In the second example, there are 4 nodes in the linked list and we need to find 5th from the end. Since
        'n' is more than the number of nodes in the linked list, the output is -1.
    
    Note:
    Try to solve it in a single traversal.
    
    Expected Time Complexity: O(N).
    Expected Auxiliary Space: O(1).
    
    Constraints:
        1 <= L <= 10^6
        1 <= N <= 10^6
 */

import java.util.Scanner; 

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {

        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {
            
            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }
    }

    int countNode() {

        int nodeCnt = 0;
        Node temp = head;

        while(temp != null) {

            nodeCnt++;
            temp = temp.next;
        }

        return nodeCnt;
    }

    int nthNodeEnd(int N) {

        int cnt = countNode();

        if(N > cnt) {
            return -1;
        }
        else {
            Node temp = head;
            int i = 1;

            while(i <= cnt - N) {

                i++;
                temp = temp.next;
            }
            return temp.data;
        }
    }

    void printLL() {

        if(head == null) {

            System.out.println("\nEmpty Linked List...!!");
        }
        else {

            Node temp = head;

            while(temp != null) {

                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }

            System.out.println("null");
        }
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';
        
        do {

            System.out.println("\n1 : AddNode");
            System.out.println("2 : CountNodes");
            System.out.println("3 : Print Linked List");
            System.out.println("4 : Nth node from end of linked list");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }
                        break;

                case 2 : 
                        System.out.println("\nTotal Number of Nodes :: " + ll.countNode());
                        break;

                case 3 :
                        ll.printLL();
                        break;

                case 4 : 
                        {   
                            System.out.print("\nEnter value for N :: ");
                            int N = sc.nextInt();

                            int ret = ll.nthNodeEnd(N);
                            System.out.println("Nth Node from end :: " + ret);
                        }
                        break;

                default :
                        System.out.println("\nInvalid Choice ...!!");
            }

            System.out.print("\nDo you want to continue ?? : ");
            ch = sc.next().charAt(0);

        }while(ch == 'y' || ch == 'Y');

        sc.close();
    }
}