/*
    16. Rotate a Linked List

        Given a singly linked list of size N. The task is to left-shift the linked list by k nodes, where k is a
    given positive integer smaller than or equal to length of the linked list.
    
    Example 1:
    Input:
        N = 5
        value[] = {2, 4, 7, 8, 9}
        k = 3
    Output: 8 9 2 4 7
    Explanation:
        Rotate 1: 4 -> 7 -> 8 -> 9 -> 2
        Rotate 2: 7 -> 8 -> 9 -> 2 -> 4
        Rotate 3: 8 -> 9 -> 2 -> 4 -> 7
    
    Example 2:
    Input:
        N = 8
        value[] = {1, 2, 3, 4, 5, 6, 7, 8}
        k = 4
    Output: 5 6 7 8 1 2 3 4
    
    Expected Time Complexity: O(N).
    Expected Auxiliary Space: O(1).
    
    Constraints:
        1 <= N <= 10^3
        1 <= k <= N
 */

import java.util.*;

class Node {

    int data;
    Node next;

    Node(int data) {

        this.data = data;
        next = null;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {

            head = newNode;
            return;
        }

        Node temp = head;
        
        while(temp.next != null)
            temp = temp.next;

        temp.next = newNode;
    }

    // int countNode() {

    //     if(head == null)
    //         return 0;

    //     Node temp = head;
    //     int cnt = 0;

    //     while(temp != null) {

    //         cnt++;
    //         temp = temp.next;
    //     }

    //     return cnt;
    // }

    void rotateLL(int k) {

        if(head == null) {

            System.out.println("\nLinked List is empty...!!");
            return;
        }

        if(k < 0) {
            
            System.out.println("\nInvalid number of rotations");
            return;
        }

        while(k != 0) {

            int data = head.data;
            head = head.next;
            addNode(data);
            k--;
        }
    }

    void printLL() {

        if(head == null) {
            System.out.println("\nLinked List is empty...!!");
            return;
        }

        Node temp = head;
        System.out.println();

        while(temp != null) {

            System.out.print(temp.data + " -> ");
            temp = temp.next;
        }

        System.out.println("null");
    }
}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        boolean flag = true;

        do {

            System.out.println("\n1 : add node in linked list");
            System.out.println("2 : rotate linked list");
            System.out.println("3 : print linked list");
            System.out.println("4 : Exit");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1:
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }                
                        break;

                case 2 :
                        {
                            System.out.print("\nEnter number of rotations :: ");
                            int k = sc.nextInt();

                            ll.rotateLL(k);
                        }
                        break;
                    
                case 3 : 
                        ll.printLL();
                        break;      

                case 4 :
                        flag = false;
                        break;
            
                default:
                        System.out.println("\nWrong Choice...!!");
                        break;
            }


        } while(flag);

        sc.close();
    }
}