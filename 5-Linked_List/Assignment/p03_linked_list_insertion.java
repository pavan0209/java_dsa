/*
    3.Linked List Insertion

    Create a link list of size N according to the given input literals. Each integer input is
    accompanied by an indicator which can either be 0 or 1. If it is 0, insert the integer in the
    beginning of the link list. If it is 1, insert the integer at the end of the link list.
    
    Hint: When inserting at the end, make sure that you handle NULL explicitly.
    
    Example 1:
    Input:
        LinkedList: 9->0->5->1->6->1->2->0->5->0
    Output: 
        5 2 9 5 6
    Explanation:
        Length of Link List = N = 5
        9 0 indicated that 9 should be inserted in the beginning. Modified Link List = 9.
        5 1 indicated that 5 should be inserted in the end. Modified Link List = 9,5.
        6 1 indicated that 6 should be inserted in the end. Modified Link List = 9,5,6.
        2 0 indicated that 2 should be inserted in the beginning. Modified Link List = 2,9,5,6.
        5 0 indicated that 5 should be inserted in the beginning. Modified Link List = 5,2,9,5,6.
        Final linked list = 5, 2, 9, 5, 6.

    Example 2:
    Input:
        LinkedList: 5->1->6->1->9->1
    Output: 
        5 6 9

    Expected Time Complexity: O(1) for insertAtBeginning() and O(N) for insertAtEnd().
    Expected Auxiliary Space: O(1) for both.

    Constraints:
        1 <= N <= 10^4
 */

import java.util.Scanner; 

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {

        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data, int pos) {

        if(pos < 0 || pos > 1)
            System.out.println("Invalid..");
        else if(pos == 1) 
            insertAtFirst(data);
        else 
            insertAtEnd(data);
    }

    void insertAtFirst(int data) {

        Node newNode = new Node(data);

        if(head == null) {

            head = newNode;
        }
        else {

            newNode.next = head;
            head = newNode;
        }
    }

    void insertAtEnd(int data) {

        Node newNode = new Node(data);

        if(head == null) {

            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }
    }

    int countNode() {

        int nodeCnt = 0;
        Node temp = head;

        while(temp != null) {

            nodeCnt++;
            temp = temp.next;
        }

        return nodeCnt;
    }

    void printLL() {

        if(head == null) {

            System.out.println("\nEmpty Linked List...");
        }
        else {

            Node temp = head;

            while (temp != null) {
                
                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }

            System.out.println("null");
        }
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';
        
        do {

            System.out.println("\n1 : AddNode");
            System.out.println("2 : CountNodes");
            System.out.println("3 : PrintNodes");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            System.out.print("Enter insert position :: ");
                            int pos = sc.nextInt();

                            ll.addNode(data, pos);
                        }
                        break;

                case 2 : 
                        System.out.println("\nTotal Number of Nodes :: " + ll.countNode());
                        break;

                case 3 : 
                        ll.printLL();
                        break;

                default :
                        System.out.println("Invalid Choice ....!!");
                        break;
            }

            System.out.print("\nDo you want to continue ?? : ");
            ch = sc.next().charAt(0);

        }while(ch == 'y' || ch == 'Y');

        sc.close();
    }
}