
import java.util.*;

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {

        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {
            
            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }
    }

    int countNode() {

        int nodeCnt = 0;
        Node temp = head;

        while(temp != null) {

            nodeCnt++;
            temp = temp.next;
        }

        return nodeCnt;
    }

    boolean isPalindromeLL() {

        Node st = head;
        int cnt = countNode();
        int i = 1;
        int temp = cnt;

        while(i <= (cnt+1)/2) {

            Node end = st;
            int j = 1;
            while(j < temp) {
                end = end.next;
                j++;
            }

            System.out.println(st.data + " == " + end.data);

            if(st.data != end.data)
                return false;

            st = st.next;
            i++;
            temp -= 2;
        }
        
        return true;
    }

    void printLL() {
        System.out.println();
        Node temp = head;

        while(temp != null) {

            System.out.print(temp.data + " -> ");
            temp = temp.next;
        }

        System.out.println("null");
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        boolean flag = true;
        
        do {

            System.out.println("\n1 : AddNode");
            System.out.println("2 : CountNodes");
            System.out.println("3 : printLL");
            System.out.println("4 : IsPalindromeLL");
            System.out.println("5 : Exit");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }
                        break;

                case 2 : 
                        System.out.println("\nTotal Number of Nodes :: " + ll.countNode());
                        break;

                case 3 :
                        ll.printLL();
                        break;

                case 4 :
                        {
                            if(ll.isPalindromeLL())
                                System.out.println("\nPalindrome Linked List...");
                            else 
                                System.out.println("\nNot a Palindrome Linked List");
                        }
                        break;

                case 5 :
                        flag = false;
                        break;

                default :
                        System.out.println("\nWrong Choice....!!");
                        break;
            }

        }while(flag);

        sc.close();
    }
}