/*
    7. Swap Kth nodes from ends
    
        Given a singly linked list of size N, and an integer K. You need to swap the Kth node from the
    beginning and Kth node from the end of the linked list. Swap the nodes through the links. Do not
    change the content of the nodes.

    Example 1:
    Input:
        N = 4, K = 1
        value[] = {1,2,3,4}
    Output: 1
    Explanation: 
        Here K = 1, hence after swapping the 1st node from the beginning and end the new list will be 4 2 3 1.

    Example 2:
    Input:
        N = 5, K = 7
        value[] = {1,2,3,4,5}
    Output: 1
    Explanation: 
        K > N. Swapping is invalid. Return the head node as it is.
    
    Expected Time Complexity: O(n)
    Expected Auxiliary space Complexity: O(1)

    Constraints:
        1 <= N <= 10^3
        1 <= K <= 10^3
*/

import java.util.Scanner; 

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {

        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {
            
            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }
    }

    int countNode() {

        int nodeCnt = 0;
        Node temp = head;

        while(temp != null) {

            nodeCnt++;
            temp = temp.next;
        }

        return nodeCnt;
    }

    void swapKthNodes(int pos) {

        int cnt = countNode();

        if(pos > cnt || cnt <= 0) {
            System.out.println("\nInvalid Position..!!");
            return;
        }
        int temp = pos;
        Node begin = head;

        while(temp > 1) {

            begin = begin.next;
            temp--;
        }   

        temp = 1;
        Node end = head;
        while(temp <= cnt-pos) {

            end = end.next;
            temp++;
        }

        int val = begin.data;
        begin.data = end.data;
        end.data = val;
    }

    void printLL() {

        if(head == null) {

            System.out.println("\nEmpty Linked List...!!");
        }
        else {

            Node temp = head;

            while(temp != null) {

                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }

            System.out.println("null");
        }
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';
        
        do {

            System.out.println("\n1 : AddNode");
            System.out.println("2 : CountNodes");
            System.out.println("3 : swap kth nodes from ends");
            System.out.println("4 : print Linked List");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }
                        break;

                case 2 : 
                        System.out.println("\nTotal Number of Nodes :: " + ll.countNode());
                        break;

                case 3 :
                        {
                            System.out.print("\nEnter position :: ");
                            int pos = sc.nextInt();

                            ll.swapKthNodes(pos);
                        }
                        break;

                case 4 :
                        ll.printLL();
                        break;

                default :
                        System.out.println("Invalid choice ....!!");
                        break;
            }

            System.out.print("\nDo you want to continue ?? : ");
            ch = sc.next().charAt(0);

        }while(ch == 'y' || ch == 'Y');

        sc.close();
    }
}