/*
    11. Insert in Middle of Linked List
    
    Given a linked list of size N and a key. The task is to insert the key in the middle of the linked list.
    
    Example 1:
    Input:
        LinkedList = 1->2->4
        key = 3
    Output: 1 2 3 4
    Explanation: 
        The new element is inserted after the current middle element in the linked list.
    
    Example 2:
    Input:
        LinkedList = 10->20->40->50
        key = 30
    Output: 10 20 30 40 50
    Explanation: 
        The new element is inserted after the current middle element in the linked list and Hence, the output is
        10 20 30 40 50.

    Expected Time Complexity : O(N)
    Expected Auxiliary Space : O(1)

    Constraints:
        1 <= N <= 10^4
 */

import java.util.Scanner; 

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {

        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {
            
            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }
    }

    int countNode() {

        int nodeCnt = 0;
        Node temp = head;

        while(temp != null) {

            nodeCnt++;
            temp = temp.next;
        }

        return nodeCnt;
    }

    void insertMiddleLL(int data) {

        Node newNode = new Node(data);

        if(head == null) {

            head = newNode;
        }
        else {

            int cnt = countNode();
            int pos = (cnt + 1) / 2;

            Node temp = head;

            while(pos > 1) {

                temp = temp.next;
                pos--;
            }

            newNode.next = temp.next;
            temp.next = newNode;
        }
    }

    void printLL() {

        if(head == null) {

            System.out.println("\nEmpty Linked List...!!");
        }
        else {

            Node temp = head;

            while(temp != null) {

                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }

            System.out.println("null");
        }
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';
        
        do {

            System.out.println("\n1 : AddNode");
            System.out.println("2 : CountNodes");
            System.out.println("3 : Print Linked List");
            System.out.println("4 : Insert In Middle Of Linked List");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }
                        break;

                case 2 : 
                        System.out.println("\nTotal Number of Nodes :: " + ll.countNode());
                        break;

                case 3 :
                        ll.printLL();
                        break;

                case 4 : 
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.insertMiddleLL(data);
                        }
                        break;

                default :
                        System.out.println("\nInvalid Choice ...!!");
            }

            System.out.print("\nDo you want to continue ?? : ");
            ch = sc.next().charAt(0);

        }while(ch == 'y' || ch == 'Y');

        sc.close();
    }
}