/*
 *  1. Count nodes of linked list
    Given a singly linked list. The task is to find the length of the linked list, where length is defined
    as the number of nodes in the linked list.

    Example 1:
    Input:
        LinkedList: 1->2->3->4->5
    Output: 5
    Explanation: Count of nodes in the linked list is 5, which is its length.

    Example 2:
    Input:
        LinkedList: 2->4->6->7->5->1->0
    Output: 7
    Explanation: Count of nodes in the linked list is 7. Hence, the outputis 7.
    
    Expected Time Complexity : O(N)
    Expected Auxiliary Space : O(1)
    
    Constraints:
        1 <= N <= 10^5
        1 <= value <= 10^3
 */

import java.util.Scanner; 

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {

        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {
            
            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }
    }

    int countNode() {

        int nodeCnt = 0;
        Node temp = head;

        while(temp != null) {

            nodeCnt++;
            temp = temp.next;
        }

        return nodeCnt;
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';
        
        do {

            System.out.println("\n1 : AddNode");
            System.out.println("2 : CountNodes");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }
                        break;

                case 2 : 
                        System.out.println("\nTotal Number of Nodes :: " + ll.countNode());
                        break;
            }

            System.out.print("\nDo you want to continue ?? : ");
            ch = sc.next().charAt(0);

        }while(ch == 'y' || ch == 'Y');

        sc.close();
    }
}