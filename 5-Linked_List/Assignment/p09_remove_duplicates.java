/*
    9. Remove duplicate element from sorted Linked List
    
    Given a singly linked list consisting of N nodes. The task is to remove duplicates (nodes with duplicate
    values) from the given list (if exists).

    Note: Try not to use extra space. The nodes are arranged in a sorted way.
    
    Example 1:
    Input:
        LinkedList: 2->2->4->5
    Output: 2 4 5
    Explanation: 
        In the given linked list 2 ->2 -> 4-> 5, only 2 occurs more than 1 time. So we need to remove it once.

    Example 2:
    Input:
        LinkedList: 2->2->2->2->2
    Output: 2
    Explanation: 
        In the given linked list 2 ->2 ->2 ->2 ->2, 2 is the only element and is repeated 5 times. So we need
        to remove any four 2.
    
    Expected Time Complexity : O(N)
    Expected Auxiliary Space : O(1)

    Constraints:
        1 <= Number of nodes <= 10^5
 */

import java.util.Scanner; 

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {

        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {
            
            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }
    }

    int countNode() {

        int nodeCnt = 0;
        Node temp = head;

        while(temp != null) {

            nodeCnt++;
            temp = temp.next;
        }

        return nodeCnt;
    }

    void removeDuplicates() {

        if(head == null) {

            System.out.println("\nNothing to delete...!!");
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                if(temp.data == temp.next.data) {

                    temp.next = temp.next.next;
                }
                else {

                    temp = temp.next;
                }
            }
        }
       
    }

    void printLL() {

        if(head == null) {

            System.out.println("\nEmpty Linked List...!!");
        }
        else {

            Node temp = head;

            while(temp != null) {

                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }

            System.out.println("null");
        }
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';
        
        do {

            System.out.println("\n1 : AddNode");
            System.out.println("2 : CountNodes");
            System.out.println("3 : Print Linked List");
            System.out.println("4 : remove duplicates From Linked List");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }
                        break;

                case 2 : 
                        System.out.println("\nTotal Number of Nodes :: " + ll.countNode());
                        break;

                case 3 :
                        ll.printLL();
                        break;

                case 4 :
                        ll.removeDuplicates();
                        break;

                default :
                        System.out.println("\nInvalid Choice ...!!");
            }

            System.out.print("\nDo you want to continue ?? : ");
            ch = sc.next().charAt(0);

        }while(ch == 'y' || ch == 'Y');

        sc.close();
    }
}