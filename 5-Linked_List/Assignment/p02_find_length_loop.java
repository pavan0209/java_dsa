/*
    2.Find length of Loop
    
    Given a linked list of size N. The task is to complete the function countNodesinLoop() that
    checks whether a given Linked List contains a loop or not and if the loop is present then return
    the count of nodes in a loop or else return 0. C is the position of the node to which the last node
    is connected. If it is 0 then no loop.
    
    Example 1:
    Input:
        N = 10
        value[]={25,14,19,33,10,21,39,90,58,45}
        C = 4
    Output: 7
    Explanation: 
        The loop is 45->33. So length of loop is 33->10->21->39-> 90->58->45 = 7. 
        The number 33 is connected to the last node to form the loop because according to the input the 4th node
        from the beginning(1 based index) will be connected to the last node for the loop.
    
    Example 2:
    Input:
        N = 2
        value[] = {1,0}
        C = 1
    Output: 2
    Explanation: 
        The length of the loop is 2.

    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)

    Constraints:
        1 <= N <= 500
        0 <= C <= N-1
 */


import java.util.Scanner; 

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {

        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {
            
            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }
    }

    int countNode() {

        int nodeCnt = 0;
        Node temp = head;

        while(temp != null) {

            nodeCnt++;
            temp = temp.next;
        }

        return nodeCnt;
    }

    int findLoopLen(int c) {

        if(c == 0) {

            return 0;
        }
        else {

            /*Node temp = head;
            int i = 1;
            int cnt = 0;

            while(temp != null) {

                if(i >= c) {
                    cnt++;
                }
                i++;
                temp = temp.next;
            }*/

            int cnt = countNode();
            if(c > cnt) {

                return -1;
            }

            return cnt-c+1;
        }
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';
        
        do {

            System.out.println("\n1 : AddNode");
            System.out.println("2 : FindLengthOfLoop");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }
                        break;
                
                case 2 :
                        {
                            System.out.print("\nEnter position where last Node is connected :: ");
                            int c = sc.nextInt();
                            System.out.println("\nLength of Loop :: " + ll.findLoopLen(c));
                        }
                        break;
                
                default :
                        System.out.println("Invalid Choice....!!");
            }

            System.out.print("\nDo you want to continue ?? : ");
            ch = sc.next().charAt(0);

        }while(ch == 'y' || ch == 'Y');

        sc.close();
    }
}