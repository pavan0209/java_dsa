/*
    15. Remove duplicates from an unsorted linked list
    
        Given an unsorted linked list of N nodes. The task is to remove duplicate elements from this unsorted
    Linked List. When a value appears in multiple nodes, the node which appeared first should be kept, all other
    duplicates are to be removed.
    
    Example 1:
    Input:
        N = 4
        value[] = {5,2,2,4}
    Output: 5 2 4
    Explanation:
        Given linked list elements are 5->2->2->4, in which 2 is repeated only. So, we will delete the extra
    repeated elements 2 from the linked list and the resultant linked list will contain 5->2->4
    
    Example 2:
    Input:
        N = 5
        value[] = {2,2,2,2,2}
    Output: 2
    Explanation:
        Given linked list elements are 2->2->2->2->2, in which 2 is repeated. So, we will delete 
    the extra repeated elements 2 from the linked list and the resultant linked list will contain only 2.
    
    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(N)
    
    Constraints:
        1 <= size of linked lists <= 10^6
        0 <= numbers in list <= 10^4
 */

import java.util.*;

class Node {

    int data;
    Node next;

    Node(int data) {

        this.data = data;
        next = null;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {

            head = newNode;
            return;
        }

        Node temp = head;
        
        while(temp.next != null)
            temp = temp.next;

        temp.next = newNode;
    }

    void removeDuplicates() {

        LinkedHashSet<Integer> lhs = new LinkedHashSet<>();
        Node temp = head;

        while(temp != null) {

            lhs.add(temp.data);
            temp = temp.next;
        }

        head = null;
        Iterator<Integer> itr = lhs.iterator();

        while(itr.hasNext())
            addNode(itr.next());
    }

    void printLL() {

        if(head == null) {
            System.out.println("\nLinked List is empty...!!");
            return;
        }

        Node temp = head;
        System.out.println();

        while(temp != null) {

            System.out.print(temp.data + " -> ");
            temp = temp.next;
        }

        System.out.println("null");
    }
}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';

        do {

            System.out.println("\n1 : add node in linked list");
            System.out.println("2 : remove duplicates from linkedList");
            System.out.println("3 : print linked list");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1:
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }                
                        break;

                case 2 :
                        ll.removeDuplicates();
                        break;
                    
                case 3 : 
                        ll.printLL();
                        break;
            
                default:
                        System.out.println("\nWrong Choice...!!");
                        break;
            }

            System.out.print("\nDo you want to continue?? :: ");
            ch = sc.next().charAt(0);

        } while(ch == 'y' || ch == 'Y');

        sc.close();
    }
}