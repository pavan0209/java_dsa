/*
    4.Doubly linked list Insertion at given position
    
        Given a doubly-linked list, a position p, and an integer x. The task is to add a new node with
    value x at the position just after pth node in the doubly linked list.

    Example 1:
    Input:
        LinkedList: 2<->4<->5
        p = 2, x = 6
    Output: 2 4 5 6
    Explanation: 
        p = 2, and x = 6. So, 6 is inserted after p, i.e, at position 3 (0-based indexing).
    
    Example 2:
    Input:
        LinkedList: 1<->2<->3<->4
        p = 0, x = 44
    Output: 1 44 2 3 4
    Explanation: 
        p = 0, and x = 44 . So, 44 is inserted after p, i.e, at position 1 (0-based indexing).
    
    Expected Time Complexity : O(N)
    Expected Auxiliary Space : O(1)

    Constraints:
        1 <= N <= 10^4
        0 <= p < N
 */

import java.util.*; 

class Node {

    int data;
    Node prev;
    Node next;

    Node(int data) {

        this.data = data;
        prev = null;
        next = null;
    }
}

class LinkedList {

    Node head = null;

    int countNode() {

        Node temp = head;
        int cnt = 0;

        while(temp != null) {

            cnt++;
            temp = temp.next;
        }

        return cnt;
    }

    void addLast(int data) {

        Node newNode = new Node(data);

        if(head == null) {
            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
            newNode.prev = temp;
        }
    }

    void addAtPos(int data, int pos) {

        int nodeCnt = countNode();

        if(pos < 0 || pos > nodeCnt) {

            System.out.println("\nInvalid Node position..!!");
            return;
        }
        else {
             
            if(pos == nodeCnt-1) {

                addLast(data);
            }
            else {
                
                Node temp = head;
                Node newNode = new Node(data);

                while(pos != 0) {
                    temp = temp.next;
                    pos--;
                }

                temp.next.prev = newNode;
                newNode.next = temp.next;
                newNode.prev = temp;
                temp.next = newNode;
            }
        }
    }

    void printLL() {

        if(head == null) {

            System.out.println("\nLinked List is empty..!!");
            return;
        }

        Node temp = head;
        
        while(temp != null) {

            System.out.print(temp.data + " -> ");
            temp = temp.next;
        }

        System.out.println("null");
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';
        
        do {

            System.out.println("\n1 : AddNode");
            System.out.println("2 : addAtPosition");
            System.out.println("3 : PrintNodes");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 : 
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addLast(data);
                        }
                        break;

                case 2 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            System.out.print("Enter insert position :: ");
                            int pos = sc.nextInt();

                            ll.addAtPos(data, pos);
                        }
                        break;
                case 3 : 
                        ll.printLL();
                        break;

                default :
                        System.out.println("Invalid Choice ....!!");
                        break;
            }

            System.out.print("\nDo you want to continue ?? : ");
            ch = sc.next().charAt(0);

        }while(ch == 'y' || ch == 'Y');

        sc.close();
    }
}