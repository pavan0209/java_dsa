/*
 *
 */

import java.util.*;

class Node {

    int data;
    Node next;

    Node(int data) {

        this.data = data;
        next = null;
    }
}

class LinkedList {

    Node addNode(Node head, int data) {

        Node newNode = new Node(data);

        if(head == null) {

            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null)
                temp = temp.next;

            temp.next = newNode;
        }

        return head;
    }

    void printLL(Node head) {

        if(head == null)
            return;

        Node temp = head;

        System.out.println();

        while(temp != null) {

            System.out.print(temp.data + " -> ");
            temp = temp.next;
        }

        System.out.println("null");
    }

    Node concatLL(Node head1, Node head2) {

        if(head1 == null && head2 == null) {
            
            System.out.println("\nBoth Linked Lists are empty...!!");
            return null;
        }

        if(head1 == null) {

            head1 = head2;
        }
        else {

            Node temp = head1;

            while(temp.next != null)
                temp = temp.next;

            temp.next = head2;
        }

        System.out.println("\nSuccesfully Concatenated....");
        return head1;
    }
}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        Node head1 = null;
        Node head2 = null;

        boolean flag = true;    

        do {

            System.out.println("\n1 : add node in linked list 1");
            System.out.println("2 : add node in linked list 2");
            System.out.println("3 : print linked list 1");
            System.out.println("4 : print linked list 2");
            System.out.println("5 : concat linked lists");
            System.out.println("6 : Exit");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            head1 = ll.addNode(head1, data);
                        }
                        break;

                case 2 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            head2 = ll.addNode(head2, data);
                        }
                        break;

                case 3 :
                        ll.printLL(head1);
                        break;

                case 4 :
                        ll.printLL(head2);
                        break;

                case 5 :
                        head1 = ll.concatLL(head1, head2);
                        break;

                case 6 :
                        flag = false;
                        break;

                default :
                        System.out.println("\nWrong choice...!!!");
                        break;
            }

        } while(flag);

        sc.close();
    }
}