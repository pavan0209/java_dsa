/*
    5. Reverse a linked list
    Given a linked list of N nodes. The task is to reverse this list.
    
    Example 1:
    Input:
        LinkedList: 1->2->3->4->5->6
    Output: 
        6 5 4 3 2 1
    Explanation: 
        After reversing the list, elements are 6->5->4->3->2->1.

    Example 2:
    Input:
        LinkedList: 2->7->8->9->10
    Output: 
        10 9 8 7 2
    Explanation: 
        After reversing the list, elements are 10->9->8->7->2.

    Expected Time Complexity: O(N).
    Expected Auxiliary Space: O(1).

    Constraints:
        1 <= N <= 10^4
 */

import java.util.Scanner; 

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {

        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {
            
            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }
    }

    int countNode() {

        int nodeCnt = 0;
        Node temp = head;

        while(temp != null) {

            nodeCnt++;
            temp = temp.next;
        }

        return nodeCnt;
    }

    /*void reverseLL() {

        int cnt = countNode();
        Node st = head;
        int tm = 1;

        while(tm <= cnt / 2) {

            int fast = tm;
            Node end = st;

            while(fast <= cnt-tm) {

                end = end.next;
                fast++;
            }

            int temp = end.data;
            end.data = st.data;
            st.data = temp;

            st = st.next;
            tm++;
        }
    }*/

    void reverseLL() {

        Node prev = null;
        Node next = null;
        Node curr = head;

        while(curr != null) {

            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }
        
        head = prev;
    }

    void printLL() {

        if(head == null) {

            System.out.println("\nEmpty Linked List..!!");
        }
        else {

            Node temp = head;

            while(temp != null) {

                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }

            System.out.println("null");
        }
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';
        
        do {

            System.out.println("\n1 : AddNode");
            System.out.println("2 : CountNodes");
            System.out.println("3 : printLL");
            System.out.println("4 : reverseLL");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }
                        break;

                case 2 : 
                        System.out.println("\nTotal Number of Nodes :: " + ll.countNode());
                        break;

                case 3 : 
                        ll.printLL();
                        break;

                case 4 :
                        ll.reverseLL();
                        break;

                default :
                        System.out.println("Invalid Choice ...!!");
                        break;
            }

            System.out.print("\nDo you want to continue ?? : ");
            ch = sc.next().charAt(0);

        }while(ch == 'y' || ch == 'Y');
    
        sc.close();
    }

}