/*
    10. Identical Linked Lists
        
    Given two Singly Linked List of N and M nodes respectively. The task is to check whether two linked lists
    are identical or not.
    Two Linked Lists are identical when they have the same data and with the same arrangement too.
    
    Example 1:
    Input:
        LinkedList1: 1->2->3->4->5->6
        LinkedList2: 99->59->42->20
    Output: Not identical
    
    Example 2:
    Input:
        LinkedList1: 1->2->3->4->5
        LinkedList2: 1->2->3->4->5
    Output: Identical
    
    Constraints:
        1 <= N <= 10^3

    Expected Time Complexity : O(N)
    Expected Auxiliary Space : O(1)
 */

import java.util.Scanner; 

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {

        this.data = data;
    }
}

class LinkedList {

    Node addNode(Node head, int data) {

        Node newNode = new Node(data);

        if(head == null) {
            
            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }

        return head;
    }

    int countNode(Node head) {

        int nodeCnt = 0;
        Node temp = head;

        while(temp != null) {

            nodeCnt++;
            temp = temp.next;
        }

        return nodeCnt;
    }

    String check_identical_LL(Node head1, Node head2) {

        int listCount1 = countNode(head1);
        int listCount2 = countNode(head2);

        if(listCount1 != listCount2)
            return "Linked Lists are Not Identical";

        Node temp1 = head1;
        Node temp2 = head2;

        while(temp1 != null) {

            if(temp1.data != temp2.data) 
                return "Linked Lists are Not Identical";

            temp1 = temp1.next;
            temp2 = temp2.next;
        }

        return "Linked Lists are Identical";
    }

    void printLL(Node head) {

        if(head == null) {

            System.out.println("\nEmpty Linked List...!!");
        }
        else {

            Node temp = head;

            while(temp != null) {

                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }

            System.out.println("null");
        }
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        Node head1 = null;
        Node head2 = null;

        char ch = 'N';
        
        do {

            System.out.println("\n1 : Add Nodes in 1st Linked List");
            System.out.println("2 : Add Nodes in 2nd Linked List");
            System.out.println("3 : Count Nodes in Linked List 2");
            System.out.println("4 : Count Nodes in Linked List 1");
            System.out.println("5 : Print Linked List 1");
            System.out.println("6 : Print Linked List 2");
            System.out.println("7 : Check identical linked lists or not");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            head1 = ll.addNode(head1, data);
                        }
                        break;
                
                case 2 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            head2 = ll.addNode(head2, data);
                        }
                        break;

                case 3 : 
                        System.out.println("\nTotal Number of Nodes in 1st Linked List :: " + ll.countNode(head1));
                        break;

                case 4 : 
                        System.out.println("\nTotal Number of Nodes in 2nd Linked List :: " + ll.countNode(head2));
                        break;

                case 5 :
                        ll.printLL(head1);
                        break;
                
                case 6 :
                        ll.printLL(head2);
                        break;

                case 7 :
                        System.out.println("\n" + ll.check_identical_LL(head1, head2));
                        break;

                default :
                        System.out.println("\nInvalid Choice ...!!");
                        break;
            }

            System.out.print("\nDo you want to continue ?? : ");
            ch = sc.next().charAt(0);

        }while(ch == 'y' || ch == 'Y');

        sc.close();
    }
}