/*
    14. Given a linked list of 0s, 1s and 2s, sort it.
    
    Given a linked list of N nodes where nodes can contain values 0s, 1s, and 2s only. The task is to
    segregate 0s, 1s, and 2s linked lists such that all zeros segregate to the head side, 2s at the
    end of the linked list, and 1s in the middle of 0s and 2s.
    
    Example 1:
    Input:
        N = 8
        value[] = {1,2,2,1,2,0,2,2}
    Output: 0 1 1 2 2 2 2 2
    Explanation: 
        All the 0s are segregated to the left end of the linked list, 2s to the right end of the list, and 1s in
        between.

    Example 2:
    Input:
        N = 4
        value[] = {2,2,0,1}
    Output: 0 1 2 2
    Explanation: 
        After arranging all the 0s,1s and 2s in the given format, the output will be 0 1 2 2.

    Expected Time Complexity: O(N).
    Expected Auxiliary Space: O(N).
    
    Constraints:
        1 <= N <= 10^6
 */

import java.util.Scanner; 

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {

        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {
            
            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }
    }

    int countNode() {

        int nodeCnt = 0;
        Node temp = head;

        while(temp != null) {

            nodeCnt++;
            temp = temp.next;
        }

        return nodeCnt;
    }

    /*
    void sortLL() {

        if(head == null) {

            System.out.println("\nEmpty Linked List");
            return;
        }

        Node temp1 = head;
        Node temp2 = head;

        while(temp1 != null) {

            temp2 = head;

            while(temp2.next != null) {

                if(temp2.data  > temp2.next.data) {

                    int temp = temp2.data;
                    temp2.data = temp2.next.data;
                    temp2.next.data = temp;
                }

                temp2 = temp2.next;
            }

            temp1 = temp1.next;
        }
        
    }*/

    void sortLL() {

        if(head == null) {

            System.out.println("\nEmpty Linked List...!!");
            return;
        }

        int countArr[] = new int[] {0, 0, 0};
        Node temp = head;

        while(temp != null) {

            countArr[temp.data]++;
            temp = temp.next;
        }

        temp = head;
        int i = 0;
        
        while(temp != null) {

            if(countArr[i] == 0) {

                i++;
            }
            else {

                temp.data = i;
                --countArr[i];
                temp = temp.next;
            }            
        }

    }

    void printLL() {

        if(head == null) {

            System.out.println("\nEmpty Linked List...!!");
        }
        else {

            Node temp = head;

            while(temp != null) {

                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }

            System.out.println("null");
        }
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';
        
        do {

            System.out.println("\n1 : AddNode");
            System.out.println("2 : CountNodes");
            System.out.println("3 : Print Linked List");
            System.out.println("4 : Sort Linked List");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }
                        break;

                case 2 : 
                        System.out.println("\nTotal Number of Nodes :: " + ll.countNode());
                        break;
                
                case 3 :
                        ll.printLL();
                        break;

                case 4 :
                        ll.sortLL();
                        break;

                default :
                        System.out.println("Invalid Choice.....!!");
            }

            System.out.print("\nDo you want to continue ?? : ");
            ch = sc.next().charAt(0);

        }while(ch == 'y' || ch == 'Y');

        sc.close();
    }
}