/*
    8. Delete without head pointer

    You are given a pointer/ reference to the node which is to be deleted from the linked list of N
    nodes. The task is to delete the node. Pointer/ reference to the head node is not given.
    
    Note: No head reference is given to you. It is guaranteed that the node to be deleted is not a tail
    node in the linked list.
    
    Example 1:
    Input:
        N = 2
        value[] = {1,2}
        node = 1
    Output: 2
    Explanation: 
        After deleting 1 from the linked list, we have remaining nodes as 2.

    Example 2:
    Input:
        N = 4
        value[] = {10,20,4,30}
        node = 20
    Output: 10 4 30
    Explanation:
        After deleting 20 from the linked list, we have remaining nodes as 10, 4 and 30.

    Expected Time Complexity : O(1)
    Expected Auxiliary Space : O(1)
    
    Constraints:
        2 <= N <= 10^3
 */

import java.util.Scanner; 

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {

        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {
            
            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }
    }

    int countNode() {

        int nodeCnt = 0;
        Node temp = head;

        while(temp != null) {

            nodeCnt++;
            temp = temp.next;
        }

        return nodeCnt;
    }

    void deleteNode(Node node) {

        if(node == null) {

            System.out.println("\nEmpty Linked List");
            return;
        }

        if(node.next == null) {

            System.out.println("\nCan't be Free..");
            return;
        }

        node.data = node.next.data;
        node.next = node.next.next;
    }

    void printLL() {

        if(head == null) {

            System.out.println("\nEmpty Linked List...!!");
        }
        else {

            Node temp = head;

            while(temp != null) {

                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }

            System.out.println("null");
        }
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';
        
        do {

            System.out.println("\n1 : AddNode");
            System.out.println("2 : CountNodes");
            System.out.println("3 : Print Linked List");
            System.out.println("4 : delete without head pointer");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }
                        break;

                case 2 : 
                        System.out.println("\nTotal Number of Nodes :: " + ll.countNode());
                        break;

                case 3 :
                        ll.printLL();
                        break;

                case 4 :
                        ll.deleteNode(ll.head.next);
                        break;

                default :
                        System.out.println("\nInvalid Choice ...!!");
            }

            System.out.print("\nDo you want to continue ?? : ");
            ch = sc.next().charAt(0);

        }while(ch == 'y' || ch == 'Y');

        sc.close();
    }
}