/*
 *  Doubly Linked List
 */

import java.util.*;

class Node {

    Node prev = null;
    int data;
    Node next = null;;

    Node(int data) {

        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addFirst(int data) {

        Node newNode = new Node(data);

        if (head == null) {

            head = newNode;

        } else {

            newNode.next = head;
            head.prev = newNode;
            head = newNode;
        }
    }

    void addLast(int data) {

        Node newNode = new Node(data);

        if(head == null) {

            head = newNode;
        }
        else {

            Node temp = head;
            
            while (temp.next != null) {
                
                temp = temp.next;
            }

            newNode.prev = temp;
            temp.next = newNode;
        }
    }

    int countNode() {

        int count = 0;
        Node temp = head;

        while(temp != null) {
            
            count++;
            temp = temp.next;
        }

        return count;
    }

    void addAtPosition(int pos, int data) {

        int nodeCnt = countNode();

        if(pos <= 0 ||  pos > nodeCnt+1) {

            System.out.println("\nInvalid Node Position.....!!!");
        }
        else if(pos == 1) {

            addFirst(data);
        }
        else if(pos == nodeCnt+1) {

            addLast(data);
        }
        else {

            Node newNode = new Node(data);
            Node temp = head;

            while(pos - 2 != 0) {

                temp = temp.next;
                pos--;
            }

            temp.next.prev = newNode;
            newNode.next = temp.next;
            newNode.prev = temp;
            temp.next = newNode;
        }
    }

    void deleteFirst() {

        if(head == null) {

            System.out.println("\nEmpty Linked List.....!!!");
        }
        else if(head.next == null) {

            head = null;
        }
        else {

            head = head.next;
            head.prev = null;
        }
    }
    
    void deleteLast() {

        if(head == null) {

            System.out.println("\nEmpty Linked List.....!!!!");
        }
        else if(head.next == null){

            head = null;
        }
        else {

            Node temp = head;

            while(temp.next.next != null) {

                temp = temp.next;
            }

            temp.next.prev = null;
            temp.next = null;
        }
    }

    void deleteAtPosition(int pos) {

        int nodeCnt = countNode();

        if(pos <= 0 || pos > nodeCnt) {

            System.out.println("\nInvalid Node Position....!!!");
        }
        else if(pos == 1) {

            deleteFirst();
        }
        else if(pos == nodeCnt) {

            deleteLast();
        }
        else {

            Node temp = head;

            while(pos - 2 != 0) {

                temp = temp.next;
                pos--;
            }

            temp.next = temp.next.next;
            temp.next.prev = temp;
        }
    }

    void printDLL() {

        if(head == null) {

            System.out.println("\nEmpty Linked List.....!!!");
        }
        else {

            Node temp = head;

            System.out.println();

            while(temp != null) {

                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }

            System.out.println("null");
        }
    }
}

class Solution {
    public static void main(String[] args) {

        LinkedList sll = new LinkedList();
        Scanner sc = new Scanner(System.in);
        char ch;

        do{

            System.out.println("\t\t\t**** Operations on Doubly Linked List ****\n");

            System.out.println("\t\t1. addFirst");
            System.out.println("\t\t2. addLast");
            System.out.println("\t\t3. addAtPosition");
            System.out.println("\t\t4. deleteFirst");
            System.out.println("\t\t5. deleteLast");
            System.out.println("\t\t6. deleteAtPosition");
            System.out.println("\t\t7. countNode");
            System.out.println("\t\t8. printDoublyLL");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1: 
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();
                            sll.addFirst(data);
                        }
                        break;

                case 2: 
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();
                            sll.addLast(data);
                        }
                        break;

                case 3 :
                        {
                            System.out.print("\nEnter Position :: ");
                            int pos = sc.nextInt();
                            System.out.print("Enter data :: ");
                            int data = sc.nextInt();

                            sll.addAtPosition(pos,data);
                        }
                        break;

                case 4 :
                        sll.deleteFirst();
                        break;
                
                case 5 :
                        sll.deleteLast();
                        break;

                case 6 :
                        {
                            System.out.print("\nEnter position to delete node :: ");
                            int pos = sc.nextInt();

                            sll.deleteAtPosition(pos);
                        }
                        break;

                case 7 :
                        System.out.println("\nTotal Nodes in Linked List :: " + sll.countNode());
                        break;

                case 8 :
                        sll.printDLL();
                        break;

                default:
                        System.out.println("\nWrong input....!!!");
                        break;
            }

            System.out.print("\nDo you want to continue ?? ");
            ch = sc.next().charAt(0);

            System.out.println();
            
        } while(ch == 'Y' || ch == 'y');

        sc.close();
    }
}