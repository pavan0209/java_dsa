/*
 *  Middle Node of Linked List
 */

import java.util.Scanner;

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {
        
        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {

            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }
    }

    int countNode() {

        if(head == null) 
            return 0;

        int cnt = 0;
        Node temp = head;

        while(temp != null) {

            cnt++;
            temp = temp.next;
        }

        return cnt;
    }

    
    int findMiddleBasic() {

        int len = countNode();
        int count = 0;
        Node temp = head;

        while(count < len / 2) {

            temp = temp.next;
            count++;
        }

        return temp.data;
    }
    

    int findMiddleSlowFast() {

        if(head == null)
            return -1;

        Node slow = head;
        Node fast = head.next;

        while(fast != null) {

            fast = fast.next;

            if(fast != null) 
                fast = fast.next;

            slow = slow.next;
        }

        return slow.data;
    }

    void printLL() {

        if(head == null) {

            System.out.println("\nEmpty Linked List");
            return;
        }
        
        Node temp = head;
        System.out.println();

        while(temp != null) {

            System.out.print(temp.data + " -> ");
            temp = temp.next;
        }

        System.out.println("null");
    }

}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';

        do{

            System.out.println("\n1 : Add Nodes in Linked List");
            System.out.println("2 : Print Linked List");
            System.out.println("3 : Middle Of Linked List (Basic)");
            System.out.println("4 : Middle Of Linked List (slow-fast ptr)");

            System.out.print("\nEnter Your Choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1:
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }
                        break;
                case 2 :
                        ll.printLL();
                        break;
                
                case 3 :
                        System.out.println("\nMiddle Node of Linked List :: " + ll.findMiddleBasic());
                        break;

                case 4 :
                        System.out.println("\nMiddle Node of Linked List :: " + ll.findMiddleSlowFast());
                        break;
                        
                default:
                        System.out.println("\nInvalid Choice...!!");
                        break;
            }

            System.out.print("\nDo you want to continue ?? :: ");
            ch = sc.next().charAt(0);

        }while(ch == 'Y' || ch == 'y');

        sc.close();
    }
}