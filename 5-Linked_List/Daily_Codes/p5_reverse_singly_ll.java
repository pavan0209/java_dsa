/*
 *
 */

import java.util.Scanner;

class Node {

    int data = 0;
    Node next = null;

    Node(int data) {
        
        this.data = data;
    }
}

class LinkedList {

    Node head = null;

    void addNode(int data) {

        Node newNode = new Node(data);

        if(head == null) {

            head = newNode;
        }
        else {

            Node temp = head;

            while(temp.next != null) {

                temp = temp.next;
            }

            temp.next = newNode;
        }
    }

    int countNode() {

        if(head == null) 
            return 0;

        int cnt = 0;
        Node temp = head;

        while(temp != null) {

            cnt++;
            temp = temp.next;
        }

        return cnt;
    }

    void revLLIteration() {

        int nodeCount = countNode();
        int i = 1;
        Node start = head;

        while(i <= nodeCount / 2) {

            int fast = i;
            Node end = start;

            while(fast <= nodeCount-i) {

                end = end.next;
                fast++;
            }

            int temp = start.data;
            start.data = end.data;
            end.data = temp;

            i++;
            start = start.next;
        }
    }
    

    void revLLIterationInPlace() {

        Node prev = null;
        Node forward = null;
        Node curr = head;

        while(curr != null) {

            forward = curr.next;
            curr.next = prev;
            prev = curr;
            curr = forward;
        }

        head = prev;
    }

    void revLLRecursion(Node curr, Node prev) {

        if(curr == null) {

            head = prev;
            return;
        }

        Node forward = curr.next;
        curr.next = prev;
        prev = curr;
        curr = forward;

        revLLRecursion(curr, prev);
    }

    void printLL() {

        if(head == null) {

            System.out.println("\nEmpty Linked List");
            return;
        }
        
        Node temp = head;
        System.out.println();

        while(temp != null) {

            System.out.print(temp.data + " -> ");
            temp = temp.next;
        }

        System.out.println("null");
    }

}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LinkedList ll = new LinkedList();

        char ch = 'N';

        do{

            System.out.println("\n1 : Add Nodes in Linked List");
            System.out.println("2 : Print Linked List");
            System.out.println("3 : Reverse Linked List (iterative)");
            System.out.println("4 : Reverse Linked List (iterative : in place)");
            System.out.println("5 : Reverse Linked List (recursive : in place)");

            System.out.print("\nEnter Your Choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1:
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            ll.addNode(data);
                        }
                        break;
                case 2 :
                        ll.printLL();
                        break;
                
                case 3 :
                        ll.revLLIteration();
                        break;
                   
                case 4 :
                        ll.revLLIterationInPlace();
                        break;        
                        
                case 5 :
                        ll.revLLRecursion(ll.head, null);
                        break;

                default:
                        System.out.println("\nInvalid Choice...!!");
                        break;
            }

            System.out.print("\nDo you want to continue ?? :: ");
            ch = sc.next().charAt(0);

        }while(ch == 'Y' || ch == 'y');

        sc.close();
    }
}