/*
    Q5. Queue Operations
    
    Given N integers, the task is to insert those elements in the queue. Also, given M integers, the task is to
    find the frequency of each number in the Queue.
    
    Note:
    insert() will be called N times by main().
    findFrequency() will be called M times by main();
    Where k is each element passing through respective function calls.

    Example 1:
    Input:
        N = 8
        1 2 3 4 5 2 3 1
        M = 5
        1 3 2 9 10
    Output:
        2
        2
        2
        -1
        -1
    Explanation:
        After inserting 1, 2, 3, 4, 5, 2, 3 and 1 into the queue, frequency of 1 is 2, 3 is 2 and 2 is 2. 
        Since 9 and 10 are not there in the queue we output -1 for them.
        
    Example 2:
    Input:
        N = 6
        1 2 1 1 1 4
        M = 4
        1 5 4 3
    Output:
        4
        -1
        1
        -1
    Explanation:
        After inserting 1, 2, 1, 1, 1 and 4 into the queue, frequency of 1 is 4 and that of 4 is 1.
        Since 5 and 3 are not there in the queue we output -1 for them.
    
    Expected Time Complexity: O(N*M)
    Expected Space Complexity: O(N)

    Constraints:
        1 <= N <= 10^3
        1 <= M <= 10^3
        1 <= Elements of Queue <= 10^6
 */

import java.util.*;

class Solution {

    static void insert(Queue<Integer> q, int k) {

        q.offer(k);
    }

    static int findFrequency(Queue<Integer> q, int k) {

        int cnt = 0;
        Queue<Integer> temp = new LinkedList<>(q);

        while(!temp.isEmpty()) {

            if(temp.poll() == k)
                cnt++;
        }

        return (cnt > 0) ? cnt : -1;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Queue<Integer> q = new LinkedList<>();
        boolean flag = true;

        do {

            System.out.println("\n1 : enQueue");
            System.out.println("2 : findFrequency");
            System.out.println("3 : Exit");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1: {
                    System.out.print("\nEnter data :: ");
                    int data = sc.nextInt();

                    insert(q, data);
                }
                    break;

                case 2: {
                    
                    System.out.print("\nEnter no :: ");
                    int key = sc.nextInt();
                    System.out.println(findFrequency(q, key));
                }
                    break;

                case 3:
                    flag = false;
                    break;

                default:
                    System.out.println("\nWrong Choice...!!!");
                    break;
            }

        } while (flag);

        sc.close();
    }
}