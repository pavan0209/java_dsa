/*
    Q4. Stack using two queues
    
    Implement a Stack using two queues q1 and q2.
    
    Example 1:
    Input:
        push(2)
        push(3)
        pop()
        push(4)
        pop()
    Output: 3 4
    Explanation:
        push(2) the stack will be {2}
        push(3) the stack will be {2 3}
        pop() poped element will be 3 the
        stack will be {2}
        push(4) the stack will be {2 4}
        pop() poped element will be 4
    
    Example 2:
    Input:
        push(2)
        pop()
        pop()
        push(3)
    Output: 2 -1
    
    Expected Time Complexity: O(1) for push() and O(N) for pop() (or vice-versa).
    Expected Auxiliary Space: O(1) for both push() and pop().
    
    Constraints:
        1 <= Number of queries <= 100
        1 <= values of the stack <= 100
 */

import java.util.*;

class Solution {

    Queue<Integer> q1 = new LinkedList<Integer>();
    Queue<Integer> q2 = new LinkedList<Integer>();

    void push(int a) {

        q1.offer(a);
        int size = q1.size();

        while (size > 1) {

            q1.offer(q1.poll());
            size--;
        }
    }

    int pop() {

        if (q1.isEmpty())
            return -1;

        return q1.poll();
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Solution sol = new Solution();
        boolean flag = true;

        do {

            System.out.println("\n1 : enQueue");
            System.out.println("2 : deQueue");
            System.out.println("3 : Exit");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1: {
                    System.out.print("\nEnter data :: ");
                    int data = sc.nextInt();

                    sol.push(data);
                }
                    break;

                case 2: {
                    int ret = sol.pop();
                    if (ret == -1)
                        System.out.println("\nStack is Empty...!!");
                    else
                        System.out.println("\n" + ret + " is popped");
                }
                    break;

                case 3:
                    flag = false;
                    break;

                default:
                    System.out.println("\nWrong Choice...!!!");
                    break;
            }

        } while (flag);

        sc.close();
    }
}