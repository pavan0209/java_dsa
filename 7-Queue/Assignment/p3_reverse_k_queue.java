/*
    Q3. Reverse First K elements of Queue
    
    Given an integer K and a queue of integers, we need to reverse the order of the first K elements of the
    queue, leaving the other elements in the same relative order.
    
    Only following standard operations are allowed on queue.
    enqueue(x) : Add an item x to rear of queue
    dequeue() : Remove an item from front of queue
    size() : Returns number of elements in queue.
    front() : Finds front item.

    Note: The above operations represent the general processings. In-built functions of the respective languages
    can be used to solve the problem.
    
    Example 1:
    Input:
        5 3
        1 2 3 4 5
    Output:
        3 2 1 4 5
    Explanation:
    After reversing the given input from the 3rd position the resultant output will be 3 2 1 4 5.
    
    Example 2:
    Input:
        4 4
        4 3 2 1
    Output:
        1 2 3 4
    Explanation:
    After reversing the given input from the 4th position the resultant output will be 1 2 3 4.
    
    Expected Time Complexity : O(N)
    Expected Auxiliary Space : O(K)
    
    Constraints:
        1 <= N <= 1000
        1 <= K <= N
 */

import java.util.*;

class Solution {

    /*static Queue<Integer> modifyQueue(Queue<Integer> q, int k) {

        Stack<Integer> m = new Stack<>();
        Queue<Integer> u = new LinkedList<>();

        int i = 1;
        while(!q.isEmpty()) {
            if(i <= k)
                m.push(q.poll());
            else
                u.offer(q.poll());
            
            i++;
        }

        System.out.println(m + "  " + u);

        while (!m.isEmpty()) 
            q.offer(m.pop());
        
        while (!u.isEmpty()) 
            q.offer(u.poll());

        return q;
    }
    */

    static Queue<Integer> modifyQueue(Queue<Integer> q, int k) {

        Stack<Integer> s = new Stack<>();
        Queue<Integer> que = new LinkedList<>();

        while (!q.isEmpty() && k > 0) {

            s.push(q.poll());
            k--;
        }

        while (!s.isEmpty())
            que.offer(s.pop());

        while (!q.isEmpty())
            que.offer(q.poll());

        return que;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Queue<Integer> q = new LinkedList<>();

        System.out.print("Size of queue :: ");
        int size = sc.nextInt();

        for (int i = 0; i < size; i++) {

            System.out.print("Enter data : ");
            int data = sc.nextInt();
            q.offer(data);
        }

        System.out.print("\nEnter k :: ");
        int k = sc.nextInt();

        System.out.println(q);
        q = modifyQueue(q, k);
        System.out.println(q);
        sc.close();
    }
}