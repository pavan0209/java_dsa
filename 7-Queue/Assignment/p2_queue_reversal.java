/*
    Q2. Queue Reversal
    
    Given a Queue Q containing N elements. The task is to reverse the Queue. 
    Your task is to complete the function rev(), that reverses the N elements of the queue.
    
    Example 1:
    Input:
        6
        4 3 1 10 2 6
    Output:
        6 2 10 1 3 4
    Explanation:
    After reversing the given elements of the queue , the resultant queue will be 6 2 101 3 4.
    
    Example 2:
    Input:
        4
        4 3 2 1
    Output:
        1 2 3 4
    Explanation:
    After reversing the given elements of the queue , the resultant queue will be 1 2 3 4.
    
    Expected Time Complexity : O(n)
    Expected Auxiliary Space : O(n)
    
    Constraints:
        1 ≤ N ≤ 105
        1 ≤ elements of Queue ≤ 105
 */

import java.util.*;

class Solution {

    static Queue<Integer> rev(Queue<Integer> q) {

        Stack<Integer> s = new Stack<>();

        while (!q.isEmpty())
            s.add(q.poll());

        while (!s.isEmpty())
            q.add(s.pop());

        return q;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Queue<Integer> q = new LinkedList<>();

        System.out.print("\nEnter no of elements to be added in queue :: ");
        int size = sc.nextInt();

        for (int i = 0; i < size; i++) {

            System.out.print("Enter data :: ");
            int data = sc.nextInt();
            q.offer(data);
        }

        System.out.println(q);

        q = rev(q);

        System.out.println(q);
        sc.close();
    }
}