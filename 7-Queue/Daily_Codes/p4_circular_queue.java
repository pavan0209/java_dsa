/*
 *  Implementation Of Circular Queue
 */

import java.util.*;

class CircularQueue {

    int size;
    int queArr[];
    int front;
    int rear;

    CircularQueue(int size) {

        this.size = size;
        queArr = new int[size];
        front = -1;
        rear = -1;
    }

    void enQueue(int data) {

        if ((front == 0 && rear == size - 1) || ((rear + 1) % size == front)) {

            System.out.println("\nQueue is full....!!");
            return;
        } else if (front == -1)
            front = rear = 0;
        else if (rear == size - 1 && front != 0)
            rear = 0;
        else
            rear++;

        queArr[rear] = data;
    }

    int flag = 0;

    int deQueue() {

        if (front == -1) {

            flag = 0;
            return -1;
        }

        int retVal = queArr[front];
        flag = 1;
        if (front >= rear)
            rear = front = -1;
        else if (front == size - 1)
            front = 0;
        else
            front++;

        return retVal;
    }

    void printQueue() {

        if(front == -1) {
            System.out.println("\nQueue is empty...!!!");
            return;
        }
        System.out.println();

        if (front <= rear) {

            for (int i = front; i <= rear; i++)
                System.out.print(queArr[i] + "  ");
        } else {

            for (int i = front; i < size; i++)
                System.out.print(queArr[i] + "  ");

            for (int i = 0; i <= rear; i++)
                System.out.print(queArr[i] + "  ");
        }

        System.out.println();
    }
}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("\nEnter Size for Queue :: ");
        int size = sc.nextInt();

        CircularQueue cque = new CircularQueue(size);

        boolean flag = true;

        do {

            System.out.println("\n1 : enqueue");
            System.out.println("2 : dequeue");
            System.out.println("3 : printQueue");
            System.out.println("4 : Exit");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1: {
                    System.out.print("\nEnter data :: ");
                    int data = sc.nextInt();

                    cque.enQueue(data);
                }
                    break;

                case 2: {
                    int val = cque.deQueue();
                    if (cque.flag == 0)
                        System.out.println("\nQueue is empty...!!");
                    else
                        System.out.println("\n" + val + " popped..");
                }
                    break;

                case 3:
                    cque.printQueue();
                    break;

                case 4:
                    flag = false;
                    break;

                default:
                    System.out.println("\nWrong Choice....!!!");
                    break;
            }

        } while (flag);

        sc.close();
    }
}