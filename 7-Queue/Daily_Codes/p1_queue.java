/*
 *  Implementation of QUEUE using Predefined class.
 */

import java.util.*;

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        Queue<Integer> q = new LinkedList<>();

        q.offer(10);
        q.offer(20);
        q.offer(30);
        q.add(40);

        System.out.println(q.peek());
        System.out.println(q.element());

        System.out.println(q);

        System.out.println(q.poll() + " popped");
        System.out.println(q);

        System.out.println(q.remove() + " popped");
        System.out.println(q);

        sc.close();
    }
}