/*
 *  Implementation Of QUEUE using Linked List.
 */

import java.util.*;

class Node {

    int data;
    Node next;

    Node(int data) {
        
        this.data = data;
        next = null;
    }
}

class Queue {

    Node front = null;
    Node rear = null;

    void enqueue(int data) {

        Node newNode = new Node(data);

        if(front == null) {

            front = newNode;
            rear = newNode; 
        }
        else {

            rear.next = newNode;
            rear = newNode;
        }
    }

    int flag = 0;
    int dequeue() {

        if(front == null) {
            flag = 0;
            return -1;
        }

        flag = 1;
        int data = front.data;
        front = front.next;

        return data;
    }

    boolean empty() {

        if(front == null)
            return true;
        else
            return false;
    }

    int frontt() {

        if(front == null) {
            flag = 0;
            return -1;
        }

        flag = 1;
        return front.data;
    }

    int rear() {

        if(front == null) {
            flag = 0;
            return -1;
        }
        
        flag = 1;
        return rear.data;
    }

    void printQueue() {

        if(front == null) {

            System.out.println("\nQueue is empty..!!");
            return;
        }

        Node temp = front;
        System.out.println();

        while(temp != null) {

            System.out.print(temp.data + "  ");
            temp = temp.next;
        }

        System.out.println();
    }
}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        Queue que = new Queue();
        char ch = 'N';

        do {

            System.out.println("\n1 : insert element in queue(enqueue)");
            System.out.println("2 : delete element from queue(dequeue)");
            System.out.println("3 : view element at front");
            System.out.println("4 : view element at rear");
            System.out.println("5 : Check whether the queue is empty or not");
            System.out.println("6 : print elements of Queue");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();
                            
                            que.enqueue(data);
                        }
                        break;

                case 2 :
                        {
                            int ret = que.dequeue();

                            if(que.flag == 0) 
                                System.out.println("\nQueue is Empty...!!");
                            else 
                                System.out.println("\n" + ret + " is popped from queue");
                        }
                        break;

                case 3 :
                        {
                            int retVal = que.frontt();
                            
                            if(que.flag == 0)
                                System.out.println("\nQueue is empty...!!");
                            else
                                System.out.println("\nElement at front :: " + retVal);
                        }
                        break;
                
                case 4 :
                        {
                            int retVal = que.rear();
                            
                            if(que.flag == 0)
                                System.out.println("\nQueue is empty...!!");
                            else
                                System.out.println("\nElement at front :: " + retVal);
                        }
                        break;

                case 5 : 
                        {
                            if(que.empty())
                                System.out.println("\nQueue is empty..!!");
                            else
                                System.out.println("\nQueue is not empty");
                        }
                        break;

                case 6 :
                        que.printQueue();
                        break;
            
                default:
                        System.out.println("\nWrong Choice..!!!");
                        break;
            }

            System.out.print("\nDo you want to continue?? :: ");
            ch = sc.next().charAt(0);

        }while(ch == 'Y' || ch == 'y');
        
        sc.close();
    }
}