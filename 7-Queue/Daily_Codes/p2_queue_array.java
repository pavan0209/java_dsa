/*
 *  Implementation Of QUEUE using Array.
 */

import java.util.*;

class Queue {

    int size;
    int queArr[];
    int front;
    int rear;

    Queue(int size) {

        this.size = size;
        queArr = new int[size];
        front = -1;
        rear = -1;
    }

    void enqueue(int data) {

        if(rear == size-1) {
            
            System.out.println("\nQueue is full");
            return;
        }

        if(front == -1) 
            front = rear = 0;
        else 
            rear++; 

        queArr[rear] = data;
    }

    int flag = 0;
    int dequeue() {

        if(front == -1) {
            flag = 0;
            return -1;
        }

        flag = 1;
        int data = queArr[front];
        front++;

        if(front > rear)
            front = rear = -1;

        return data;
    }

    boolean empty() {

        if(front == -1)
            return true;
        else
            return false;
    }

    int front() {

        if(front == -1) {
            flag = 0;
            return -1;
        }

        flag = 1;
        return queArr[front];
    }

    int rear() {

        if(front == -1) {
            flag = 0;
            return -1;
        }
        
        flag = 1;
        return queArr[rear];
    }

    void printQueue() {

        if(front == -1) {

            System.out.println("\nQueue is empty..!!");
            return;
        }

        System.out.println();
        for(int i = front; i <= rear; i++) {

            System.out.print(queArr[i] + "  ");
        }
        System.out.println();
    }
}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("\nEnter Size for queue :: ");
        int size = sc.nextInt();

        Queue que = new Queue(size);
        char ch = 'N';

        do {

            System.out.println("\n1 : insert element in queue(enqueue)");
            System.out.println("2 : delete element from queue(dequeue)");
            System.out.println("3 : view element at front");
            System.out.println("4 : view element at rear");
            System.out.println("5 : Check whether the queue is empty or not");
            System.out.println("6 : print elements of Queue");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();
                            
                            que.enqueue(data);
                        }
                        break;

                case 2 :
                        {
                            int ret = que.dequeue();

                            if(que.flag == 0) 
                                System.out.println("\nQueue is Empty...!!");
                            else 
                                System.out.println("\n" + ret + " is popped from queue");
                        }
                        break;

                case 3 :
                        {
                            int retVal = que.front();
                            
                            if(que.flag == 0)
                                System.out.println("\nQueue is empty...!!");
                            else
                                System.out.println("\nElement at front :: " + retVal);
                        }
                        break;
                
                case 4 :
                        {
                            int retVal = que.rear();
                            
                            if(que.flag == 0)
                                System.out.println("\nQueue is empty...!!");
                            else
                                System.out.println("\nElement at front :: " + retVal);
                        }
                        break;

                case 5 : 
                        {
                            if(que.empty())
                                System.out.println("\nQueue is empty..!!");
                            else
                                System.out.println("\nQueue is not empty");
                        }
                        break;

                case 6 :
                        que.printQueue();
                        break;
            
                default:
                        System.out.println("\nWrong Choice..!!!");
                        break;
            }

            System.out.print("\nDo you want to continue?? :: ");
            ch = sc.next().charAt(0);

        }while(ch == 'Y' || ch == 'y');
        
        sc.close();
    }
}