/*
 *  Given an Array of Size N.
 *  Return the count of pairs(i, j) with arr[i] + arr[j] = k;
 * 
 *  arr[] = [3,5,2,1,-3,7,8,15,6,13]
 *  N = 10
 *  K = 10
 *  op : 6
 *  
 * Note : i != j 
 */

class Solution {

    static int countPairs(int arr[], int N, int k) {

        int pairCount = 0;

        for (int i = 0; i < N; i++) {

            for (int j = 0; j < N; j++) {

                if (i != j && arr[i] + arr[j] == k)
                    pairCount++;
            }
        }

        return pairCount;
    }

    public static void main(String[] args) {

        int arr[] = { 3, 5, 2, 1, -3, 7, 8, 15, 6, 13 };

        System.out.println(Solution.countPairs(arr, arr.length, 10));

    }
}