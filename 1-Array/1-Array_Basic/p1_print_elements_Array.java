/*
 *  Given an array of size N. Print all the elements .
 *  Arr : [5,6,2,3,1,9]
 */

class Solution {

    static void printArray(int arr[], int N) {

        for (int i = 0; i < N; i++) {

            System.out.print(arr[i] + "  ");
        }

        System.out.println();
    }

    public static void main(String[] args) {

        int arr[] = { 5, 6, 2, 3, 1, 9 };

        Solution.printArray(arr, arr.length);

    }
}