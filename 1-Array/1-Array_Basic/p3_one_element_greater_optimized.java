/*
 *  Given an integer array of size N.
 *  Count the no. of elements having at least one element greater than itself.
 *  
 *  Arr[] = [2,5,1,4,8,0,8,1,3,8]
 *  N : 10
 *  op : 7
 */

class Solution {

    static int oneElementGreater(int arr[], int N) {

        int max = Integer.MIN_VALUE;
        int maxCnt = 0;

        for (int i = 0; i < N; i++) {

            if (arr[i] > max) {
                max = arr[i];
                maxCnt = 0;
            }

            if (arr[i] == max)
                maxCnt++;

        }

        return N - maxCnt;

        // Sorting Approach
        /*
         * int cnt = 1;
         * java.util.Arrays.sort(arr);
         * 
         * for(int i = 0; i < N-1; i++) {
         * 
         * if(arr[i] < arr[i+1])
         * cnt++;
         * }
         * 
         * if(N > 1 && (arr[N-1] > arr[N-2]))
         * cnt++;
         * 
         * return cnt;
         */
    }

    public static void main(String[] args) {

        int arr[] = { 2, 5, 1, 4, 8, 0, 8, 1, 3, 8 };

        System.out.println(Solution.oneElementGreater(arr, arr.length));
    }
}