/*
 *  Given an array of size N.
 * 
 *  Reverse the array without using extra space
 * 
 *  ip : Arr[] = {8,4,1,3,9,2,6,7}
 *  op : Arr[] = {7,6,2,9,3,1,4,8}
 * 
 */

class Solution {

    static void revArray(int arr[], int N) {

        int j = N - 1;
        int i = 0;

        while (i <= j) {

            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;

            i++;
            j--;
        }
    }

    public static void main(String[] args) {

        int arr[] = { 8, 4, 1, 3, 9, 2, 6, 7 };

        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        System.out.println();

        Solution.revArray(arr, arr.length);

        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        System.out.println();
    }
}