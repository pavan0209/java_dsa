/*
 *  Given an integer array of size N.
 *  Count the no. of elements having at least one element greater than itself.
 *  
 *  Arr[] = [2,5,1,4,8,0,8,1,3,8]
 *  N : 10
 *  op : 7
 */

class Solution {

    static int oneElementGreater(int arr[], int N) {

        int cnt = 0;

        for (int i = 0; i < N; i++) {

            for (int j = 0; j < N; j++) {

                if (arr[i] < arr[j]) {
                    cnt++;
                    break;
                }
            }
        }

        return cnt;
    }

    public static void main(String[] args) {

        int arr[] = { 2, 5, 1, 4, 8, 0, 8, 1, 3, 8 };

        System.out.println(Solution.oneElementGreater(arr, arr.length));
    }
}