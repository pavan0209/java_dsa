/*
 *  Second Largest element in the array
 *  
 *  ip : arr[] = {9,8,4,1,3,9,2,6,7}
 *  op : 8
 */

class Solution {

    static int secLargest(int arr[], int N) {

        int max = Integer.MIN_VALUE;
        int secMax = Integer.MIN_VALUE;

        for (int i = 0; i < N; i++) {

            if (arr[i] > max)
                max = arr[i];
        }

        for (int i = 0; i < N; i++) {

            if (arr[i] > secMax && arr[i] < max)
                secMax = arr[i];
        }

        return secMax;
    }

    public static void main(String[] args) {

        int arr[] = { 9, 8, 4, 1, 3, 9, 2, 6, 7, 10, 21 };

        System.out.println(Solution.secLargest(arr, arr.length));
    }
}