/*
    Given an character array (lowercase) return the count of pair (i, j) such that
    
    a) i < j
    b) arr[i] == 'a'
       arr[j] == 'g'

    input :
        arr[] = [a,b,e,g,a,g]
    output :
        3
 */

class Solution {

    // Optimized Approach :
    // T.C : O(N)

    static int countPairs(char arr[], int N) {

        int pair = 0;
        int countA = 0;

        for (int i = 0; i < N; i++) {

            if (arr[i] == 'a')
                countA++;

            if (arr[i] == 'g')
                pair += countA;
        }

        return pair;
    }

    public static void main(String[] args) {

        char arr[] = { 'a', 'b', 'e', 'g', 'a', 'g' };

        System.out.println(countPairs(arr, arr.length));
    }
}