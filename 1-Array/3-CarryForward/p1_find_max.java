/*
    Maximum in range

    Arr[] = [3,4,5,1,2,7,8,9]
    N = 3
    K = 3
 */

class Solution {

    static int findMax(int arr[], int N, int K) {

        int max = Integer.MIN_VALUE;

        if (K < 0 || K >= N)
            return -1;

        for (int i = 0; i <= K; i++) {

            if (arr[i] > max)
                max = arr[i];
        }

        return max;
    }

    public static void main(String[] args) {

        int arr[] = { 3, 4, 5, 1, 2, 7, 8, 9 };

        System.out.println(findMax(arr, arr.length, 7));
    }
}