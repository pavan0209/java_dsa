/*
    Given an array of size N
    Build an array leftmax of size N

    leftmax of i contains the maximum for the index 0 to index i

    input :
        Arr[] = [-3,6,2,4,5,2,8,-9,3,1]
        N = 10
    output :
        leftMax = [-3,6,6,6,6,6,8,8,8,8]
 */

class Solution {

    // Optimized Approach
    // T.C : O(N)
    // S.C : O(N)

    static int[] findLeftMax(int arr[], int N) {

        int leftMax[] = new int[N];
        leftMax[0] = arr[0];

        for (int i = 1; i < N; i++) {

            if (leftMax[i - 1] < arr[i])
                leftMax[i] = arr[i];
            else
                leftMax[i] = leftMax[i - 1];

        }
        return leftMax;
    }

    public static void main(String[] args) {

        int arr[] = { -3, 6, 2, 4, 5, 2, 8, -9, 3, 1 };

        System.out.println(java.util.Arrays.toString(arr));
        int leftMax[] = findLeftMax(arr, arr.length);
        System.out.println(java.util.Arrays.toString(leftMax));
    }
}