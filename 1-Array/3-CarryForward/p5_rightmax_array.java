/*
    Given an array of size N
    Build an array rightmax of size N

    rightmax of i contains the maximum for the index i to index N-1

    input :
        Arr[] = [-3,6,2,4,5,2,8,-9,3,1]
        N = 10
    output :
        rightmax = [8,8,8,8,8,8,8,3,3,1]
 */

class Solution {

    // Optimized Approach
    // T.C : O(N)
    // S.C : O(N)

    static int[] findRightMax(int arr[], int N) {

        int rightMax[] = new int[N];
        rightMax[N - 1] = arr[N - 1];

        for (int i = N - 2; i >= 0; i--) {

            if (rightMax[i + 1] < arr[i])
                rightMax[i] = arr[i];
            else
                rightMax[i] = rightMax[i + 1];
        }
        return rightMax;
    }

    public static void main(String[] args) {

        int arr[] = { -3, 6, 2, 4, 5, 2, 8, -9, 3, 1 };

        System.out.println(java.util.Arrays.toString(arr));

        int rightMax[] = findRightMax(arr, arr.length);
      
        System.out.println(java.util.Arrays.toString(rightMax));
    }
}