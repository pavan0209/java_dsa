/*
    Given an character array (lowercase) return the count of pair (i, j) such that
    
    a) i < j
    b) arr[i] == 'a'
       arr[j] == 'g'

    input :
        arr[] = [a,b,e,g,a,g]
    output :
        3
 */

class Solution {

    // BruteForce Aprroach
    // T.C : O(N*N)

    static int countPairs(char arr[], int N) {

        int cnt = 0;

        for (int i = 0; i < N; i++) {
            
            if (arr[i] == 'a') {
                for (int j = i + 1; j < N; j++) {
                    if (arr[j] == 'g')
                        cnt++;
                }
            }
        }

        return cnt;
    }

    public static void main(String[] args) {

        char arr[] = { 'a', 'b', 'e', 'g', 'a', 'g' };

        System.out.println(countPairs(arr, arr.length));
    }
}