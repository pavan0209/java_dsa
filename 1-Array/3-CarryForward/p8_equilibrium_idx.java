/*
 *  Find Equilibrium index
 */

class Solution {

    static int eqIndex(int arr[], int N) {

        int leftSum = 0;

        for(int i = 0; i < N; i++) {

            int rightSum = 0;

            for(int j = i+1; j < N; j++) 
                rightSum += arr[j];
            
            if(leftSum == rightSum)
                return i+1;

            leftSum += arr[i];
        }

        return -1;
    }

    /*static int eqIndex(int arr[], int N) {

        for(int i = 0; i < N; i++) {

            int leftSum = 0;
            int rightSum = 0;

            for(int j = 0; j < i; j++)
                leftSum += arr[j];

            for(int j = i+1; j < N; j++)
                rightSum += arr[j];

            if(leftSum == rightSum)
                return i;
        }

        return -1;
    }*/

    public static void main(String[] args) {

        //int arr[] = {-7, 1, 5, 2, -4, 3, 0};
        int arr[] = {1};
        
        System.out.println(eqIndex(arr, arr.length));
    }
}