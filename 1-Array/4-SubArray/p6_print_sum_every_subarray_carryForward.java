/*
 *  Given an array of size N
 *  
 *  Print the sum of every single subarray with time complexity O(N^2) and without using extra space(carryForward).
 *  
 *      arr[] = {2,4,1,3}
 *      op :  2, 6, 7, 10, 4, 5, 8, 1, 4, 3
 *   
 *      Explaination :
 *          {2}         => 2
 *          {2,4}       => 6
 *          {2,4,1}     => 7
 *          {2,4,1,3}   => 10     
 *          {4}         => 4
 *          {4,1}       => 5
 *          {4,1,3}     => 8
 *          {1}         => 1
 *          {1,3}       => 4
 *          {3}         => 3
 */

class Solution {

    static void sumSubArray(int arr[], int N) {

        for (int i = 0; i < N; i++) {

            int sum = 0;
            for (int j = i; j < N; j++) {

                sum += arr[j];
                System.out.println(sum);
            }
        }
    }

    public static void main(String[] args) {

        int arr[] = { 2, 4, 1, 3 };

        Solution.sumSubArray(arr, arr.length);
    }
}