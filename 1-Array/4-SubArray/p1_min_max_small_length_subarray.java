/*
 *  Given an integer array of size N.
 *  return the length of the smallest subarray which contains both the maximum of the array and minimum of the 
 *  array.
 * 
 *  Arr : [1,2,3,1,3,4,6,4,6,3]
 *  op : 4
 */

class Solution {

    static int shortestSubArray(int arr[], int N) {

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for(int i = 0; i < N; i++) {

            if(arr[i] < min)
                min = arr[i];

            if(arr[i] > max)
                max = arr[i];
        }

        int minLength = Integer.MAX_VALUE;
        int len = 0;

        for(int i = 0; i < N; i++) {

            if(arr[i] == min) {
                for(int j = i+1; j < N; j++) {

                    if(arr[j] == max) {

                        len = j - i + 1;
                        if(minLength > len)
                            minLength = len;
                    }
                }
            }
            else if(arr[i] == max) {
                for(int j = i+1; j < N; j++) {

                    if(arr[j] == min) {

                        len = j - i + 1;
                        if(minLength > len)
                            minLength = len;
                    }
                }
            }
        }
        return minLength;
    }

    public static void main(String[] args) {

        int arr[] = {1,2,3,1,3,4,6,4,6,3};

        System.out.println(Solution.shortestSubArray(arr, arr.length));
    }
}