/*
 *  Find out thr number of subarray in the given array
 *  
 *  Arr[] = {4,2,10,3,12,-2,15}
 *  op : 28
 */

class Solution {

    static int countSubArrays(int arr[], int N) {

        int cnt = 0;

        for (int i = 0; i < N; i++) {

            for (int j = i; j < N; j++) {

                cnt++;
            }
        }

        return cnt;
    }

    public static void main(String[] args) {

        int arr[] = { 4, 2, 10, 3, 12, -2, 15 };

        System.out.println(Solution.countSubArrays(arr, arr.length));
    }
}