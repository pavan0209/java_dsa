/*
 *  Maximum SubArray Sum :
 *  
 *  Given an integer of size N.
 *  Find the contiguous subarray (containing at least one numbers) has the largest sum and return its sum.
 *  
 *  input : [-2,1,-3,4,-1,2,1,-5,4]
 *  output : [4,-1,2,1]
 */

class Solution {

    static void maxSubArraySum(int arr[], int N) {

        int maxSum = Integer.MIN_VALUE;
        int sum = 0;
        int temp = 0;
        int start = -1, end = -1;

        for (int i = 0; i < N; i++) {

            sum += arr[i];

            if(sum > maxSum) {
                start = temp;
                maxSum = sum;
                end = i;
            }

            if(sum < 0) {  
                sum = 0;
                temp = i+1;
            }
        }

        for(int i = start; i <= end; i++) {

            System.out.print(arr[i] + "  ");
        }

    }

    public static void main(String[] args) {

        int arr[] = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };

        Solution.maxSubArraySum(arr, arr.length);

        System.out.println();
    }
}