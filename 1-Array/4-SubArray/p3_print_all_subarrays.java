/*
 *  Given an array of size N
 *  print all the subarrays 
 * 
 *  int arr[] = {2,4,1,3}
 * 
 *  op :
 *      2  2,4  2,4,1  2,4,1,3  4  4,1  4,1,3  1  1,3  3
 */

class Solution {

    static void printSubArrays(int arr[], int N) {

        for (int i = 0; i < N; i++) {

            for (int j = i; j < N; j++) {

                for (int k = i; k <= j; k++) {

                    if (k != j)
                        System.out.print(arr[k] + ", ");
                    else
                        System.out.println(arr[k]);
                }
            }

            System.out.println();
        }
    }

    public static void main(String[] args) {

        int arr[] = { 2, 4, 1, 3 };

        Solution.printSubArrays(arr, arr.length);
    }
}