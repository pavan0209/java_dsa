/*
 *  Given an array of size N.
 *  
 *  print all the elements in a given subarray from start to end.
 * 
 *  arr[] = {-2,1,-3,4,-1,2,1,-5,4}
 *  start = 3
 *  end = 7
 */

class Solution {

    static void printArray(int arr[], int N, int start, int end) {

        if (start < 0 || start >= N || end < 0 || end >= N)
            return;

        for (int i = start; i <= end; i++) {

            System.out.print(arr[i] + "  ");
        }
    }

    public static void main(String[] args) {

        int arr[] = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };

        Solution.printArray(arr, arr.length, 3, 7);
    }
}