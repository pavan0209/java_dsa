/*
 *  Maximum SubArray Sum :
 *  
 *  Given an integer of size N.
 *  Find the contiguous subarray (containing at least one numbers) has the largest sum and return its sum.
 *  
 *  input : [-2,1,-3,4,-1,2,1,-5,4]
 *  output : 6
 * 
 *  Explaination : [4,-1,2,1] has the largest sum sum = 6
 */

class Solution {

    static int maxSubArraySum(int arr[], int N) {

        // Optimized Approach
        // time Complexity = O(N)
        // Space complexity= O(1)

        int maxSum = Integer.MIN_VALUE;
        int sum = 0;

        for (int i = 0; i < N; i++) {

            sum += arr[i];

            if (sum > maxSum)
                maxSum = sum;

            if (sum < 0)
                sum = 0;
        }

        return maxSum;
    }

    public static void main(String[] args) {

        int arr[] = { 1, -2, 3 };
        System.out.println(Solution.maxSubArraySum(arr, arr.length));
    }
}