/*
 *  Given an array of size N.
 *  Find the total sum of all the subarray sum
 *  
 *  arr[] = {1,2,3}
 *  op : 20
 *  
 *  Explaination :
 * 
 *      {1}      => 1
 *      {1,2}    => 3
 *      {1,2,3}  => 6
 *      {2}      => 2
 *      {2,3}    => 5
 *      {3}      => 3
 */

class Solution {

    static int totalSumSubArray(int arr[], int N) {

        int totSum = 0;

        for (int i = 0; i < N; i++) {

            int sum = 0;

            for (int j = i; j < N; j++) {

                sum += arr[j];
                totSum += sum;
            }
        }

        return totSum;
    }

    public static void main(String[] args) {

        int arr[] = { 1, 2, 3 };

        System.out.println(Solution.totalSumSubArray(arr, arr.length));
    }
}