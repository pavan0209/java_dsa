/*
 *  Given an array of size N
 *  
 *  Print the sum of every single subarray using prefix sum technique
 *  
 *      arr[] = {2,4,1,3}
 *      op :  2, 6, 7, 10, 4, 5, 8, 1, 4, 3
 *   
 *      Explaination :
 *          {2}         => 2
 *          {2,4}       => 6
 *          {2,4,1}     => 7
 *          {2,4,1,3}   => 10     
 *          {4}         => 4
 *          {4,1}       => 5
 *          {4,1,3}     => 8
 *          {1}         => 1
 *          {1,3}       => 4
 *          {3}         => 3
 */

class Solution {

    static void sumSubArray(int arr[], int N) {

        int prefSum[] = new int[N];

        prefSum[0] = arr[0];

        for (int i = 1; i < N; i++) {

            prefSum[i] = prefSum[i - 1] + arr[i];
        }

        int sum = 0;

        for (int i = 0; i < N; i++) {

            for (int j = i; j < N; j++) {

                if (i == 0)
                    sum = prefSum[j];
                else
                    sum = prefSum[j] - prefSum[i - 1];

                System.out.println(sum);
            }
        }
    }

    public static void main(String[] args) {

        int arr[] = { 2, 4, 1, 3 };

        Solution.sumSubArray(arr, arr.length);
    }
}