/*
 *  Sum of Array Elements in a given range.
 *  
 *      Arr[] = [2,5,3,11,7,9]
 */

import java.util.Scanner;

class Solution {

    static int sumArrayRange(int arr[], int N, int st, int end) {

        int sum = 0;

        if(st < 0 || st > N-1 || end < 0 || end >= N)
            return -1;
            
        for(int i = st; i <= end; i++) 
            sum += arr[i];

        return sum;
    }   

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int arr[] = {2,5,3,110,7,9};

        System.out.print("Enter start index :: ");
        int st = sc.nextInt();

        System.out.print("Enter Last index :: ");
        int end = sc.nextInt();

        System.out.println(Solution.sumArrayRange(arr, arr.length, st, end));

        sc.close();
    }
}