/*
    In Place Prefix Sum

    - Given an array A of N integers
    - Construct prefix sum of an array in the given array itself
    - Return an array of integers denoting the prefix sum of given array

    Constraints :

        1 <= N <= 10^5
        1 <= A[i] <= 10^3

    input 1:
        A = [1,2,3,4,5]
    output 1:
        [1,3,6,10,15]
 */

class Solution {

    static int[] prefixSum(int arr[], int N) {

        for(int i = 1; i < N; i++) 
            arr[i] = arr[i-1] + arr[i];

        return arr;
    }

    public static void main(String[] args) {

        int arr[] = {1,2,3,4,5};

        System.out.println(java.util.Arrays.toString(arr));
        arr = prefixSum(arr, arr.length);
        System.out.println(java.util.Arrays.toString(arr));
    }
}