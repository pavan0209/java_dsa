/*
 *  Given an Array of size N and Q number of queries.
 * 
 *  Query contains two parameters (s,e). 
 *  s = startIndex
 *  e = endIndex
 *  
 *  Arr[-3,6,2,4,5,2,8,-9,3,1]
 *  N = 10
 *  Q = 3
 *  
 *  query      s     e      sum
 *    1        1     3       12
 *    2        2     7       12
 *    3        1     1        6
 */

import java.util.Scanner; 

class Solution {

    static void rangeSum(int arr[], int N, int Q) {

        Scanner sc = new Scanner(System.in);
        int newArr[] = new int[N];
        newArr[0] = arr[0];

        for(int i = 1; i < N; i++) 
            newArr[i] = newArr[i-1] + arr[i];

        for(int i = 0; i < Q; i++) {

            System.out.print("Enter start index :: ");
            int s = sc.nextInt();

            System.out.print("Enter last index :: ");
            int e = sc.nextInt();

            if(s < 0 || s >= N || e < 0 || e >= N)
                continue;

            if(s == 0){
                System.out.println("sum :: " + newArr[0]);
                continue;
            }
            System.out.println("sum :: " + (newArr[e]-newArr[s-1]));
        }

        sc.close();
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("\nEnter size of array :: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        for(int i = 0; i < size; i++) {

            System.out.print("Enter data :: ");
            arr[i] = sc.nextInt();
        }

        System.out.print("\nEnter number of queries :: ");
        int q = sc.nextInt();

        rangeSum(arr, size, q);

        sc.close();
    }
}