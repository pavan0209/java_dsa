/*
 *  Rotate Array
 */

class Solution {

    static void arrayRotate(int arr[], int N, int B) {

        for(int i = 0; i < B; i++) {

            int j = N-1;
            int temp = arr[N-1];

            while(j > 0) {
                arr[j] = arr[j-1];
                j--;
            }
            arr[0] = temp;
        }
    }

    public static void main(String[] args) {

        int arr[] = {1,2,3,4};

        System.out.println(java.util.Arrays.toString(arr));
        arrayRotate(arr, arr.length, 2);
        System.out.println(java.util.Arrays.toString(arr));
    }
}