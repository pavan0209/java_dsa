/*
 *  Iterate through entire matrix column by column
 * 
 *  ip : 1   2   3   4      op : 1   5   9
 *       5   6   7   8           2   6   10
 *       9   10  11  12          3   7   11
 *                               4   8   12  
 */

class Solution {

    static void printColByCol(int arr[][], int N, int M) {

        for (int i = 0; i < arr[0].length; i++) {

            for (int j = 0; j < arr.length; j++) {

                System.out.print(arr[j][i] + "  ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {

        int arr[][] = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 } };

        Solution.printColByCol(arr, arr.length, arr[0].length);
    }
}