/*
 *
 */

class Solution {

    static void printDiagonals(int arr[][], int N, int M) {

        for (int j = M - 1; j >= 0; j--) {

            int i = 0;
            int temp = j;

            while (i < N && temp >= 0) {

                System.out.print(arr[i][temp] + "  ");
                i++;
                temp--;
            }

            System.out.println();
        }
    }

    public static void main(String[] args) {

        int arr[][] = { { 1, 2, 3, 4, 5, 6 },
                { 7, 8, 9, 10, 11, 12 },
                { 13, 14, 15, 16, 17, 18 },
                { 19, 20, 21, 22, 23, 24 }
        };

        Solution.printDiagonals(arr, arr.length, arr[0].length);
    }
}