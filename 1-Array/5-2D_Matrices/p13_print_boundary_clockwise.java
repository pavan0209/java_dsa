/*
 *  Print Boundaries of Matrix
 */

class Solution {

    static void printBoundariesClockWise(int arr[][], int N) {

        for(int i = 0; i < N-1; i++) 
            System.out.print(arr[0][i] + "  ");

        for(int i = 0; i < N-1; i++)
            System.out.print(arr[i][N-1] + "  ");

        for(int i = 0; i < N-1; i++)
            System.out.print(arr[N-1][N-i-1] + "  ");

        for(int i = 0; i < N-1; i++)
            System.out.print(arr[N-i-1][0] + "  ");

        /*int j = 0, k = 0;

        for(int i = 0; i < N-1; i++) 
            System.out.print(arr[j][k++] + "  ");

        for(int i = 0; i < N-1; i++)
            System.out.print(arr[j++][k] + "  ");

        for(int i = 0; i < N-1; i++) 
            System.out.print(arr[j][k--] + "  ");

        for(int i = 0; i < N-1; i++)
            System.out.print(arr[j--][k] + "  ");*/
    }

    public static void main(String[] args) {

        int arr[][] = { { 1, 2, 3, 4, 5 },
                        { 6, 7, 8, 9, 10 },
                        { 11, 12, 13, 14, 15 },
                        { 16, 17, 18, 19, 20 },
                        { 21, 22, 23, 24, 25 }
                    };

        for (int i = 0; i < arr.length; i++) {

            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j] + "  ");
            }
            System.out.println();
        }

        System.out.println();

        Solution.printBoundariesClockWise(arr, arr.length);

        System.out.println("\n");
    }
}