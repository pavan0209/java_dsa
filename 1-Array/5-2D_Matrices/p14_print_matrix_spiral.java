/*
 *  Print Matrix in spiral fashion.
 */

class Solution {

    static void printMatrixSpiral(int arr[][], int N) {

        int n = 0;

        for (int temp = N-1 ; temp >= 0; temp -= 2) {

            int j = n, k=n;

            for (int i = 0; i < N - 1; i++)
                System.out.print(arr[j][k++] + "  ");

            for (int i = 0; i < N - 1; i++)
                System.out.print(arr[j++][k] + "  ");

            for (int i = 0; i < N - 1; i++)
                System.out.print(arr[j][k--] + "  ");

            for (int i = 0; i < N - 1; i++)
                System.out.print(arr[j--][k] + "  ");

            n++;
        }
    }

    public static void main(String[] args) {

        int arr[][] = { { 1, 2, 3, 4},
                { 5,6,7,8 },
                { 9,10,11,12 },
                { 13,14,15,16 }
        };

        for (int i = 0; i < arr.length; i++) {

            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j] + "  ");
            }
            System.out.println();
        }

        System.out.println();

        Solution.printMatrixSpiral(arr, arr.length);

        System.out.println("\n");
    }
}