/*
 *  Given a square matrix print right diagonal
 * 
 *      ip :  
 *          1   2   3
 *          4   5   6
 *          7   8   9
 * 
 *      op : 3  5 7      
 */

class Solution {

    static void printLeftDiagonal(int arr[][], int N) {

        //BruteForce Approach 
        /*for(int i = 0; i < N; i++) {

            for(int j = 0; j < N; j++) {

                if(i+j == N-1) 
                    System.out.print(arr[i][j] + "  ");
            }
        }
        System.out.println();*/

        //Optimized Approach

        for(int i = 0; i < N; i++) {

            System.out.print(arr[i][N-i-1] + "  ");
        }

        System.out.println();
    }

    public static void main(String[] args) {

        int arr[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

        Solution.printLeftDiagonal(arr, arr.length);
    }
}