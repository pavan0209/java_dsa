/*
 *  Print the sum of entire matrix
 * 
 *  ip : 1   2   3   4          op : 78
 *       5   6   7   8           
 *       9   10  11  12                                      
 */

class Solution {

    static void printColByCol(int arr[][], int N, int M) {

        int sum = 0;

        for (int i = 0; i < N; i++) {    

            for (int j = 0; j < M; j++) {

                sum += arr[i][j];
            }
        }

        System.out.println(sum);
    }

    public static void main(String[] args) {

        int arr[][] = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 } };

        Solution.printColByCol(arr, arr.length, arr[0].length);
    }
}