/*
 *  Print the row wise sum of entire matrix
 * 
 *  ip : 1   2   3   4      op : 10  26  42
 *       5   6   7   8           
 *       9   10  11  12                                      
 */

class Solution {

    static void printColByCol(int arr[][], int N, int M) {

        for (int i = 0; i < N; i++) {

            int sum = 0;

            for (int j = 0; j < M; j++) {

                sum += arr[i][j];
            }

            System.out.print(sum + "  ");
        }

        System.out.println();
    }

    public static void main(String[] args) {

        int arr[][] = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 } };

        Solution.printColByCol(arr, arr.length, arr[0].length);
    }
}