/*
 *  Given a 2D Matrix print all elements.
 */

class Solution {

    static void print2DArray(int arr[][], int N, int M) {

        for (int i = 0; i < N; i++) {

            for (int j = 0; j < M; j++) {

                System.out.print(arr[i][j] + "\t");
            }

            System.out.println();
        }
    }

    public static void main(String[] args) {

        int arr[][] = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 } };

        Solution.print2DArray(arr, arr.length, arr[0].length);
    }
}