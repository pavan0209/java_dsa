/*
 *  Q6. Product array puzzle
    Problem Description
    - Given an array of integers A, find and return the product array of the same size where the ith element of
    the product array will be equal to the product of all the elements divided by the ith element of the array.

    - Note: It is always possible to form the product array with integer (32 bit) values. Solve it without using
    the division operator.

    Constraints
        2 <= length of the array <= 1000
        1 <= A[i] <= 10

    For Example
    
    Input 1:  A = [1, 2, 3, 4, 5]
    Output 1: [120, 60, 40, 30, 24]
    
    Input 2:  A = [5, 1, 10, 1]
    Output 2: [10, 50, 5, 50]
 */

class Solution {

    static long[] productExceptSelf(int arr[], int n) {

        long res[] = new long[n];
        long prod = 1;

        for (int i = 0; i < n; i++) {

            for (int j = 0; j < n; j++) {

                if (j == i)
                    continue;

                prod = prod * arr[j];
            }

            res[i] = prod;
            prod = 1;
        }

        return res;
    }

    public static void main(String[] args) {

        int A[] = { 10, 3, 5, 6, 2 };

        long res[] = Solution.productExceptSelf(A, A.length);

        for (int i = 0; i < res.length; i++)
            System.out.print(res[i] + "  ");

        System.out.println();
    }
}