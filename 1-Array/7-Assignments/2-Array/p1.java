/*
 *  WAP to take character array as input, but only print characters do not print special characters.
 */

import java.io.*;

class Solution {

    String printChar(char[] arr) {

        String str = "";

        for(int i = 0; i < arr.length; i++) {

            if((arr[i] >= 65 && arr[i] <= 90) || (arr[i] >= 97 && arr[i] <= 122)) {

                str += arr[i];;
            }
        }
        return str;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter size for array :: ");
        int size = Integer.parseInt(br.readLine());

        char arr[] = new char[size];

        System.out.println("Enter array elements :: ");

        for(int i = 0; i < size; i++) {
            arr[i] = (char)br.read();
            br.skip(1);
        }

        Solution obj = new Solution();
        String res = obj.printChar(arr);

        for(int i = 0; i < res.length(); i++) {

            System.out.print(res.charAt(i) + " ");
        }

        System.out.println();
    }
}