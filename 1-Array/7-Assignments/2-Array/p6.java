/*
 *  WAP to remove a specific element from an array.
 */

import java.io.*;

class Solution {

    int removeElement(int arr[], int key) {

        int cnt = 0;

        for(int i = 0; i < arr.length-1; i++) {

            if(arr[i] == key) {

                cnt++;
                arr[i] = 0;
            }
        }

        if(arr[arr.length-1] == key)
            cnt++;

        return cnt;
    }   

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter size for array :: ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter array elements :: ");

        for(int i = 0; i < size; i++)
            arr[i] = Integer.parseInt(br.readLine());

        System.out.print("Enter element to remove from array :: ");
        int key = Integer.parseInt(br.readLine());

        Solution obj = new Solution();

        int cnt = obj.removeElement(arr, key);

        System.out.print("\nArray elements after removing element :: ");
        for(int i = 0; i < arr.length-cnt; i++) 
            System.out.print(arr[i] + " ");

        System.out.println();

    }
}