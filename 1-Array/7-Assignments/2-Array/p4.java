/*
 *  WAP to find sum of even and odd integers in a given array of integers.
 */

import java.io.*;

class Solution {

    int[] finEvenOdd(int arr[]) {

        int count[] = {0,0};

        for(int i = 0; i < arr.length; i++) {

            if(arr[i] % 2 != 0) 
                count[0] += arr[i];
            else
                count[1] += arr[i];
        }

        return count;
    }
    

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter size for array :: ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter array elements :: ");

        for(int i = 0; i < size; i++)
            arr[i] = Integer.parseInt(br.readLine());

        Solution obj = new Solution();
        
        int cnt[] = obj.finEvenOdd(arr);

        System.out.println("Odd Numbers Sum:: " + cnt[0]);
        System.out.println("Even Numbers Sum :: " + cnt[1]);

        System.out.println();
    }
}