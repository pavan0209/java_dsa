/*
 *  WAP to merge 2 given arrays
 */

import java.io.*;

class Solution {

    int[] mergeArray(int arr1[], int arr2[]) {

        int merge[] = new int[arr1.length + arr2.length];

        for(int i = 0; i < arr1.length; i++) {

            merge[i] = arr1[i];
        }

        int temp = arr1.length;
        for(int i = 0; i < arr2.length; i++) { 

            merge[temp++] = arr2[i];
        }

        return merge;
    }
    
    

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter size for array1 :: ");
        int size1 = Integer.parseInt(br.readLine());

        System.out.print("\nEnter size for array2 :: ");
        int size2 = Integer.parseInt(br.readLine());

        int arr1[] = new int[size1];
        int arr2[] = new int[size2];

        System.out.println("Enter array1 elements :: ");

        for(int i = 0; i < size1; i++)
            arr1[i] = Integer.parseInt(br.readLine());

        System.out.println("Enter array2 elements :: ");

        for(int i = 0; i < size2; i++)
            arr2[i] = Integer.parseInt(br.readLine());


        Solution obj = new Solution();
        
        int merged[] = obj.mergeArray(arr1, arr2);

        System.out.print("Merged Array :: ");
        for(int i = 0; i < merged.length; i++) {

            System.out.print(merged[i] + " ");
        }

        System.out.println();
    }
}