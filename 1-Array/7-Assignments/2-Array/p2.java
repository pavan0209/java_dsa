/*
 *  WAP to find the common elements between 2 arrays.
 */

import java.io.*;
import java.util.*;

class Solution {

    String[] commonElements(int arr1[], int arr2[]) {

        String common = "";

        Arrays.sort(arr1);
        Arrays.sort(arr2);

        for(int i = 0; i < arr1.length; i++) {

            for(int j = 0; j < arr1.length; j++) {

                if(arr1[i] == arr2[j] && !(common.contains(Integer.toString(arr1[i])))) {

                    common += Integer.toString(arr1[i]) + "#";
                }
            }
        }

        return common.split("#");
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter size for array :: ");
        int size = Integer.parseInt(br.readLine());

        int arr1[] = new int[size];
        int arr2[] = new int[size];

        System.out.println("Enter array1 elements :: ");

        for(int i = 0; i < size; i++) {
            arr1[i] = Integer.parseInt(br.readLine());
        }

        System.out.println("Enter array2 elements :: ");

        for(int i = 0; i < size; i++) {
            arr2[i] = Integer.parseInt(br.readLine());
        }

        Solution obj = new Solution();
        
        String common[] = obj.commonElements(arr1, arr2);

        System.out.print("Common Elements :: ");
        for(int i = 0; i < common.length; i++) {

            System.out.print(String.valueOf(common[i]) + " ");
        }

        System.out.println();
    }
}