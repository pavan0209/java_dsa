/*
 *  Find Maximum number in array.
 */

import java.io.*;

class Solution {

    int findMax(int arr[], int size, int max) {

        /*int max = arr[0];

        for(int i = 1; i < arr.length; i++) {

            if(max < arr[i])
                max = arr[i];
            else
                continue;
        }

        return max;*/

        if(size < 0)
            return max;

        int x = 0;
        if(arr[size] > max) 
            x = arr[size];
        else
            x = max;

        return findMax(arr, size-1, x);
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter size for array :: ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter array elements :: ");

        for(int i = 0; i < size; i++) {

            arr[i] = Integer.parseInt(br.readLine());
        }

        Solution obj = new Solution();
        
        int max = obj.findMax(arr, arr.length-2, arr[arr.length-1]);

        System.out.println("Max element in array is :: " + max);
    }
}