/*
 *  Strong numbers in array.
 */

import java.io.*;

class Solution {

    String StrongNumbers(int arr[]) {

        String strogNumbers = "";

        for(int i = 0; i < arr.length; i++) {

            int sum = 0;
            for(int j = arr[i]; j != 0; j /= 10) {

                int fact = 1;

                for(int k = 2; k <= j % 10; k++)
                    fact *= k;
                
                sum += fact;
            }

            if(sum == arr[i])
                strogNumbers += Integer.toString(arr[i]) + "#";
        }
        strogNumbers += "#";

        return strogNumbers;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter size for array :: ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter array elements :: ");

        for(int i = 0; i < size; i++)
            arr[i] = Integer.parseInt(br.readLine());

        Solution obj = new Solution();
        
        String ans = obj.StrongNumbers(arr);

        String[] res = ans.split("#");

        System.out.print("Strong Numbers in Array :: ");
        for(int i = 0; i < res.length; i++) {

            System.out.print(String.valueOf(res[i]) + " ");
        }

        System.out.println();
    }
}