/*
 *  Find Minimum number in array.
 */

import java.io.*;

class Solution {

    int findMin(int arr[], int size, int min) {

        /*int min = arr[0];

        for(int i = 1; i < arr.length; i++) {

            if(min > arr[i])
                min = arr[i];
            else
                continue;
        }

        return min;*/

        if(size < 0)
            return min;

        int x = 0;
        if(arr[size] < min) 
            x = arr[size];
        else
            x = min;

        return findMin(arr, size-1, x);
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter size for array :: ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter array elements :: ");

        for(int i = 0; i < size; i++)
            arr[i] = Integer.parseInt(br.readLine());

        Solution obj = new Solution();
        int small = arr[arr.length-1];
        
        int min = obj.findMin(arr, arr.length-2, small);

        System.out.println("Minimum element in array is :: " + min);

    }
}