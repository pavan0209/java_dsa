/*
 *  Frequency of Digits.
 */

import java.io.*;
import java.util.*;

class Solution {

    void frequencyDigit(int arr[], int size) {

        Arrays.sort(arr);
        int i = 0;

        while(i < arr.length) {

            int cnt = 0;
            
            for(int j = 0; j < arr.length; j++) {

                if(arr[i] == arr[j])
                    cnt++;
            }

            System.out.println("Frequency of " + arr[i] + " is :: " + cnt);
            if(cnt != 0)
                i += cnt;
            else 
                i++;
        }
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter size for array :: ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter array elements :: ");

        for(int i = 0; i < size; i++)
            arr[i] = Integer.parseInt(br.readLine());

        Solution obj = new Solution();
        obj.frequencyDigit(arr, arr.length-1);

    }
}