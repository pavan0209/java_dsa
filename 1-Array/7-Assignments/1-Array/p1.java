/*
 *  Write a program to create array of 5 integer elements, And print all 5 elements from an array
 *  (take hardcoded values in the array)  
 *  
 *  output : 1
 *           2
 *           3
 *           4
 *           5
 */

class Solution {
    public static void main(String[] args) {

        int arr[] = new int[5];

        arr[0] = 10;
        arr[1] = 20;
        arr[2] = 30;
        arr[3] = 40;
        arr[4] = 50;

        System.out.println("\narr[0] :: " + arr[0]);
        System.out.println("arr[1] :: " + arr[1]);
        System.out.println("arr[2] :: " + arr[2]);
        System.out.println("arr[3] :: " + arr[3]);
        System.out.println("arr[4] :: " + arr[4]);

        System.out.println();
    }
}