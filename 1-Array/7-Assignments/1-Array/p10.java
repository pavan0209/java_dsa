/*
 *  Sort array in ascending order
 */

import java.io.*;

class Solution {

    void sortArrayAscending(int arr[]) {

        for(int i = 0; i < arr.length-1; i++) { 

            for(int j = 0; j < arr.length-i-1; j++) {

                if(arr[j] > arr[j+1]) {

                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter size for array :: ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter array elements :: ");

        for(int i = 0; i < size; i++)
            arr[i] = Integer.parseInt(br.readLine());

        Solution obj = new Solution();
        
        obj.sortArrayAscending(arr);

        System.out.print("Sorted array in ascending order :: ");
        
        for(int i = 0; i < arr.length; i++) {

            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}