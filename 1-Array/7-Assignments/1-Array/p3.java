/*
 *  Write a program to create an array of 'n' Integer elements. Where 'n' value should be taken from the user.
 *  Insert the values from users and print accordingly.
 * 
 *  Input:
 *          n = 5;
 *          Enter elements in the array :
 *          1
 *          2
 *          3
 *          4
 *          5
 *  Output: 
 *          1
 *          2
 *          3
 *          4
 *          5
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter array size :: ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter array elements :: ");

        for(int i = 0; i < arr.length; i++) {

            arr[i] = Integer.parseInt(br.readLine());
        }
        
        System.out.println("\nArray elements are :: \n");

        for(int i = 0; i < arr.length; i++) {

            System.out.println("arr[" + i + "] :: " + arr[i]);
        }

        System.out.println();
    }
}