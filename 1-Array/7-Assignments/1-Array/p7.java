/*
 *  Sum of Array elements.
 */

import java.io.*;

class Solution {

    int arraySum(int arr[], int size) {

        /*int sum = 0;

        for(int i = 0; i < arr.length; i++) {

            sum += arr[i];
        }
        return sum;
        */

        if(size < 0) 
            return 0;
        
        return arr[size] + arraySum(arr, size-1);
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter size for array :: ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter array elements :: ");

        for(int i = 0; i < size; i++)
            arr[i] = Integer.parseInt(br.readLine());

        Solution obj = new Solution();
        
        int min = obj.arraySum(arr, arr.length-1);

        System.out.println("Minimum element in array is :: " + min);
    }
}