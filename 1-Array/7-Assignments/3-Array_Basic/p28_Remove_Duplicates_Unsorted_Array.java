/*
 *28] Remove Duplicates from unsorted array

    Given an array of integers which may or may not contain duplicate elements. Your task is to remove
    duplicate elements, if present.
    
    Example 1:
    Input:
        N = 6
        A[] = {1, 2, 3, 1, 4, 2}
    Output:
        1 2 3 4
    
    Example 2:
    Input:
        N = 4
        A[] = {1, 2, 3, 4}
    Output:
        1 2 3 4
    
    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(N)
    
    Constraints:
        1<=N<=10^5
        1<=A[i]<=10^5
*/

import java.util.*;

class Solution {

    static int[] removeDuplicates(int arr[], int N) {

        LinkedHashSet<Integer> lhs = new LinkedHashSet<>();

        for (int i = 0; i < N; i++)
            lhs.add(arr[i]);

        int res[] = new int[lhs.size()];
        int j = 0;
        Iterator<Integer> itr = lhs.iterator();

        while (itr.hasNext())
            res[j++] = itr.next();

        return res;
    }

    public static void main(String[] args) {

        int A[] = { 1, 2, 3, 4 };

        A = Solution.removeDuplicates(A, A.length);

        for (int i = 0; i < A.length; i++) {

            System.out.print(A[i] + "  ");
        }

        System.out.println();
    }
}