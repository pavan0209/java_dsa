/*
 *26] Positive and negative elements

    Given an array arr[ ] containing equal number of positive and negative elements, arrange the array such
    that every positive element is followed by a negative element.
    
    Note- The relative order of positive and negative numbers should be maintained.
    
    Example 1:
    Input:
        N = 6
        arr[] = {-1, 2, -3, 4, -5, 6}
    Output:
        2 -1 4 -3 6 -5
    Explanation: Positive numbers in order are 2, 4 and 6. Negative numbers in order are -1, -3 and -5.
    So the arrangement we get is 2, -1, 4, -3, 6 and -5.
    
    Example 2:
    Input:
        N = 4
        arr[] = {-3, 2, -4, 1}
    Output:
        2 -3 1 -4
    
    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(N)
    
    Constraints:
        1 ≤ N ≤ 10^6
        1 ≤ arr[i] ≤ 10^9
 */

import java.util.*;

class Solution {

    static int[] arrangePositiveNegative(int arr[], int N) {

        ArrayList<Integer> pos = new ArrayList<>();
        ArrayList<Integer> neg = new ArrayList<>();

        for (int i = 0; i < N; i++) {

            if (arr[i] < 0)
                neg.add(arr[i]);
            else
                pos.add(arr[i]);
        }

        int j = 0, k = 0;

        for (int i = 0; i < N - 1; i += 2) {

            arr[i] = pos.get(j++);
            arr[i + 1] = neg.get(k++);
        }

        return arr;
    }

    public static void main(String[] args) {

        int arr[] = { -3, 2, -4, 1 };

        Solution.arrangePositiveNegative(arr, arr.length);

        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        System.out.println();
    }
}