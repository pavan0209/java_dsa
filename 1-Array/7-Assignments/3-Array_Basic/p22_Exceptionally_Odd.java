/*
 *22] Exceptionally odd

    Given an array of N positive integers where all numbers occur even number of times except one number which
    occurs odd number of times. Find the exceptional number.

    Example 1:
    Input:
        N = 7
        Arr[] = {1, 2, 3, 2, 3, 1, 3}
    Output: 3
    Explanation: 3 occurs three times.
    
    Example 2:
    Input:
        N = 7
        Arr[] = {5, 7, 2, 7, 5, 2, 5}
    Output: 5
    Explanation: 5 occurs three times.
    
    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)
    
    Constraints:
        1 ≤ N ≤ 10^5
        1 ≤ arr[i] ≤ 10^6
 */

class Solution {

    static int exceptionallyOdd(int arr[], int N) {

        java.util.Arrays.sort(arr);
        int cnt = 0;

        if (N == 1)
            return arr[0];

        for (int i = 0; i < N - 1; i++) {

            cnt++;
            if (arr[i] != arr[i + 1]) {

                if (cnt % 2 != 0)
                    return arr[i];

                cnt = 0;
            }
        }

        if (arr[N - 1] != arr[N - 2] && (cnt + 1) % 2 != 0)
            return arr[N - 1];

        if (arr[N - 1] == arr[N - 2] && (cnt + 1) % 2 != 0)
            return arr[N - 1];

        return -1;
    }

    public static void main(String[] args) {

        int Arr[] = { 8, 4, 4, 8, 23 };

        System.out.println(Solution.exceptionallyOdd(Arr, Arr.length));
    }
}