/*
 *40] Fibonacci in the array

        Given an array arr of size N, the task is to count the number of elements of the array which are
    Fibonacci numbers.
   
    Example 1:
    Input: 
        N = 9, arr[] = {4, 2, 8, 5, 20, 1, 40, 13, 23}
    Output: 5
    Explanation: 
        Here, Fibonacci series will be 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55. Numbers that are present in array
    are 2, 8, 5, 1, 13
    
    Example 2:
    Input: 
        N = 4, arr[] = {4, 7, 6, 25}
    Output: 0
    Explanation: No Fibonacci number in this array.
    
    Expected Time Complexity: O(N).
    Expected Auxiliary Space: O(1).
    
    Constraints:
        1 ≤ N ≤ 10^6
 */

import java.util.*;

class Solution {

    static int checkFibNums(long arr[], int n) {

        Arrays.sort(arr);
        long max = arr[n-1];
        int cnt = 0;
        long a = 0;
        long b = 1;

        ArrayList <Long> fib = new ArrayList <>();
        fib.add((long)0);

        for(int i = 1; a <= max; i++) {

            long c = a + b;
            b = a;
            a = c;

            fib.add(a);
        }

        for(int i = 0; i < n; i++) {

            if(fib.contains(arr[i]))
                cnt++;
        }

        return cnt;
    }

    public static void main(String[] args) {

        long arr[] = {4, 7, 6, 25};

        System.out.println(Solution.checkFibNums(arr, arr.length));

    }
}