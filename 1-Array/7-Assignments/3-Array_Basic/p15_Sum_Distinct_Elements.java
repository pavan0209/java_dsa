/*
 *15] Sum of distinct elements

    You are given an array Arr of size N. Find the sum of distinct elements in an array.
    
    Example 1:
        Input:
            N = 5
            Arr[] = {1, 2, 3, 4, 5}
        Output: 15
    Explanation: Distinct elements are 1, 2, 3, 4, 5. So the sum is 15.
    
    Example 2:
        Input:
            N = 5
            Arr[] = {5, 5, 5, 5, 5}
        Output: 5
    Explanation: Only Distinct element is 5. So the sum is 5.

    Expected Time Complexity: O(N*logN)
    Expected Auxiliary Space: O(N*logN)
    
    Constraints:
        1 ≤ N ≤ 10^7
        0 ≤ A[i] ≤ 10^4
 */

import java.util.*;

class Solution {

    static int distinctElementSum(int arr[], int N) {

        /*
         * int sum = 0;
         * 
         * for(int i = 0; i < N-1; i++) {
         * 
         * if(arr[i] != arr[i+1])
         * sum += arr[i];
         * }
         * 
         * if(N == 1 || sum == 0)
         * return arr[0];
         * 
         * return (arr[N-1] != arr[N-2]) ? sum+arr[N-1] : sum;
         */

        TreeSet<Integer> ts = new TreeSet<Integer>();

        for (int i = 0; i < N; i++)
            ts.add(arr[i]);

        int sum = 0;

        Iterator<Integer> itr = ts.iterator();

        while (itr.hasNext())
            sum += itr.next();

        return sum;

    }

    public static void main(String[] args) {

        int Arr[] = { 5, 5, 5, 5, 15 };

        System.out.println(Solution.distinctElementSum(Arr, Arr.length));
    }
}