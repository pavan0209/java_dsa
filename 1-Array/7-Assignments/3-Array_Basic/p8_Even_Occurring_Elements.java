/*
 *8] Even occurring elements
    Given an array Arr of N integers that contains an odd number of occurrences for all
    numbers except for a few elements which are present even number of times. Find
    the elements which have even occurrences in the array.
    
    Example 1:
    Input:
    N = 11
    Arr[] = {9, 12, 23, 10, 12, 12,
    15, 23, 14, 12, 15}
    Output: 12 15 23
    
    Example 2:
    Input:
    N = 5
    Arr[] = {23, 12, 56, 34, 32}
    Output: -1
    Explanation:
    Every integer is present odd number of times.
    
    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)
    
    Constraints:
    1 ≤ N ≤ 10^5
    0 ≤ Arr[i] ≤ 63
 */

import java.util.*;

class Solution {

    static ArrayList<Integer> evenOccurring(int arr[], int N) {

        Arrays.sort(arr);
        ArrayList<Integer> al = new ArrayList<Integer>();
        int cnt = 0;

        if (N == 1) {
            al.add(-1);
            return al;
        }

        for (int i = 0; i < N - 1; i++) {
            cnt++;
            if (arr[i] != arr[i + 1]) {

                if (cnt % 2 == 0)
                    al.add(arr[i]);

                cnt = 0;
            }
        }

        if (arr[N - 1] == arr[N - 2]) {
            if ((cnt + 1) % 2 == 0)
                al.add(arr[N - 1]);
        }

        if (al.isEmpty())
            al.add(-1);

        return al;
    }

    public static void main(String[] args) {

        int arr[] = { 23, 12, 56, 34, 32 };

        ArrayList<Integer> al = Solution.evenOccurring(arr, arr.length);

        for (int i = 0; i < al.size(); i++) {

            System.out.print(al.get(i) + "  ");
        }

        System.out.println();
    }
}