/*
 *2] Find minimum and maximum element in an array

    Given an array A of size N of integers. Your task is to find the minimum and
    maximum elements in the array.

    Example 1:
    Input:
    N = 6
    A[] = {3, 2, 1, 56, 10000, 167}
    Output: 1 10000
    Explanation: minimum and maximum elements of array are 1 and 10000.
    
    Example 2:
    Input:
    N = 5
    A[] = {1, 345, 234, 21, 56789}
    Output: 1 56789
    Explanation: minimum and maximum elements of array are 1 and 56789.
    
    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)
    
    Constraints:
    1 <= N <= 10^5
    1 <= Ai <=10^12
 * 
 */

class Solution {

    static int[] findMinMax(int arr[], int size) {

        int res[] = { arr[0], arr[0] };

        for (int i = 1; i < size; i++) {

            if (arr[i] < res[0])
                res[0] = arr[i];

            if (arr[i] > res[1])
                res[1] = arr[i];
        }

        return res;
    }

    public static void main(String[] args) {

        int arr[] = { 3, 2, 1, 56, 10000, 167 };

        int ans[] = Solution.findMinMax(arr, arr.length);

        System.out.println("Min :: " + ans[0] + "\nMax :: " + ans[1]);
    }
}