/*
 *12] First and last occurrences of X
    
    Given a sorted array having N elements, find the indices of the first and last
    occurrences of an element X in the given array.
    
    Note: If the number X is not found in the array, return '-1' as an array.
    
    Example 1:
        Input:
            N = 4 , X = 3
            arr[] = { 1, 3, 3, 4 }
        Output:
            1 2
    Explanation:
        For the above array, first occurance of X = 3 is at index = 1 and last
        occurrence is at index = 2.

    Example 2:
        Input:
            N = 4, X = 5
            arr[] = { 1, 2, 3, 4 }
        Output:
            -1
    Explanation:
        As 5 is not present in the array, so the answer is -1.
    
    Expected Time Complexity: O(log(N))
    Expected Auxiliary Space: O(1)
    
    Constraints:
    1 <= N <= 10^5
    0 <= arr[i], X <= 10^9
 */

class Solution {

    static int[] firstLastOccurrence(int arr[], int N, int X) {

        int flag = 0;
        int first = arr[0];
        int last = -1;

        for (int i = 0; i < N; i++) {

            if (arr[i] == X) {

                if (flag == 0) {
                    first = i;
                    flag = 1;
                }
                last = i;
            }
        }

        if (last == -1) {

            int res[] = { -1 };
            return res;
        } else {

            int res[] = { first, last };
            return res;
        }
    }

    public static void main(String[] args) {

        int arr[] = { 1, 2, 3, 4, 5};

        int res[] = Solution.firstLastOccurrence(arr, arr.length, 5);

        for (int i = 0; i < res.length; i++) {

            System.out.print(res[i] + "  ");
        }

        System.out.println();
    }
}