/*
 *10] Max Odd Sum

    Given an array of integers, check whether there is a subsequence with odd sum and
    if yes, then find the maximum odd sum. If no subsequence contains an odd sum,
    print -1.
    
    Example 1:
    Input:
    N=4
    arr[] = {4, -3, 3, -5}
    Output: 7
    Explanation:
    The subsequence with maximum odd
    sum is 4 + 3 = 7
    
    Example 2:
    Input:
    N=5
    arr[] = {2, 5, -4, 3, -1}
    Output: 9
    Explanation:
    The subsequence with maximum odd
    sum is 2 + 5 + 3 + -1 = 9
    
    Expected Time Complexity: O(N).
    Expected Auxiliary Space: O(1).
    
    Constraints:
    2 ≤ N ≤ 10^7
    -10^3 <= arr[i] <= 10^3
 */

class Solution {

    static long findMaxOddSubarraySum(long arr[] ,int n) {

        long sum[] = new long[n];
        sum[0] = arr[0];

        for(int i = 1; i < arr.length; i++) {

            sum[i] = arr[i-1] + arr[i];
        }

        for(int i = 0; i < sum.length; i++) {

            if(sum[i] % 2 == 0)
                return -1;
        }

        long max = sum[0];

        for(int i = 1; i < sum.length; i++) {

            if(sum[i] > max)
                max = sum[i];
        }

        return max;
    }


    public static void main(String[] args) {

        long arr[] = {2, 5, -4, 3, -1};

        System.out.println(Solution.findMaxOddSubarraySum(arr, arr.length));
    }
}