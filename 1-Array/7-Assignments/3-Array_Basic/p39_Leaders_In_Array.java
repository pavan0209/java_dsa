/*
 *39] Leaders in an array

    Write a program to print all the LEADERS in the array. An element is a leader if it is greater than all the
    elements to its right side. And the rightmost element is always a leader.
    
    For example:
    Input: 
        arr[] = {16, 17, 4, 3, 5, 2},
    Output : 
        17, 5, 2
    
    Input: 
        arr[] = {1, 2, 3, 4, 5, 2},
    Output: 
        5, 2
 */

import java.util.*;

class Solution {

    static ArrayList<Integer> leadersInArray(int arr[], int n) {

        ArrayList<Integer> al = new ArrayList<Integer>();
        int max = arr[n - 1];
        String str = String.valueOf(max);

        for (int i = n - 2; i >= 0; i--) {

            if (max <= arr[i]) {

                str = String.valueOf(arr[i]) + "#" + str;
                max = arr[i];
            }
        }

        String S[] = str.split("[#]");

        for (int i = 0; i < S.length; i++)
            al.add(Integer.valueOf(S[i]));

        return al;
    }

    public static void main(String[] args) {

        int arr[] = { 1, 2, 3, 4, 5, 2 };

        ArrayList<Integer> al = Solution.leadersInArray(arr, arr.length);

        for (int i = 0; i < al.size(); i++)
            System.out.print(al.get(i) + "  ");

        System.out.println();
    }
}