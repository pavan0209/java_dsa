/*
 *19] Find common elements in three sorted arrays

    Given three Sorted arrays in non-decreasing order, print all common elements in these arrays.
    
    Examples:
        Input:
            ar1[] = {1, 5, 10, 20, 40, 80}
            ar2[] = {6, 7, 20, 80, 100}
            ar3[] = {3, 4, 15, 20, 30, 70, 80, 120}
        Output: 
            20, 80
    
        Input:
            ar1[] = {1, 5, 5}
            ar2[] = {3, 4, 5, 5, 10}
            ar3[] = {5, 5, 10, 20}
        Output: 
            5, 5
 */

import java.util.*;

class Solution {

    static ArrayList<Integer> commonElements(int arr1[], int arr2[], int arr3[]) {

        ArrayList <Integer> al1 = new ArrayList <>();
        ArrayList <Integer> al2 = new ArrayList <>();
        ArrayList <Integer> al3 = new ArrayList <>();
        ArrayList <Integer> res = new ArrayList <>();

        for(int i = 0; i < arr1.length; i++)
            al1.add(arr1[i]);

        for(int i = 0; i < arr2.length; i++)
            al2.add(arr2[i]);

        for(int i = 0; i < arr3.length; i++)
            al3.add(arr3[i]);

        /*if(al1.size() > al2.size() && al1.size() > al3.size()) 
            maxSize = al1.size();
        else if(al2.size() > al1.size() && al2.size() > al3.size())
            maxSize = al2.size();
        else 
            maxSize = al3.size();*/

        for(int i = 0; i < al2.size(); i++) {

            if(al1.contains(al2.get(i)) && al2.contains(al2.get(i)) && al3.contains(al2.get(i)))
                res.add(al2.get(i));
        }

        return res;
    }

    public static void main(String[] args) {

        int ar1[] = {1, 5, 5};
        int ar2[] = {3, 4, 5, 5, 10};
        int ar3[] = {5, 5, 10, 20};

        ArrayList <Integer> ans = Solution.commonElements(ar1, ar2, ar3);

        for(int i = 0; i < ans.size(); i++) {

            System.out.print(ans.get(i) + "  ");
        }

        System.out.println();
    }
}