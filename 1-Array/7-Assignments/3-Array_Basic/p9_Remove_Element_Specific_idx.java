/*
 9] Remove an Element at Specific Index from an Array
    Given an array of a fixed length. The task is to remove an element at a specific
    index from the array.
    
    Examples 1:
    Input: arr[] = { 1, 2, 3, 4, 5 }, index = 2
    Output: arr[] = { 1, 2, 4, 5 }
    
    Examples 2:
    Input: arr[] = { 4, 5, 9, 8, 1 }, index = 3
    Output: arr[] = { 4, 5, 9, 1 }
 */

class Solution {

    static int[] removeEleSpecificIndex(int arr[], int index) {

        if (index > arr.length - 1)
            return arr;

        int res[] = new int[arr.length - 1];
        int j = 0;

        for (int i = 0; i < arr.length; i++) {

            if (i == index)
                continue;

            res[j++] = arr[i];
        }

        return res;
    }

    public static void main(String[] args) {

        int arr[] = { 1, 2, 3, 4, 5 };

        arr = Solution.removeEleSpecificIndex(arr, 4);

        for (int i = 0; i < arr.length; i++) {

            System.out.print(arr[i] + "  ");
        }

        System.out.println();
    }
}