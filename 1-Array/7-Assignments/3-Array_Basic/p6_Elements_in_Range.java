/*
 *6] Elements in the Range

    Given an array arr[] containing positive elements. A and B are two numbers
    defining a range. The task is to check if the array contains all elements in the given
    range.

    Example 1:
    Input: N = 7, A = 2, B = 5
    arr[] = {1, 4, 5, 2, 7, 8, 3}
    Output: Yes
    Explanation: It has elements between range 2-5 i.e 2,3,4,5

    Example 2:
    Input: N = 7, A = 2, B = 6
    arr[] = {1, 4, 5, 2, 7, 8, 3}
    Output: No
    Explanation: Array does not contain 6.

    Note: If the array contains all elements in the given range then driver code outputs
    Yes otherwise, it outputs No

    Expected Time Complexity: O(N).
    Expected Auxiliary Space: O(1).

    Constraints:
    1 ≤ N ≤ 10^7
 * 
 */

class Solution {

    static String checkNumsRange(int arr[], int N, int A, int B) {

        java.util.Arrays.sort(arr);

        if(A < 0 || A > N) 
            return "No";

        if(B < 0 || B > N)
            return "No";

        int idx = 0;   

        for(int i = 0; i < N; i++) {

            if(arr[i] == A) {
                idx = i;
                break;
            }
        }

        while(arr[idx] <= arr[B]) {

            if(arr[idx+1] - arr[idx] != 1)
                return "No";
            
            idx++;    
        }
    
        return "Yes";
    }

    public static void main(String[] args) {

        int arr[] = {1, 4, 5, 2, 7, 8, 3};

        System.out.println(checkNumsRange(arr, arr.length, 2, 5));
    }
}