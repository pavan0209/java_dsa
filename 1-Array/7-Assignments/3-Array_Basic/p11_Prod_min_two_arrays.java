/*
 * 11] Product of maximum in first array and minimum in second

    Given two arrays of A and B respectively of sizes N1 and N2, the task is to
    calculate the product of the maximum element of the first array and minimum
    element of the second array.
    
    Example 1:
        Input : A[] = {5, 7, 9, 3, 6, 2},
                B[] = {1, 2, 6, -1, 0, 9}
        Output : -9
    Explanation:
        The first array is 5 7 9 3 6 2.
        The max element among these elements is 9.
        The second array is 1 2 6 -1 0 9.
        The min element among these elements is -1.
        The product of 9 and -1 is 9*-1=-9.
    
    Example 2:
        Input : A[] = {0, 0, 0, 0},
                B[] = {1, -1, 2}
        Output : 0
    
    Expected Time Complexity: O(N + M).
    Expected Auxiliary Space: O(1).
    
    Output:
        For each test case, output the product of the max element of the first array and the
    minimum element of the second array.
    
    Constraints:
        1 ≤ N, M ≤ 10^6
        -10^8 ≤ Ai, Bi ≤ 10^8
 */

import java.util.*;

class Solution {

    static long productOfMin(int A[], int B[]) {

        Arrays.sort(A);
        Arrays.sort(B);

        return A[A.length - 1] * B[0];
    }

    public static void main(String[] args) {

        int A[] = { 0, 0, 0, 0 };
        int B[] = { 1, -1, 2 };

        System.out.println(Solution.productOfMin(A, B));
    }
}