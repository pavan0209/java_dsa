/*
 *25] Maximum product of two numbers

        Given an array Arr of size N with all elements greater than or equal to zero. Return the maximum product
    of two numbers possible.
    
    Example 1:
    Input:
        N = 6
        Arr[] = {1, 4, 3, 6, 7, 0}
    Output: 42
    
    Example 2:
    Input:
        N = 5
        Arr = {1, 100, 42, 4, 23}
    Output: 4200
    
    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)
    
    Constraints:
        2 ≤ N ≤ 10^7
        0 ≤ Arr[i] ≤ 10^4
 */

class Solution {

    static long maxProductTwoNums(int arr[], int N) {

        long prod = 1;

        for (int i = 0; i < N; i++) {

            for (int j = 0; j < N; j++) {

                long temp = 1;

                if (i != j)
                    temp = arr[i] * arr[j];

                if (temp > prod)
                    prod = temp;
            }
        }

        return prod;
    }

    public static void main(String[] args) {

        int Arr[] = { 1, 4, 3, 6, 7, 0 };

        System.out.println(Solution.maxProductTwoNums(Arr, Arr.length));
    }
}