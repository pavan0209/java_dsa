/*
 *37] Move all negative numbers to beginning and positive to end with constant extra space.

        An array contains both positive and negative numbers in random order. Rearrange the array elements so
    that all negative numbers appear before all positive numbers.
    
    Examples :
        Input: -12, 11, -13, -5, 6, -7, 5, -3, -6
        Output: -12 -13 -5 -7 -3 -6 11 6 5
 */

class Solution {

    static int[] rearrange(int arr[], int N) {

        for (int i = 0; i < N; i++) {

            for (int j = 0; j < N; j++) {

                if (arr[j] > 0) {

                    int temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
        }

        return arr;

    }

    public static void main(String[] args) {

        int arr[] = { -12, 11, -13, -5, 6, -7, 5, -3, -6 };
        arr = Solution.rearrange(arr, arr.length);

        for (int i = 0; i < arr.length; i++) {

            System.out.print(arr[i] + "  ");
        }
    }
}