/*
 *23] Find the smallest and second smallest element in an array

    Given an array of integers, your task is to find the smallest and second smallest element in the array. If 
    smallest and second smallest do not exist, print -1.
    
    Example 1:
    Input :
        5
        2 4 3 5 6
    Output :
        2 3
    Explanation:
        2 and 3 are respectively the smallest and second smallest elements in the array.
    
    Example 2:
    Input :
        6
        1 2 1 3 6 7
    Output :
        1 2
    Explanation:
        1 and 2 are respectively the smallest and second smallest elements in the array.
    
    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)
    
    Constraints:
        1<=N<=10^5
        1<=A[i]<=10^5
 */

class Solution {

    static int[] smallestSecSmallest(int arr[], int N) {

        java.util.Arrays.sort(arr);

        int ans[] = {arr[0], -1};
        int ans2[] = {-1};
        int flag = 0;

        for(int i = 1; i < arr.length; i++) {

            if(arr[i-1] < arr[i]) {
                ans[1] = arr[i];
                flag = 1;
                break;
            }
        }
        
        return (flag == 1) ?  ans : ans2;
    }

    public static void main(String[] args) {

        int arr[] = {1, 2, 1, 3, 6, 7};

        int ans[] = Solution.smallestSecSmallest(arr, arr.length);

        for(int i = 0; i < ans.length; i++) {

            System.out.print(ans[i] + "  ");
        }

        System.out.println();
    }
}