/*
 *18] Find Subarray with given sum | Set 1 (Non-negative Numbers)
        
    Given an array arr[] of non-negative integers and an integer sum, find a subarray that adds to a given sum.
    
    Note: There may be more than one subarray with sum as the given sum, print first such subarray.
    
    Examples:
    Input: 
        arr[] = {1, 4, 20, 3, 10, 5}, sum = 33
    Output: 
        Sum found between indexes 2 and 4
    Explanation: 
        Sum of elements between indices 2 and 4 is 20 + 3 + 10 = 33
    
    Input: 
        arr[] = {1, 4, 0, 0, 3, 10, 5}, sum = 7
    Output: 
        Sum found between indexes 1 and 4
    Explanation: 
        Sum of elements between indices 1 and 4 is 4 + 0 + 0 + 3 = 7
    
    Input: 
        arr[] = {1, 4}, sum = 0
    Output: 
        No subarray found
    Explanation: 
        There is no subarray with 0 sum
 */

import java.util.*;

class Solution {

    static ArrayList<Integer> subarraySum(int[] arr, int n, int s) {

        ArrayList<Integer> al = new ArrayList<Integer>();

        int sum = 0;

        if (n == 0) {
            al.add(-1);
            return al;
        }

        for (int i = 0; i < n - 1; i++) {

            sum = 0;

            for (int j = i; j < n && sum <= s; j++) {

                sum += arr[j];

                if (sum == s) {

                    al.add(i);
                    al.add(j);

                    return al;
                }
            }
        }

        al.add(-1);

        return al;
    }

    public static void main(String[] args) {

        int arr[] = { 1, 4, 3 };

        ArrayList<Integer> res = Solution.subarraySum(arr, arr.length, 7);

        if (res.get(0) != -1)
            System.out.println("Sum found between indexes " + res.get(0) + " and " + res.get(1));
        else
            System.out.println("No subarray found");
    }
}