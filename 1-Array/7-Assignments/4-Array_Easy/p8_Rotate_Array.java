/*
 *Que 8 : Rotate Array
    
        Given an unsorted array arr[] of size N. Rotate the array to the left (counter-clockwise direction) by
    D steps, where D is a positive integer.

    Example 1:
    Input:
        N = 5, D = 2
        arr[] = {1,2,3,4,5}
    Output: 
        3 4 5 1 2
    Explanation: 1 2 3 4 5 when rotated by 2 elements, it becomes 3 4 5 1 2.

    Example 2:
    Input:
        N = 10, D = 3
        arr[] = {2,4,6,8,10,12,14,16,18,20}
    Output: 
        8 10 12 14 16 18 20 2 4 6
    Explanation: 
        2 4 6 8 10 12 14 16 18 20 when rotated by 3 elements, it becomes 8 10 12 14 16 18 20 2 4 6.

    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)
    
    Constraints:
        1 <= N <= 10^6
        1 <= D <= 10^6
        0 <= arr[i] <= 10^5
 */

import java.util.*;

class Solution {

    static int[] rotateArray(int arr[], int n, int steps) {

        ArrayList<Integer> al = new ArrayList <>();

        for (int i = 0; i < n; i++)
            al.add(arr[i]);

        for (int i = 0; i < steps; i++)
            al.add(al.remove(0));

        for (int i = 0; i < n; i++)
            arr[i] = al.get(i);

        return arr;
    }

    public static void main(String[] args) {

        int arr[] = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };

        arr = Solution.rotateArray(arr, arr.length, 3);

        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        System.out.println();
    }
}