/*
 *Que 6 : Second Largest
    Given an array Arr of size N, print the second largest distinct element from an array.

    Example 1:
    Input:
        N = 6
        Arr[] = {12, 35, 1, 10, 34, 1}
    Output: 34
    Explanation: The largest element of the array is 35 and the second largest element is 34.

    Example 2:
    Input:
        N = 3
        Arr[] = {10, 5, 10}
    Output: 5
    Explanation: The largest element of the array is 10 and the second largest element is 5.

    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)
    
    Constraints:
        2 ≤ N ≤ 10^5
        1 ≤ Arri ≤ 10
 */

class Solution {

    static int findLargest(int arr[], int n) {

        java.util.Arrays.sort(arr);

        for(int i = n-1; i > 0; i--) {

            if(arr[i] > arr[i-1])
                return arr[i-1];
        }

        return -1;
    }

    public static void main(String[] args) {

        int Arr[] = {10, 5, 10};

        System.out.println(Solution.findLargest(Arr, Arr.length));
    }
}