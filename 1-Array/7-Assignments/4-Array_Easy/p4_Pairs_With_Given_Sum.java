/*
 *Que 4 : Count pairs with given sum
    
        Given an array of N integers, and an integer K, find the number of pairs of elements in the array whose
    sum is equal to K.

    Example 1:
    Input:
        N = 4, K = 6
        arr[] = {1, 5, 7, 1}
    Output: 2
    Explanation:
        arr[0] + arr[1] = 1 + 5 = 6
        and arr[1] + arr[3] = 5 + 1 = 6.

    Example 2:
    Input:
        N = 4, K = 2
        arr[] = {1, 1, 1, 1}
    Output: 6
    Explanation:
        Each 1 will produce sum 2 with any 1.

    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(N)
    
    Constraints:
        1 <= N <= 10^5
        1 <= K <= 10^8
        1 <= Arr[i] <= 10
 */

class Solution {

    static int getPairsCount(int[] arr, int n, int k) {
        
        int cnt = 0;
        //java.util.Arrays.sort(arr);

        for(int i = 0; i < n; i++) {

            for(int j = i+1; (j < n && arr[i] + arr[j] <= k); j++) {

                if(arr[i] + arr[j] == k)
                    cnt++;
            }
        }

        return cnt;
    }

    public static void main(String[] args) {

        int arr[] = {1, 5, 7, 1};

        System.out.println(Solution.getPairsCount(arr, arr.length, 6));
    }
}