/*
 *Que 7 : Check if array is sorted
    
        Given an array arr[] of size N, check if it is sorted in non-decreasing order or not.
    
    Example 1:
    Input:
        N = 5
        arr[] = {10, 20, 30, 40, 50}
    Output: 1
    Explanation: The given array is sorted.

    Example 2:
    Input:
        N = 6
        arr[] = {90, 80, 100, 70, 40, 30}
    Output: 0
    Explanation: The given array is not sorted.

    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)
    
    Constraints:
        1 ≤ N ≤ 10^5
        1 ≤ Arr[i] ≤ 10^6
 */

import java.util.*;
 
class Solution {

    static int sortedArrayOrNot(int arr[], int n) {

        /*int i = 0;
        for( ;i < n-1; i++) {

            if(arr[i] > arr[i+1])
                return 0;
        }

        if(arr[i] > arr[n-1])
            return 0;

        return 1;*/

        int temp[] = Arrays.copyOf(arr, n);
        Arrays.sort(temp);

        return (Arrays.equals(arr, temp)) ? 1 : 0;
    }

    public static void main(String[] args) {

        int arr[] = {10, 20, 30, 40, 50};

        System.out.println(Solution.sortedArrayOrNot(arr, arr.length));
    }
}