/*
 *Que 1 : Missing number in array
        
        Given an array of size N-1 such that it only contains distinct integers in the range of 1 to N. Find the
    missing element.

    Example 1:
    Input:
        N = 6
        A[] = {1,2,4,5,6}
    Output: 3

    Example 2:
    Input:
        N = 11
        A[] = {1,3,2,5,6,7,8,11,10,4}
    Output: 9

    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)
    
    Constraints:
        1 ≤ N ≤ 10^6
        1 ≤ A[i] ≤ 10^6
*/

class Solution {

    static int findMissingNumber(int A[], int n) {

        java.util.Arrays.sort(A);

        if (A[0] > 1)
            return 1;

        if (A.length == 1)
            return A[0] + 1;

        for (int i = 1; i < n - 1; i++) {

            if (A[i] - A[i - 1] > 1)
                return A[i] - 1;
        }

        return A[n - 1] + 1;
    }

    public static void main(String[] args) {

        int A[] = { 1, 2, 3, 4 };

        System.out.println(Solution.findMissingNumber(A, A.length));
    }
}
