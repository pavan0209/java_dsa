/*
 *Que 2 :Sort an array of 0s, 1s and 2s
    
    Given an array of size N containing only 0s, 1s, and 2s; sort the array in ascending order.

    Example 1:
    Input:
        N = 5
        arr[]= {0 2 1 2 0}
    Output:
        0 0 1 2 2
    Explanation: 0s 1s and 2s are segregated into ascending order.

    Example 2:
    Input:
        N = 3
        arr[] = {0 1 0}
    Output:
        0 0 1
    Explanation: 0s 1s and 2s are segregated into ascending order.

    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)
    
    Constraints:
        1 <= N <= 10^6
        0 <= A[i] <= 2
 */

class Solution {

    public static void sort012(int arr[], int n) {

        java.util.Arrays.sort(arr);
    }

    public static void main(String[] args) {

        int arr[] = { 0, 2, 1, 2, 0 };

        Solution.sort012(arr, arr.length);

        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        System.out.println();
    }
}