/*
 *   Given an array of size N.
 *   Find the maximum sum of length k
 *  
 *   ip : arr[] = {-3,4,-2,5,3,-2,8,2,1,4}
 *         k = 4
 *   op : 15
 */

class Solution {

    static int maximumSubSumLengthK(int arr[], int N, int K) {

        int maxSum = Integer.MIN_VALUE;

        for(int i = 0; i <= N-K; i++) {

            int sum = 0;

            for(int j = i; j < K+i; j++)
                sum += arr[j];

            if(sum > maxSum)
                maxSum = sum;
        }

        return maxSum;
    }

    public static void main(String[] args) {

        int arr[] = {-3,4,-2,6,3,-2,8,2,1,4};

        System.out.println(Solution.maximumSubSumLengthK(arr, arr.length, 4));
    }
}