/*
 *  find count of subarrays having length k
 * 
 *  ip : arrr[] = {-3,4,-2,6,3,-2,8,2,1,4};
 *  op : 7
 */

class Solution {

    static int countSubArraysLengthK(int arr[], int N, int K) {

        //Approach-1:
        /*int cnt = 0;

        for(int i = 0; i < N; i++) {

            for(int j = i; j < N; j++) {

                int temp = 0;

                for(int k = i; k <= j; k++) 
                    temp++;

                if(temp == K)
                    cnt++;
            }
        }
        return cnt;*/

        //Approach-2 :
        //return N-K+1;

        //Approach-3
        /*int cnt = 0;

        if(K == 0)
            return 0;

        for(int i = 0; i <= N-K; i++)
            cnt++;

        return cnt;*/

        //Approach-4
        int cnt = 0;
        int right = K-1;

        while(right < N) {

            cnt++;
            right++;
        }

        return cnt;
    }

    public static void main(String[] args) {

        int arr[] = {-3,4,-2,6,3,-2,8,2,1,4};

        System.out.println(Solution.countSubArraysLengthK(arr, arr.length, 4));
    }
}