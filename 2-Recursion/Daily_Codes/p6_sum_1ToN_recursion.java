/*
 *
 */

class Solution {

    static int sumNum(int num) {

        if(num <= 1)
            return 1;

        return num + sumNum(num-1);
    }

    public static void main(String[] args) {

        System.out.println(Solution.sumNum(10));
    }
}