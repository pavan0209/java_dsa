/*
 *  Print 1 to N using Loop.
 */

class Solution {

    static void printNums(int N) {

        for(int i = 1; i <= N; i++) 
            System.out.print(i + "  ");
    }

    public static void main(String[] args) {

        Solution.printNums(10);

        System.out.println();
    }
}