/*
 *  Print 1 to N using recursion.
 */

class Solution {

    //Approach 1:
    /*
    static void printNums(int N) {

        if(N <= 0)
            return;
        
        printNums(N-1);
        System.out.print(N + "  ");
    }
    */

    //Approach 2:
    static int i = 1;
    static void printNums(int N) {

        if(i > N)
            return;

        System.out.print(i + "  ");
        i++;
        printNums(N);
    }

    public static void main(String[] args) {

        Solution.printNums(10);

        System.out.println();
    }
}