/*
 *
 */

class Solution {

    static int fun(int num) {

        if(num == 0)
            return 1;

        //num = 5 + fun(--num);   // error: 'void' type not allowed here => if return type is void
        //num = 5 + fun(--num);
        //System.out.println(num);    //error: missing return statement

        return 5 + fun(--num);
    }

    public static void main(String[] args) {

        System.out.println(Solution.fun(2));
    }
}