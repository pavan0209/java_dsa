/*
 *
 */

class Solution {

    static int fun(int num) {

        if(num == 1)
            return 1;
        
        return fun(--num) + 3;
    }

    public static void main(String[] args) {

        System.out.println(Solution.fun(2));
    }   
}