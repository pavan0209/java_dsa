/*
 *  Write a program to check if a given number is an Abundant Number or not.
 *  (An Abundant number is the sum of all its proper divisors, denoted by sum(n), is greater than the number's 
 *  value.)
 */

import java.util.Scanner;

class Solution {

    static int checkNum(int N, int i, int sum) {

        if (i > N / 2)
            return sum;

        if (N % i == 0)
            sum += i;

        return checkNum(N, i + 1, sum);
    }

    static String isAbundantNumber_Recursion(int N) {

        return (checkNum(N, 1, 0) > N) ? "Abundant Number" : "Not an Abundant Number";
    }

    static String isAbundantNumber_Loop(int N) {

        int sum = 0;

        for (int i = 1; i <= N / 2; i++) {

            if (N % i == 0)
                sum += i;
        }

        return (sum > N) ? "Abundant Number" : "Not an Abundant Number";
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Number :: ");
        int num = sc.nextInt();

        System.out.println(Solution.isAbundantNumber_Loop(num));

        System.out.println(Solution.isAbundantNumber_Recursion(num));

        sc.close();
    }
}