/*
 *  Write a program to check if a given number is a Magic Number or not. (A Magic
 *  Number is a number in which the eventual sum of the digits is equal to 1).
 */

import java.util.Scanner;

class Solution {

    static int digitSum(int N) {

        if (N <= 0)
            return 0;

        return N % 10 + digitSum(N / 10);
    }

    static int MagicNum_Recursion(int N, int sum) {

        if (N <= 0)
            return digitSum(sum);

        return MagicNum_Recursion(N / 10, sum + N % 10);
    }

    static int MagicNum_Loop(int N) {

        int sum = digitSum(N);
        return digitSum(sum);
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Number :: ");
        int num = sc.nextInt();

        if (Solution.MagicNum_Loop(num) == 1)
            System.out.println("Magic Number");
        else
            System.out.println("Not a Magic Number");

        if (Solution.MagicNum_Recursion(num, 0) == 1)
            System.out.println("Magic Number");
        else
            System.out.println("Not a Magic Number");

        sc.close();
    }
}