/*
 *  Write a program to check if a given number is an Armstrong number or not.
 *  (An Armstrong number is a number that is equal to the sum of its own digits each raised to the power of the 
 *  number of digits.)
 */

import java.util.Scanner;

class Solution {

    static int isArmStrongNumber_Recursion(int N, int sum, int digCount) {

        if (N <= 0)
            return sum;

        return isArmStrongNumber_Recursion(N / 10, sum + (int) Math.pow(N % 10, digCount), digCount);
    }

    static int isArmStrongNumber_Loop(int N) {

        int digCount = Integer.toString(N).length();
        int sum = 0;

        for (int i = N; i != 0; i /= 10)
            sum += (int) Math.pow(i % 10, digCount);

        return sum;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Number :: ");
        int num = sc.nextInt();

        if (Solution.isArmStrongNumber_Loop(num) == num)
            System.out.println("ArmStrong Number");
        else
            System.out.println("Not a ArmStrong Number");

        if (Solution.isArmStrongNumber_Recursion(num, 0, Integer.toString(num).length()) == num)
            System.out.println("ArmStrong Number");
        else
            System.out.println("Not a ArmStrong Number");

        sc.close();
    }
}