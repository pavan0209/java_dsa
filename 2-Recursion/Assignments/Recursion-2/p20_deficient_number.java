/*
    Write a program to determine whether a given positive integer is a Deficient Number or not. A Deficient
    Number is a positive integer where the sum of its proper divisors is less than the number itself.
 */

class Solution {

    static int sum = 0;
    static int i = 1;
    static boolean isDeficient_Recursion(int N) {

        if(i > N/2) 
            return (sum < N) ? true : false;
            
        if(N % i == 0)
            sum += i;
        
        i++;
        return isDeficient_Loop(N);
    }

    static boolean isDeficient_Loop(int N) {

        int sum = 0;

        for(int i = 1; i <= N/2; i++) {

            if(N % i == 0)
                sum += i ;
        }

        return (sum < N) ? true : false;
    }

    public static void main(String[] args) {

        System.out.println(isDeficient_Loop(2));
        System.out.println(isDeficient_Recursion(2));
    }
}