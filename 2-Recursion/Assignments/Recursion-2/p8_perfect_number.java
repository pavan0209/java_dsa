/*
 *  Write a program to check whether a given positive integer is a Perfect Number or not.
 * 
 * (A Perfect Number is a positive integer that is equal to the sum of its proper divisors, excluding itself.)
 */

import java.util.Scanner;

class Solution {

    static int perfectNumber_Recursion(int N, int sum, int i) {

        if (i > N / 2)
            return sum;

        if (N % i == 0)
            sum += i;

        return perfectNumber_Recursion(N, sum, i + 1);
    }

    static int perfectNumber_Loop(int N) {

        int sum = 0;

        for (int i = 1; i <= N / 2; i++) {

            if (N % i == 0)
                sum += i;
        }

        return sum;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Number :: ");
        int num = sc.nextInt();

        if (Solution.perfectNumber_Loop(num) == num)
            System.out.println("Perfect Number");
        else
            System.out.println("Not a Perfect Number");

        if (Solution.perfectNumber_Recursion(num, 0, 1) == num)
            System.out.println("Perfect Number");
        else
            System.out.println("Not a Perfect Number");

        sc.close();
    }
}