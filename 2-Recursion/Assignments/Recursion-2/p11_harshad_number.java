/*
 *  Write a program to check if a given number is a Harshad Number or not.
 *  (A Harshad Number is a number that is divisible by the sum of its digits.)
 */

import java.util.Scanner;

class Solution {

    static int checkNum(int N) {

        if (N <= 0)
            return 0;

        return N % 10 + checkNum(N / 10);
    }

    static String isHarshadNumber_Recursion(int N) {

        return (N % checkNum(N) == 0) ? "Harshad Number" : "Not a Harshad Number";
    }

    static String isHarshadNumber_Loop(int N) {

        int sum = 0;

        for (int i = N; i != 0; i /= 10)
            sum += i % 10;

        return (N % sum == 0) ? "Harshad Number" : "Not a Harshad Number";
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Number :: ");
        int num = sc.nextInt();

        System.out.println(Solution.isHarshadNumber_Loop(num));

        System.out.println(Solution.isHarshadNumber_Recursion(num));

        sc.close();
    }
}