/*
 *  1. WAP to find the factorial of a number.
 */

import java.util.Scanner;

class Solution {

    static int factorial_Recursion(int N) {

        if (N <= 1)
            return 1;

        return N * factorial_Recursion(N - 1);
    }

    static int factorial_Loop(int N) {

        int fact = 1;

        for (int i = 2; i <= N; i++)
            fact *= i;

        return fact;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Number :: ");
        int num = sc.nextInt();

        System.out.println(Solution.factorial_Loop(num));

        System.out.println(Solution.factorial_Recursion(num));

        sc.close();
    }
}