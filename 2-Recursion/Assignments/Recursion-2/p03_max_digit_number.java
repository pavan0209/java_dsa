/*
 *  3. Write a program to print the maximum digit in a given number.
 */

import java.util.Scanner;

class Solution {

    static int maxDigit_Recursion(int N, int max) {

        if (N <= 0)
            return max;

        return maxDigit_Recursion(N / 10, (N % 10 > max) ? max = N % 10 : max);
    }

    static int maxDigit_Loop(int N) {

        int max = Integer.MIN_VALUE;

        for (int i = N; i != 0; i /= 10) {

            if (i % 10 > max)
                max = i % 10;
        }

        return max;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Number :: ");
        int num = sc.nextInt();

        System.out.println(Solution.maxDigit_Loop(num));

        System.out.println(Solution.maxDigit_Recursion(num, Integer.MIN_VALUE));

        sc.close();
    }

}