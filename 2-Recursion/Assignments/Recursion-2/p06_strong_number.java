/*
 *  6 Write a program to check whether a given number is a Strong Number or not.
 */

import java.util.Scanner;

class Solution {

    static int factorial(int N) {

        if(N <= 1)
            return 1;

        return N * factorial(N-1);
    } 

    static int strongNum_Recursion(int N, int sum) {

        if (N <= 0)
            return sum;

        return strongNum_Recursion(N / 10, sum+factorial(N%10));
    }

    static int strongNum_Loop(int N) {

        int sum = 0;

        for (int i = N; i != 0; i /= 10)
            sum += factorial(i % 10);

        return sum;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Number :: ");
        int num = sc.nextInt();

        if(Solution.strongNum_Loop(num) == num)
            System.out.println("Strong Number");
        else
            System.out.println("Not a Strong Number");

        if (Solution.strongNum_Recursion(num, 0) == num)
            System.out.println("Strong Number");
        else
            System.out.println("Not a Strong Number");
    }
}