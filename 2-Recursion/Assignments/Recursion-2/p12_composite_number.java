/*
 *  Write a program to determine whether a given positive integer is a composite number or not.
 */

import java.util.Scanner; 

class Solution {

    static int checkNum(int N, int i,int cnt) {

        if(i > N)
            return cnt;

        if( N % i == 0)
            cnt++;

        return checkNum(N, i+1, cnt);
    }

    static String isCompositeNumber_Recursion(int N) {

        return (checkNum(N, 1, 0) > 2) ? "Composite Number" : "Not a Composite Number";
    }

    static String isCompositeNumber_Loop(int N) {

        int cnt = 0;

        for(int i = 1; i <= N; i++) {

            if(N % i == 0 )
            cnt++;

            if(cnt  > 2)
                break;
        }

        return (cnt > 2) ? "Composite Number" : "Not a Composite Number";
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Number :: ");
        int num = sc.nextInt();

        System.out.println(Solution.isCompositeNumber_Loop(num));

        System.out.println(Solution.isCompositeNumber_Recursion(num));

        sc.close();
    }
}