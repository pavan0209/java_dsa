/*
    Write a program to check if a given number is a Duck Number or not.
    (A Duck Number is a number which doesn't start with a zero but has at least one digit as zero.)
 */

class Solution {

    static boolean isDuck_Loop(int N) {

        String str = Integer.toString(N);

        if(str.charAt(0) == '0')
            return false;
        else if(str.indexOf('0') == -1)
            return false;
        else
            return true;
    }

    public static void main(String[] args) {    

        System.out.println(isDuck_Loop(165));
    }
}