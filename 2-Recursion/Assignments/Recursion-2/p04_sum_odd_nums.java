/*
 *  1. Write a program to print the sum of odd numbers up to a given number..
 */

import java.util.Scanner;

class Solution {

    static int OddNumSum_Recursion(int N, int sum, int i) {

        if (i > N)
            return sum;

        return OddNumSum_Recursion(N, sum + i, i + 2);
    }

    static int OddNumsSum_Loop(int N) {

        int sum = 0;

        for (int i = 1; i <= N; i += 2)
            sum += i;

        return sum;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Number :: ");
        int num = sc.nextInt();

        System.out.println(Solution.OddNumsSum_Loop(num));

        System.out.println(Solution.OddNumSum_Recursion(num, 0, 1));

        sc.close();
    }
}