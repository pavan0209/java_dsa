/*
    Write a program that determines whether a given number is a cube number or not.
    (A cube number is defined as a number that is the cube of an integer.)
 */

class Solution {

    static int i = 1;

    static boolean isCube_Recursion(int N) {

        if (i * i * i > N) {
            i--;
            return (i * i * i == N) ? true : false;
        }

        i++;
        return isCube_Recursion(N);
    }

    static boolean isCube_Loop(int N) {

        int i = 1;
        while (i * i * i <= N)
            i++;

        i--;
        return (i * i * i == N) ? true : false;
    }

    public static void main(String[] args) {

        System.out.println(isCube_Loop(27));
        System.out.println(isCube_Recursion(64));
    }
}