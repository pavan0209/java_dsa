/*
 *  6. WAP to calculate the product of digits of a given number.
 */

import java.util.Scanner;

class Solution {

    static int digitProduct_Recursion(int N) {

        if (N < 10)
            return N;

        return N % 10 * digitProduct_Recursion(N / 10);
    }

    static int digitProduct_Loop(int N) {

        int prod = 1;
        for (int i = N; i != 0; i /= 10)
            prod *= i % 10;

        return prod;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Number :: ");
        int num = sc.nextInt();

        System.out.println(Solution.digitProduct_Loop(num));

        System.out.println(Solution.digitProduct_Recursion(num));

        sc.close();
    }

}