/*
 *  Write a program to determine whether a given number is square number or not.
 */

class Solution {

    static int i = 1;

    static boolean isSquareNo_Recursion(int N) {

        if(i * i > N){
            i--;
            if(i * i == N)
                return true;
            return false;
        }
        i++;
        return isSquareNo_Recursion(N);
    }

    static boolean isSquareNo_Loop(int N) {

        int i = 1;

        while(i * i <= N) {
            i++;
        }
        i--;
        if(i * i == N)
            return true;
        else 
            return false;

    }

    public static void main(String[] args) {

        System.out.println(isSquareNo_Loop(25));
        System.out.println(isSquareNo_Recursion(0));
    }
}