/*
 *  Q2. WAP to replace 'a' in a string with '1'
 */

import java.util.Scanner; 

class Solution {

    static char ch = 'l';

    static String replace_a_l_Recursion(String str) {

        if(str == null || str.length() <= 0) 
            return str;

        /*
        if(str.charAt(0) == 'a' || str.charAt(0) == 'A')
            return 'l' + replace_a_l_Recursion(str.substring(1));
        else
            return  str.charAt(0) + replace_a_l_Recursion(str.substring(1));
        */

        if(str.charAt(0) == 'a' || str.charAt(0) == 'A')
            ch = 'l';
        else
            ch = str.charAt(0);

        return  ch + replace_a_l_Recursion(str.substring(1));
    }

    static String replace_a_l_Loop(String str) {

        String res = "";

        for(int i = 0; i < str.length(); i++) {

            char ch = str.charAt(i);
            if(ch == 'A' ||  ch == 'a') {
                res += 'l';
                continue;
            }
            res += str.charAt(i);
        }

        return res;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter String :: ");
        String str = sc.nextLine();

        System.out.println(Solution.replace_a_l_Loop(str));
        System.out.println(Solution.replace_a_l_Recursion(str));

        sc.close();
    }
}