/*
 *  Q5. WAP to check whether a given string is a palindrome string or not.
 */

import java.util.Scanner; 

class Solution {

    static String rev = "";
    static int i = 0;

    /*
    static boolean isPalindrome_Recursion(String str) {

        if(i >= str.length())
            return (str.equals(rev)) ? true : false;

        rev = str.charAt(i) + rev;
        i++;

        return isPalindrome_Recursion(str);
    }
    */

    static boolean isPalindrome_Recursion(String str, int start, int end) {

        if(start >= end)
            return true;

        if(str.charAt(start) != str.charAt(end))
            return false;

        return isPalindrome_Recursion(str, start+1, end-1);
    }

    static boolean isPalindrome_Loop(String str) {

        String rev = "";

        for(int i = 0; i < str.length(); i++) 
            rev = str.charAt(i) + rev;

        return (rev.equals(str)) ? true : false;
    }

     public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter String :: ");
        String str = sc.nextLine();

        System.out.println(Solution.isPalindrome_Loop(str));
        System.out.println(Solution.isPalindrome_Recursion(str, 0, str.length()-1));

        sc.close();
    }
}