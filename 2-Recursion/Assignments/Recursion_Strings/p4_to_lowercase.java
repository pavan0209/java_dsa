/*
 *  Q3. WAP to convert all characters to lowercase.
 */

import java.util.Scanner;

class Solution {

    static char ch = 'A';
    
    static String toLowerCase_Recursion(String str) {
        
        if(str == null || str.length() <= 0)
            return str;
            
        if(str.charAt(0) >= 'A' && str.charAt(0) <= 'Z')    
            ch = (char)(str.charAt(0)+32);
        else
            ch = str.charAt(0);

        return ch + toLowerCase_Recursion(str.substring(1));
    }

    static String toLowerCase_Loop(String str) {

        String res = "";

        for(int i = 0; i < str.length(); i++) {

            if(str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') {
                res += (char)(str.charAt(i)+32);
                continue;
            }
            res += str.charAt(i);
        }

        return res;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter String :: ");
        String str = sc.nextLine();

        System.out.println(Solution.toLowerCase_Loop(str));
        System.out.println(Solution.toLowerCase_Recursion(str));

        sc.close();
    }
}