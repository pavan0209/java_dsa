/*
 *  WAP to print the result of removing duplicates from a given string.
 * 
 *  Input: Str = HappyNewYear
 *  Output: HapyNewYr
 */

import java.util.Scanner; 

class Solution {

    static boolean ifAlreadyPresent(String str, char ch) {

        if(str == null || str.length() <= 0)
            return false;

        if(str.charAt(0) == ch)
            return true;

        return ifAlreadyPresent(str.substring(1), ch);
    }

    static String res = "";
    static int i = 0;

    static String removeDuplicates_Recursion(String str) {

        if(i >= str.length()) 
            return res;

        if(!ifAlreadyPresent(str.substring(0,i), str.charAt(i)))
            res += str.charAt(i);
        
        i++;
        return removeDuplicates_Recursion(str);
    }

    static String removeDuplicates_Loop(String str) {

        String res = String.valueOf(str.charAt(0));

        for(int i = 1; i < str.length(); i++) {

            if(!ifAlreadyPresent(str.substring(0,i), str.charAt(i)))
                res += str.charAt(i);
        }

        return res;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter String :: ");
        String str = sc.nextLine();

        System.out.println(Solution.removeDuplicates_Loop(str));
        System.out.println(Solution.removeDuplicates_Recursion(str));

        sc.close();
    }
}