/*
 *  Q1. WAP to count the vowels in string.
 */

import java.util.Scanner; 

class Solution {

    static int cnt = 0;

    static int countVowels_Recursion(String str) {

        if(str.length() <= 0 )
            return cnt;

        char ch = str.charAt(0);
        if(ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U' ||
           ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')
            cnt = cnt+1;
        

        return countVowels_Recursion(str.substring(1));
    }

    static int countVowels_Loop(String str) {

        int cnt = 0;

        for(int i = 0; i < str.length(); i++) {

            char ch = str.charAt(i);
            if(ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U' ||
               ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')
                cnt = cnt+1;
        }

        return cnt;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter String :: ");
        String str = sc.nextLine();

        System.out.println(Solution.countVowels_Loop(str));
        System.out.println(Solution.countVowels_Recursion(str));

        sc.close();
    }
}