/*
 *  Q3. WAP to convert all characters in the array to uppercase.
 */

import java.util.Scanner;

class Solution {

    static int i = 0;

    static char[] toUpperCase_Recursion(char carr[]) {
        
        if(i >= carr.length)
            return carr;

        if(carr[i] >= 'a' && carr[i] <= 'z')
            carr[i] = (char)(carr[i] - 32);
        
        i++;
        return toUpperCase_Recursion(carr);
    }

    static char[] toUpperCase_Loop(char carr[]) {

        for(int i = 0; i < carr.length; i++) {

            if(carr[i] >= 'a' && carr[i] <= 'z')
                carr[i] = (char)(carr[i] - 32);
        }

        return carr;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter String :: ");
        String str = sc.nextLine();

        System.out.println(Solution.toUpperCase_Loop(str.toCharArray()));
        System.out.println(Solution.toUpperCase_Recursion(str.toCharArray()));

        sc.close();
    }
}