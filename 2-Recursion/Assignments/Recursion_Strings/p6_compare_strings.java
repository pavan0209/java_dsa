/*
 *  Q5. WAP to check whether a given string is a equal to other string or not.
 */

import java.util.Scanner; 

class Solution {

    static boolean compareStrings_Recursion(String str1, String str2) {

        if(str1.length() != str2.length())
            return false;

        if(str1.length() <= 0)
            return true;
        
        if(str1.charAt(0) != str2.charAt(0))
            return false;    
            
        return compareStrings_Recursion(str1.substring(1), str2.substring(1));
    }

    static boolean compareStrings_Loop(String str1, String str2) {

        if(str1.length() != str2.length())
            return false;

        for(int i = 0; i < str1.length(); i++) {

            if(str1.charAt(i) != str2.charAt(i))
                return false;
        }

        return true;
    }

     public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter First String :: ");
        String str1 = sc.nextLine();
        System.out.print("Enter Second String :: ");
        String str2 = sc.nextLine();

        System.out.println(Solution.compareStrings_Loop(str1, str2));
        System.out.println(Solution.compareStrings_Recursion(str1, str2));

        sc.close();
    }
}