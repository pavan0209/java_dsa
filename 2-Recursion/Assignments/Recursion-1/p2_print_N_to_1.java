/*
 *  2. WAP to display the first 10 natural numbers in reverse order.
 */

class Solution {

    static void printNTo1_Recursion(int N) {

        if (N == 0)
            return;

        System.out.print(N + "  ");
        printNTo1_Recursion(N - 1);
    }

    static void printNTo1_Loop(int N) {

        for (int i = N; i >= 1; i--) {

            System.out.print(i + "  ");
        }
        System.out.println();
    }

    public static void main(String[] args) {

        Solution.printNTo1_Loop(10);

        Solution.printNTo1_Recursion(10);

        System.out.println();
    }

}