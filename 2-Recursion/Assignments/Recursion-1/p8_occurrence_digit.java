/*
 *  8. WAP to count the occurrence of a specific digit in a given number.
 */

class Solution {

    static int digitOccurrence_Recursion(int N, int target, int cnt) {

        if (N <= 0)
            return cnt;

        if (N % 10 == target)
            cnt += 1;

        return digitOccurrence_Recursion(N / 10, target, cnt);
    }

    static int digitOccurrence_Loop(int N, int target) {

        int cnt = 0;

        for (int i = N; i != 0; i /= 10) {

            if (i % 10 == target)
                cnt++;
        }

        return cnt;
    }

    public static void main(String[] args) {

        System.out.println(Solution.digitOccurrence_Loop(100, 0));

        System.out.println(Solution.digitOccurrence_Recursion(100, 0, 0));
    }
}