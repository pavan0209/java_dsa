/*
 *  3. WAP to print the sum of n natural numbers.
 */

class Solution {

    /*static int sumOfNaturalNums_Recursion(int N, int sum) {

        if(N == 0)
            return sum;
        
        return sumOfNaturalNums_Recursion(N-1, sum + N);
    }*/

    static int sumOfNaturalNums_Recursion(int N) {

        if (N == 1)
            return 1;

        return N + sumOfNaturalNums_Recursion(N - 1);
    }

    static int sumOfNaturalNums_Loop(int N) {

        int sum = 0;
        for (int i = N; i >= 1; i--)
            sum += i;

        return sum;
    }

    public static void main(String[] args) {

        System.out.println(Solution.sumOfNaturalNums_Loop(10));

        System.out.println(Solution.sumOfNaturalNums_Recursion(10));
    }

}