/*
 *  1. WAP to print the numbers between 1 to 10.
 */

class Solution {

    static void print1ToN_Recursion(int N) {

        if (N == 0)
            return;

        print1ToN_Recursion(N - 1);
        System.out.print(N + "  ");
    }

    static void print1ToN_Loop(int N) {

        for (int i = 1; i <= N; i++) {

            System.out.print(i + "  ");
        }
        System.out.println();
    }

    public static void main(String[] args) {

        Solution.print1ToN_Loop(10);

        Solution.print1ToN_Recursion(10);

        System.out.println();
    }

}