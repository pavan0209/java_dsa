/*
 *  4. WAP to print the length of digits in a number.
 */

class Solution {

    static int numLength_Recursion(int N, int digCount) {

        if (N < 10)
            return digCount + 1;

        return numLength_Recursion(N / 10, digCount + 1);
    }

    static int numLength_Loop(int N) {

        int digCount = 0;
        for (int i = N; i != 0; i /= 10)
            digCount++;

        return digCount;
    }

    public static void main(String[] args) {

        System.out.println(Solution.numLength_Loop(10));

        System.out.println(Solution.numLength_Recursion(10, 0));
    }

}