/*
 *  4. WAP to check whether the given number is prime or not.
 */

class Solution {

    static int isPrime_Recursion(int N, int i, int digCount) {

        if (i > N)
            return digCount;

        if (N % i == 0)
            digCount += 1;

        return isPrime_Recursion(N, i + 1, digCount);
    }

    static String isPrime_Loop(int N) {

        int cnt = 0;

        for (int i = 1; i <= N; i++) {

            if (N % i == 0)
                cnt++;

            if (cnt > 2)
                break;
        }

        return (cnt == 2) ? "Prime Number" : "Not a Prime Number";
    }

    public static void main(String[] args) {

        System.out.println(Solution.isPrime_Loop(2));

        if (Solution.isPrime_Recursion(2, 1, 0) == 2)
            System.out.println("Prime Number");
        else
            System.out.println("Not a Prime Number");
    }

}