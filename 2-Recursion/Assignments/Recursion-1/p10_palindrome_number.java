/*
 *  10. WAP to check whether the given number is palindrome or not.
 */

class Solution {

    static int isPalindrome_Recursion(int N, int rev) {

        if (N <= 0)
            return rev;

        return isPalindrome_Recursion(N / 10, rev * 10 + N % 10);
    }

    static String isPalindrome_Loop(int N) {

        int rev = 0;

        for (int i = N; i != 0; i /= 10)
            rev = rev * 10 + i % 10;

        return (N == rev) ? "Palindrome Number" : "Not a Palindrome Number";
    }

    public static void main(String[] args) {

        System.out.println(Solution.isPalindrome_Loop(12321));

        int num = 12321;

        if (Solution.isPalindrome_Recursion(num, 0) == num)
            System.out.println("Palindrome Number");
        else
            System.out.println("Not a palindrome Number");
    }
}