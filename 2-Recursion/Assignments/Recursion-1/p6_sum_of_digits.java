/*
 *  6. WAP to calculate the sum of digits of a given positive integer..
 */

class Solution {

    static int digitSum_Recursion(int N) {

        if (N < 0)
            return -1;

        if (N < 10)
            return N;

        return N % 10 + digitSum_Recursion(N / 10);
    }

    static int digitSum_Loop(int N) {

        int sum = 0;
        for (int i = N; i != 0; i /= 10)
            sum += i % 10;

        return sum;
    }

    public static void main(String[] args) {

        System.out.println(Solution.digitSum_Loop(123));

        System.out.println(Solution.digitSum_Recursion(-20));
    }

}