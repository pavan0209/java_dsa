/*  
 *  9. WAP to print string in reverse order.
 */

class Solution {

    static String strRev_Recursion(String org, String rev, int i) {

        if (org.length() == i)
            return rev;

        return strRev_Recursion(org, org.charAt(i) + rev, i + 1);
    }

    static String strRev_Loop(String org) {

        String rev = "";

        for (int i = 0; i < org.length(); i++)
            rev = org.charAt(i) + rev;

        return rev;
    }

    public static void main(String[] args) {

        System.out.println(Solution.strRev_Loop("pavan"));

        System.out.println(Solution.strRev_Recursion("Pavan", "", 0));
    }
}