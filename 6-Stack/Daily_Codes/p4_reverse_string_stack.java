/*
 *
 */

import java.util.*; 

class Solution {

    static String strReverse(String str) {

        Stack<Character> stk = new Stack<>();
        String rev = "";

        for(int i = 0; i < str.length(); i++)
            stk.push(str.charAt(i));

        while(!stk.empty())
            rev += stk.pop();

        return rev;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter String :: ");
        String str = sc.nextLine();

        System.out.println("\nOriginal String :: " + str);
        System.out.println("Revsersed String :: " + Solution.strReverse(str) + "\n");

        sc.close();
    }
}