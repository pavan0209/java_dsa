/*
 *  Merge Two Sorted Stacks.
 */

import java.util.*;

class StackDemo {

    Stack<Integer> sortStack(Stack<Integer> s1, Stack<Integer> s2) {

        Stack<Integer> s3 = new Stack<>();

        while (!s1.empty() && !s2.empty()) {

            if (s1.peek() > s2.peek())
                s3.push(s1.pop());
            else
                s3.push(s2.pop());
        }

        while (!s1.empty())
            s3.push(s1.pop());

        while (!s2.empty())
            s3.push(s2.pop());

        while (!s3.empty())
            s1.push(s3.pop());

        return s1;
    }
}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        StackDemo stk = new StackDemo();

        System.out.print("\nEnter no. of elements for stack1 :: ");
        int s1_size = sc.nextInt();

        Stack<Integer> s1 = new Stack<>();
        System.out.println("Enter data for stack1 in sorted manner\n");

        for (int i = 0; i < s1_size; i++) {

            System.out.print("Enter data :: ");
            int data = sc.nextInt();

            s1.push(data);
        }

        System.out.print("\nEnter no. of elements for stack2 :: ");
        int s2_size = sc.nextInt();

        Stack<Integer> s2 = new Stack<>();
        System.out.println("Enter data for stack2 in sorted manner :: \n");

        for (int i = 0; i < s2_size; i++) {

            System.out.print("Enter data :: ");
            int data = sc.nextInt();

            s2.push(data);
        }

        s1 = stk.sortStack(s1, s2);

        System.out.println("\n" + s1);

        sc.close();
    }
}