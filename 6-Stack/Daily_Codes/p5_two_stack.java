/*
 *  Implement two stacks in one array
 */

import java.util.*;

class TwoStack {

    int size;
    int stackArr[];
    int top1;
    int top2;

    TwoStack(int size) {

        this.size = size;
        stackArr = new int[size];
        top1 = -1;
        top2 = size;
    }

    void push1(int data) {

        if(top2 - top1 > 1) {

            top1++;
            stackArr[top1] = data;
        }
        else {

            System.out.println("\nStack Overflow");
        }
    }

    void push2(int data) {

        if(top2 - top1 > 1) {

            top2--;
            stackArr[top2] = data;
        }
        else {

            System.out.println("\nStack Overflow");
        }
    }

    int flag1 = 0;
    int pop1() {

        if(top1 == -1) {

            flag1 = 0;
            return -1;
        }

        flag1 = 1;
        int val = stackArr[top1];
        top1--;

        return val;
    }

    int flag2 = 0;
    int pop2() {

        if(top2 == size) {

            flag2 = 0;
            return -1;
        }
        
        flag2 = 1;
        int val = stackArr[top2];
        top2++;

        return val;
    }

    void printStack1() {

        if(top1 == -1) {
            System.out.println("\nStack1 is Empty...!!");
            return;
        }

        System.out.println();
        for(int i = 0; i <= top1; i++)
            System.out.print(stackArr[i] + "  ");
        System.out.println();
    }

    void printStack2() {

        if(top2 == size) {

            System.out.println("\nStack2 is Empty...!!");
            return;
        }
        System.out.println();
        for(int i = size-1; i >= top2; i--)
            System.out.print(stackArr[i] + "  ");
        System.out.println();
    }
} 

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("\nEnter size of stack :: ");
        int size = sc.nextInt();

        TwoStack stk = new TwoStack(size);

        char ch = 'N';

        do {

            System.out.println("\n1 : push element in stack 1");
            System.out.println("2 : push element in stack 2");
            System.out.println("3 : pop element from stack 1");
            System.out.println("4 : pop element from stack 2");
            System.out.println("5 : print stack 1");
            System.out.println("6 : print stack 2");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1:
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            stk.push1(data);
                        }
                        break;

                case 2:
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            stk.push2(data);
                        }
                        break;

                case 3 :
                        {
                            int val = stk.pop1();

                            if(stk.flag1 == 0) 
                                System.out.println("\nStack Underflow..!!");
                            else
                                System.out.println("\n" + val + " is popped from stack1");
                        }
                        break;
                 
                case 4 :
                        {
                            int val = stk.pop2();

                            if(stk.flag2 == 0) 
                                System.out.println("\nStack Underflow..!!");
                            else
                                System.out.println("\n" + val + " is popped from stack2");
                        }
                        break;

                case 5 :
                        stk.printStack1();
                        break;

                case 6 :
                        stk.printStack2();
                        break;
            
                default:
                        System.out.println("\nWrong choice..!!");
                        break;
            }

            System.out.print("\nDo you want to continue?? :: ");
            ch = sc.next().charAt(0);

        } while(ch == 'Y' || ch == 'y');

        sc.close();
    }
}