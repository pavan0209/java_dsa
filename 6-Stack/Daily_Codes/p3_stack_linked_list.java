/*
 *  Stack implementation using Linked List
 */
import java.util.Scanner;

class Node {

    int data;
    Node next;

    Node(int data) {

        this.data = data;
        next = null;
    }
}

class StackDemo {
    
    Node head = null;

    boolean isEmpty() {

        if(head == null)
            return true;
        else 
            return false;
    }

    void push(int data) {
        
        Node newNode = new Node(data);

        if(head == null) {

            head = newNode;
        }
        else {

            newNode.next = head;
            head = newNode;
        }
    }

    int flag = 0;

    int pop() {

        if(head == null) {

            flag = 0;
            return -1;
        }

        flag = 1;
        int data = head.data;
        head = head.next;
        return data;
    }

    int peek() {

        if(head == null) {
            
            flag = 0;
            return -1;
        }
        
        flag = 1; 
        return head.data;
    }

    int size() {

        if(head == null)
            return 0;

        Node temp = head;
        int cnt = 0;

        while(temp != null) {

            cnt++;
            temp = temp.next;
        }

        return cnt;
    }

    void printStack() {

        if(head == null) {

            System.out.println("\nStack is Empty...!!");
            return;
        }
        else {

            System.out.println();

            Node temp = head;

            while(temp != null) {

                System.out.print(temp.data + "  ");
                temp = temp.next;
            }

            System.out.println();
        }
    }
}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        StackDemo stk = new StackDemo();

        char ch = 'N';

        do {

            System.out.println("\n1 : Push Elements in Stack");
            System.out.println("2 : Pop Element from Stack");
            System.out.println("3 : Peek Element Of Stack");
            System.out.println("4 : Is Stack Empty ?? ");
            System.out.println("5 : Size of Stack");
            System.out.println("6 : Print Elements of Stack");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 : 
                        {   
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            stk.push(data);
                        }
                        break;

                case 2 : 
                        {
                            int retVal = stk.pop();
                            if(stk.flag == 0) 
                                System.out.println("\nStack UnderFlow...!!");
                            else
                                System.out.println("\n" + retVal + " is popped..");
                        }
                        
                        break;

                case 3 :
                        {
                            int retVal = stk.peek();
                            if(stk.flag == 0) 
                                System.out.println("\nStack UnderFlow...!!");
                            else
                                System.out.println("\nElement at Peek(top) :: " + retVal);
                        }
                        
                        break;

                case 4 :
                        {
                            if(stk.isEmpty())
                                System.out.println("\nStack is Empty...!!");
                            else 
                                System.out.println("\nStack is Not Empty...");
                        }
                        break;

                case 5 :
                        System.out.println("\nSize of Stack :: " + stk.size());
                        break;

                case 6 :
                        stk.printStack();
                        break;

                default :
                        System.out.println("\nInvalid Choice ..!!");
                        break;
            }

            System.out.print("\nDo you want to continue ?? :: ");
            ch = sc.next().charAt(0);

        } while(ch == 'Y' || ch == 'y');

        sc.close();
    }
}