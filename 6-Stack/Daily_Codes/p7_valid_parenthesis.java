/*
 *  Check whether the expression is valid parenthesis or not. If yes return true otherwise return false.
 */

import java.util.*;

class StackDemo {

    boolean validParenthesis(String str) {

        Stack<Character> s = new Stack<>();

        for(int i = 0; i < str.length(); i++) {

            char ch = str.charAt(i);
            if(ch == '(' || ch == '[' || ch == '{'){

                s.push(ch);
            }
            else {

                if(s.empty())
                    return false;

                char top = s.peek();
                if((top == '(' && ch == ')') || (top == '[' && ch == ']') || (top == '{' && ch == '}')) {

                    s.pop();
                }
                else {
                    return false;
                }
            }
        }

        if(s.empty())
            return true;
        else
            return false;
    }
}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        StackDemo stk = new StackDemo();

        System.out.print("\nEnter expression :: ");
        String str = sc.next();

        if(stk.validParenthesis(str))
            System.out.println("Balanced");
        else
            System.out.println("Not Balanced");

        sc.close();
    }
}