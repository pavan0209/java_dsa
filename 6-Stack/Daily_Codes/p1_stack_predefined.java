/*
 *  Stack implementation using predefined class
 */

import java.util.*;

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        Stack<Integer> stk = new Stack<>();

        char ch = 'N';

        do {

            System.out.println("\n1 : Push Elements in Stack");
            System.out.println("2 : Pop Element from Stack");
            System.out.println("3 : Peek Element Of Stack");
            System.out.println("4 : Is Stack Empty");
            System.out.println("5 : Size of Stack");
            System.out.println("6 : Print Elements of Stack");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 : 
                        {   
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            stk.push(data);
                        }
                        break;

                case 2 :
                        System.out.println("\n" + stk.pop() + " is popped..");
                        break;

                case 3 :
                        System.out.println("\nElement At Peak(top) :: " + stk.peek());
                        break;

                case 4 :
                        {
                            if(stk.empty())
                                System.out.println("\nStack is Empty ...!!");
                            else 
                                System.out.println("\nStack is Not Empty...");
                        }
                        break;

                case 5 :
                        System.out.println("\nSize of Stack :: " + stk.size());
                        break;

                case 6 :
                        System.out.println(stk);
                        break;

                default :
                        System.out.println("Invalid Choice ..!!");
                        break;
            }

            System.out.print("\nDo you want to continue ?? :: ");
            ch = sc.next().charAt(0);

        } while(ch == 'Y' || ch == 'y');

        sc.close();
    }
}