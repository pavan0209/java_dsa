/*
 *  Stack implementation using Array
 */
import java.util.Scanner;

class StackDemo {

    int size;
    int sArr[];
    int top = -1;

    StackDemo(int size) {

        this.size = size;
        sArr = new int[size];
    }
    
    boolean isEmpty() {

        if (top == -1) 
            return true;
        else
            return false;
    }

    boolean isFull() {

        if(top == size-1) 
            return true;
        else 
            return false;
    }

    void push(int data) {
        
        if(isFull()) {

            System.out.println("\nStack OverFlow..!!");
            return;
        }
        else {

            top++;

            sArr[top] = data;
        }
    }

    int flag = 0;

    int pop() {

        if(isEmpty()) {
            flag = 0;
            return -1;
        }
        else {

            flag = 1;
            int data = sArr[top];
            top--;

            return data;
        }
    }

    int peek() {

        if(isEmpty()) {
            flag = 0;
            return -1;
        }
        
        flag = 1; 
        return sArr[top];
    }

    int size() {

        return top;
    }

    void printStack() {

        if(isEmpty()) {

            System.out.println("\nStack is Empty...!!");
            return;
        }
        else {

            System.out.println();

            for(int i = 0; i <= top; i++) {

                System.out.print(sArr[i] + "  ");
            }

            System.out.println();
        }
    }
}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("\nEnter Size of Stack :: ");
        int size = sc.nextInt();

        StackDemo stk = new StackDemo(size);

        boolean flag = true;

        do {

            System.out.println("\n1 : Push Elements in Stack");
            System.out.println("2 : Pop Element from Stack");
            System.out.println("3 : Peek Element Of Stack");
            System.out.println("4 : Is Stack Empty ?? ");
            System.out.println("5 : Is Stack Full ?? ");
            System.out.println("6 : Size of Stack");
            System.out.println("7 : Print Elements of Stack");
            System.out.println("8 : Exit");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 : 
                        {   
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            stk.push(data);
                        }
                        break;

                case 2 : 
                        {
                            int retVal = stk.pop();
                            if(stk.flag == 0) 
                                System.out.println("\nStack UnderFlow...!!");
                            else
                                System.out.println("\n" + retVal + " is popped..");
                        }
                        
                        break;

                case 3 :
                        {
                            int retVal = stk.peek();
                            if(stk.flag == 0) 
                                System.out.println("\nStack UnderFlow...!!");
                            else
                                System.out.println("\nElement at Peek(top) :: " + retVal);
                        }
                        
                        break;

                case 4 :
                        {
                            if(stk.isEmpty())
                                System.out.println("\nStack is Empty...!!");
                            else 
                                System.out.println("\nStack is Not Empty...");
                        }
                        break;

                case 5 :
                        {
                            if(stk.isFull())
                                System.out.println("\nStack is Full ...!!");
                            else 
                                System.out.println("\nStack is Not Full...");
                        }
                        break;

                case 6 :
                        System.out.println("\nSize of Stack :: " + stk.size());
                        break;

                case 7 :
                        stk.printStack();
                        break;

                case 8 :
                        flag = false;
                        break;

                default :
                        System.out.println("\nInvalid Choice ..!!");
                        break;
            }
        } while(flag);

        sc.close();
    }
}