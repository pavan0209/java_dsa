/*
    15. Sort a stack
    
    Given a stack, the task is to sort it such that the top of the stack has the greatest element.
    
    Example 1:
    Input:
        Stack: 3 2 1
    Output: 3 2 1
    
    Example 2:
    Input:
        Stack: 11 2 32 3 41
    Output: 41 32 11 3 2
    
    Expected Time Complexity: O(N*N)
    Expected Auxilliary Space: O(N) recursive.
    
    Constraints:
        1<=N<=100
 */

import java.util.*;

class Solution {

    static Stack<Integer> sort(Stack<Integer> s) {

        //Collections.sort(s);
        //return s;

        ArrayList<Integer> al = new ArrayList<>(s);

        for(int i = 0; i < al.size()-1; i++) {
            int k = 0;
            for(int j = 0; j < al.size()-i-1; j++) {

                if(al.get(j) > al.get(j+1)) {

                    int temp = al.get(j);
                    al.set(k, al.get(j+1));
                    al.set(k+1, temp);
                    k++;
                }
            }
        }
        s = new Stack<>();
        for(int i = 0; i < al.size(); i++)
            s.push(al.get(i));

        return s;
    }

    public static void main(String[] args) {

        Stack<Integer> s = new Stack<>();

        s.push(3);
        s.push(2);
        s.push(1);

        System.out.println(s);
        s = sort(s);
        System.out.println(s);
    }
}