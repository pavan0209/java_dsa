/*
    2. Implement stack using array
        
        Write a program to implement a Stack using Array. Your task is to use the class as shown in the comments
    in the code editor and complete the functions push() and pop() to implement a stack.
    
    Example 1:
    Input:
        push(2)
        push(3)
        pop()
        push(4)
        pop()
    Output: 3, 4
    Explanation:
        push(2) the stack will be {2}
        push(3) the stack will be {2 3}
        pop() poped element will be 3,
        the stack will be {2}
        push(4) the stack will be {2 4}
        pop() poped element will be 4
    
    Example 2:
    Input:
        pop()
        push(4)
        push(5)
        pop()
    Output: -1, 5
    
    Expected Time Complexity : O(1) for both push() and pop().
    Expected Auixilliary Space : O(1) for both push() and pop().
    
    Constraints:
        1 <= Q <= 100
        1 <= x <= 100
 */

import java.util.Scanner; 

class StackDemo {

    int size;
    int stackArr[];
    int top;

    StackDemo(int size) {
        
        this.size = size;
        stackArr = new int[size];
        top = -1;
    }

    void push(int data) {

        if(top == size-1) {

            System.out.println("\nStack Overflow...!!");
            return;
        }

        top++;
        stackArr[top] = data;
    }

    int flag = 0;
    int pop() {

        if(top == -1) {

            flag = 0;
            return -1;
        }
        
        flag = 1;
        int data = stackArr[top];
        top--;

        return data;
    }

    void printStack() {

        if(top == -1) {

            System.out.println("\nStack is empty...!!");
            return;
        }

        System.out.println();

        for(int i = 0; i <= top; i++)   
            System.out.print(stackArr[i] + "  ");
        
        System.out.println();
    }
}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("\nEnter size for stack :: ");
        int size = sc.nextInt();

        StackDemo stk = new StackDemo(size);

        boolean flag = true;

        do {

            System.out.println("\n1 : push element in stack");
            System.out.println("2 : pop element from stack");
            System.out.println("3 : print stack");
            System.out.println("4 : Exit");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            stk.push(data);
                        }
                        break;

                case 2 :
                        {
                            int ret = stk.pop();

                            if(stk.flag == 0)
                                System.out.println("\nStack Underflow...!!");
                            else
                                System.out.println("\n" + ret + " popped from stack");
                        }
                        break;

                case 3 :
                        stk.printStack();
                        break;

                case 4 :
                        flag = false;
                        break;

                default:
                        System.out.println("\nWrong choice..!!!");
                        break;
            }

        } while(flag);

        sc.close();
    }
}