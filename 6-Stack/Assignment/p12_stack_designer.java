/*
    12. Stack designer

        You are given an array arr of size N. You need to push the elements of the array into a stack and then
    print them while popping.
    
    Example 1:
    Input:
        n = 5
        arr = {1 2 3 4 5}
    Output:
        5 4 3 2 1
    
    Example 2:
    Input:
        n = 7
        arr = {1 6 43 1 2 0 5}
    Output:
        5 0 2 1 43 6 1
    Constraints:
        1 <= Ai <= 107
 */

import java.util.*;

class StackDemo {

    Stack<Integer> _push(ArrayList<Integer> arr, int n) {

        Stack<Integer> s = new Stack<>();

        for (int i = 0; i < n; i++)
            s.push(arr.get(i));

        return s;
    }

    void _pop(Stack<Integer> s) {

        while (!s.empty()) 
            System.out.print(s.pop() + " ");
    }

}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        StackDemo stk = new StackDemo();

        System.out.print("\nHow Many Elements you want to insert in array ?? :: ");
        int size = sc.nextInt();

        ArrayList<Integer> al = new ArrayList<>();

        System.out.println();
        for (int i = 0; i < size; i++) {

            System.out.print("Enter data :: ");
            int data = sc.nextInt();
            al.add(data);
        }

        Stack<Integer> s = stk._push(al, size);
        System.out.println("\nOriginal Stack :: " + s);
        System.out.print("stack while popping :: ");
        stk._pop(s);

        System.out.println();
        sc.close();
    }
}