/*
    11. Reversing the equation
        
        Given a mathematical equation that contains only numbers and +, -, *, /. Print the
    equation in reverse, such that the equation is reversed, but the numbers remain the same.
    It is guaranteed that the given equation is valid, and there are no leading zeros.
    
    Example 1:
    Input:
        S = "20-3+5*2"
    Output: 2*5+3-20
    Explanation: 
        The equation is reversed with numbers remaining the same.

    Example 2:
    Input:
        S = "5+2*56-2/4"
    Output: 4/2-56*2+5
    Explanation: 
        The equation is reversed with numbers remaining the same.

    Expected Time Complexity: O(|S|).
    Expected Auxiliary Space: O(|S|).
    
    Constraints:
        1<=|S|<=105
    
    The string contains only the characters '0' - '9', '+', '-', '*', and '/'.
 */

import java.util.*;

class Solution {

    static String strRev(String str) {

        Stack<String> s = new Stack<>();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {

            char ch = str.charAt(i);
            if (Character.isDigit(ch)) {

                sb.append(ch);
            } 
            else {
                s.push(sb.toString());
                s.push(String.valueOf(ch));
                sb.setLength(0);
            }
        }
        s.push(sb.toString());

        StringBuffer rev = new StringBuffer();
        while (!s.isEmpty())
            rev.append(s.pop());

        return rev.toString();
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        //System.out.print("\nEnter String :: ");
        // String str = sc.nextLine();
        String str = "20-3+5*2";

        System.out.print("\nOriginal String :: " + str);

        System.out.println("\nReversed String :: " + Solution.strRev(str));

        System.out.println();
        sc.close();
    }
}