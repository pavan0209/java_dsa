/*
    7. Special Stack
    
        Design a data-structure SpecialStack that supports all the stack operations like push(),
    pop(), isEmpty(), isFull() and an additional operation getMin() which should return
    minimum element from the SpecialStack. Your task is to complete all the functions, using
    stack data-Structure.
    
    Example 1:
    Input:
        Stack: 18 19 29 15 16
    Output: 15
    Explanation:
        The minimum element of the stack is 15.
    
    Note: The output of the code will be the value returned by getMin() function.
    
    Expected Time Complexity: O(N) for getMin, O(1) for remaining all 4 functions.
    Expected Auxiliary Space: O(1) for all the 5 functions.
    
    Constraints:
        1 ≤ N ≤ 104
 */

import java.util.*;

class Solution {

    static void push(int a, Stack<Integer> s) {
        
        s.push(a);
    }

    static int pop(Stack<Integer> s) {

        if(!s.empty())
            return s.pop();
        else
            return -1;
    }

    static int min(Stack<Integer> s) {
        
        int min = Integer.MAX_VALUE;

        for(int i = 0; i < s.size(); i++) {

            if(s.get(i) < min)
                min = s.get(i);
        }

        return min;
    }

    static boolean isFull(Stack<Integer> s, int n) {
        
        if(s.size() == n)
            return true;
        else 
            return false;
    }

    static boolean isEmpty(Stack<Integer> s) {
        
        return s.empty();
    }

    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        Stack<Integer> s = new Stack<>();
        boolean flag = true;

        do {

            System.out.println("\n1 : push");
            System.out.println("2 : pop");
            System.out.println("3 : min element");
            System.out.println("4 : isFull");
            System.out.println("5 : isEmpty");
            System.out.println("6 : Exit");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1 :
                        {   
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            push(data, s);
                        }
                        break;

                case 2 :
                        System.out.println("\n" + pop(s) + " popped");
                        break;

                case 3 :
                        System.out.println("\nMinimum Element :: " + min(s));
                        break;

                case 4 :
                        System.out.println("\nIs stack full ?? :: " + isFull(s, 5));
                        break;

                case 5 :
                        System.out.println("\nIs stack empty ?? :: " + isEmpty(s));
                        break;

                case 6 :
                        flag = false;
                        break;

                default:
                        System.out.println("\nWrong Choice..!!");
                        break;
            }

        }   while(flag);

        sc.close();
    }
}