/*
    6. Reverse a string using Stack

    You are given a string S, the task is to reverse the string using stack.
    
    Example 1:
    Input: S="GeeksforGeeks"
    Output: skeeGrofskeeG

    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(N)
    
    Constraints:
        1 ≤ length of the string ≤ 100
 */

import java.util.*; 

class Solution {

    static String strRev(String str) {

        Stack<Character> s = new Stack<>();

        for(int i = 0; i < str.length(); i++) 
            s.push(str.charAt(i));

        StringBuffer sb = new StringBuffer("");

        while(!s.empty())
            sb.append(s.pop());

        return sb.toString();
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("\nEnter String :: ");
        //String str = sc.nextLine();
        String str = "5+2*56-2/4";

        System.out.print("\nOriginal String :: " + str);
    
        System.out.println("\nReversed String :: " + Solution.strRev(str));

        System.out.println();
        sc.close();
    }
}