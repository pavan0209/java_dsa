/*
    5. Implement Stack using Linked List
    
        Let's give it a try! You have a linked list and you have to implement the functionalities
    push and pop of stack using this given linked list. Your task is to use the class as shown in
    the comments in the code editor and complete the functions push() and pop() to implement a stack.

    Example 1:
    Input:
        push(2)
        push(3)
        pop()
        push(4)
        pop()
    Output: 3 4
    Explanation:
        push(2) the stack will be {2}
        push(3) the stack will be {2 3}
        pop() poped element will be 3, the stack will be {2}
        push(4) the stack will be {2 4}
        pop() poped element will be 4
    
    Example 2:
    Input:
        pop()
        push(4)
        push(5)
        pop()
    Output: -1 5
    
    Expected Time Complexity: O(1) for both push() and pop().
    Expected Auxiliary Space: O(1) for both push() and pop().
   
    Constraints:
        1 <= Q <= 100
        1 <= x <= 100
 */
import java.util.Scanner;

class Node {

    int data;
    Node next;

    Node(int data) {

        this.data = data;
        next = null;
    }
}

class StackDemo {
    
    Node head = null;

    boolean isEmpty() {

        if(head == null)
            return true;
        else 
            return false;
    }

    void push(int data) {
        
        Node newNode = new Node(data);

        if(isEmpty()) {

            head = newNode;
        }
        else {

            newNode.next = head;
            head = newNode;
        }
    }

    int flag = 0;

    int pop() {

        if(isEmpty()) {

            flag = 0;
            return -1;
        }

        flag = 1;
        int data = head.data;
        head = head.next;
        return data;
    }

    void printStack() {

        if(isEmpty()) {

            System.out.println("\nStack is Empty...!!");
            return;
        }
        else {

            System.out.println();

            Node temp = head;

            while(temp != null) {

                System.out.print(temp.data + "  ");
                temp = temp.next;
            }

            System.out.println();
        }
    }
}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        StackDemo stk = new StackDemo();

        boolean flag = true;

        do {

            System.out.println("\n1 : Push Elements in Stack");
            System.out.println("2 : Pop Element from Stack");
            System.out.println("3 : Print Elements of Stack");
            System.out.println("4 : Exit");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch(choice) {

                case 1 : 
                        {   
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            stk.push(data);
                        }
                        break;

                case 2 : 
                        {
                            int retVal = stk.pop();
                            if(stk.flag == 0) 
                                System.out.println("\nStack UnderFlow...!!");
                            else
                                System.out.println("\n" + retVal + " is popped..");
                        }
                        
                        break;

                case 3 :
                        stk.printStack();
                        break;

                case 4 :
                        flag = false;
                        break;

                default :
                        System.out.println("\nInvalid Choice ..!!");
                        break;
            }

        } while(flag);

        sc.close();
    }
}