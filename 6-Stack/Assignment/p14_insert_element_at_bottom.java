/*
    14. Insert an Element at the Bottom of a Stack

        You are given a stack St of N integers and an element X. You have to insert X at the bottom of the given
    stack.
    
    Example 1:
    Input:
        N = 5
        X = 2
        St = {4,3,2,1,8}
    Output:
        {2,4,3,2,1,8}
    Explanation:
        After insertion of 2, the final stack will be {2,4,3,2,1,8}.
    
    Example 2:
    Input:
        N = 3
        X = 4
        St = {5,3,1}
    Output:
        {4,5,3,1}
    Explanation:
        After insertion of 4, the final stack will be {4,5,3,1}.
    
    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(N)

    Constraints:
        1 <= N <= 105
        1 <= X, Elements of Stack <= 103
    
    Sum of N over all test cases doesn't exceeds 106
 */

import java.util.*; 

class Solution {

    static Stack<Integer> insertAtBottom(Stack<Integer> St, int X) {

        Stack<Integer> temp = new Stack<>();

        while(!St.empty()) 
            temp.push(St.pop());

        St.push(X);
        while(!temp.isEmpty())
            St.push(temp.pop());

        return St;
    }

    public static void main(String[] args) {

        Stack<Integer> st = new Stack<>();

        st.push(5);
        st.push(3);
        st.push(1);

        System.out.println(st);

        st = insertAtBottom(st, 4);

        System.out.println(st);
    }
}