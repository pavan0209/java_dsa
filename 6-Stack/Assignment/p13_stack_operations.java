/*
    13. Stack Operations
    
    This Java module deals with Stacks, Queues, and ArrayLists. We'll perform various operations on them.
    Given a stack of integers and Q queries. The task is to perform operation on stack according to the query.
    
    Note: use push() to add element in the stack, peek() to get topmost element without removal, pop() gives
    topmost element with removal, search() to return position if found else -1.
    
    Input Format:
        First line of input contains nubmer of testcases T. For each testcase, first line of input contains Q,
    number of queries. Next line contains Q queries seperated by space. Queries are as:
    i x : (adds element x in the stack).
    r : (returns and removes the topmost element from the stack).
    h : Prints the topmost element.
    f y : (check if the element y is present or not in the stack). Print "Yes" if present, else "No".
    
    Output Format:
        For each testcase, perform Q queries and print the output wherever required.
    
    Constraints:
        1 <= T <= 100
        1 <= Q <= 103
    
    Example:
    Input:
        2
        6
        i 2 i 4 i 3 i 5 h f 8
        4
        i 3 i 4 r f 3
    Output:
        5
        No
        Yes
    Explanation:
        Testcase 1: Inserting 2, 4, 3, and 5 onto the stack. Returning top element which is 5.
        Finding 8 will give No, as 8 is not in the stack.
 */

import java.util.*;

class StackDemo {

    void insert(Stack<Integer> st, int x) {

        st.push(x);
    }

    void remove(Stack<Integer> st) {

        int x = st.pop();
        System.out.println(x);
    }

    void headOf_Stack(Stack<Integer> st) {

        int x = st.peek();
        System.out.println(x + " ");
    }

    void find(Stack<Integer> st, int val) {

        if (st.contains(val))
            System.out.println("Yes");
        else
            System.out.println("No");
    }
}

class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        StackDemo stk = new StackDemo();
        Stack<Integer> s = new Stack<>();

        boolean flag = true;

        do {

            System.out.println("\ni :: add element in stack");
            System.out.println("r :: remove topmost element from stack");
            System.out.println("h :: print topmost element");
            System.out.println("f :: check if y is present in stack or not");
            System.out.println("e :: Exit");

            System.out.print("\nEnter your choice :: ");
            char choice = sc.next().charAt(0);

            switch (choice) {

                case 'i': 
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            stk.insert(s, data);
                        }
                        break;

                case 'r':
                        stk.remove(s);
                        break;

                case 'h':
                        stk.headOf_Stack(s);
                        break;

                case 'f': 
                        {
                            System.out.print("\nEnter data to find :: ");
                            int data = sc.nextInt();

                            stk.find(s, data);
                        }
                        break;

                case 'e' :
                        flag = false;
                        break;

                default:
                    System.out.println("\nWrong Choice...!!!");
                    break;
            }
        } while (flag);

        sc.close();
    }
}