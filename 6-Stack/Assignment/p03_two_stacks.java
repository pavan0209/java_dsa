/*
    3. Implement two stacks in an array

    Your task is to implement 2 stacks in one array efficiently. You need to implement 4 methods.

    push1 : pushes element into first stack.
    push2 : pushes element into second stack.
    pop1 : pops element from first stack and returns the popped element. If first stack is empty, it should
    return -1.
    pop2 : pops element from second stack and returns the popped element. If second stack is empty, it should
    return -1.
    
    Example 1:
    Input:
        push1(2)
        push1(3)
        push2(4)
        pop1()
        pop2()
        pop2()
    Output:
        3 4 -1
    Explanation:
        push1(2) the stack1 will be {2}
        push1(3) the stack1 will be {2,3}
        push2(4) the stack2 will be {4}
        pop1() the poped element will be 3 from stack1 and stack1 will be {2}
        pop2() the poped element will be 4 from stack2 and now stack2 is empty
        pop2() the stack2 is now empty hence returned -1.
    
    Example 2:
        Input:
        push1(1)
        push2(2)
        pop1()
        push1(3)
        pop1()
        pop1()
    Output:
        1 3 -1
    Explanation:
        push1(1) the stack1 will be {1}
        push2(2) the stack2 will be {2}
        pop1() the poped element will be 1 from stack1 and stack1 will be empty
        push1(3) the stack1 will be {3}
        pop1() the poped element will be 3 from stack1 and stack1 will be empty
        pop1() the stack1 is now empty hence returned -1.
        
    Expected Time Complexity: O(1) for all the four methods.
    Expected Auxiliary Space: O(1) for all the four methods.
    
    Constraints:
        1 <= Number of queries <= 104
        1 <= Number of elements in the stack <= 100
    
    The sum of count of elements in both the stacks < size of the given array
 */

import java.util.Scanner; 

class StackDemo {

    int size;
    int stackArr[];
    int top1;
    int top2;

    StackDemo(int size) {
        
        this.size = size;
        stackArr = new int[size];
        top1 = -1;
        top2 = size;
    }

    void push1(int data) {

        if(top2 - top1 > 1) {

            top1++;
            stackArr[top1] = data;
        }
        else {

            System.out.println("\nStack Overflow...!!");
        }
    }

    void push2(int data) {

        if(top2 - top1 > 1) {

            top2--;
            stackArr[top2] = data;
        }
        else {

            System.out.println("\nStack Overflow...!!");
        }
    }

    int flag1 = 0;
    int pop1() {

        if(top1 == -1) {

            flag1 = 0;
            return -1;
        }
        
        flag1 = 1;
        int data = stackArr[top1];
        top1--;

        return data;
    }

    int flag2 = 0;
    int pop2() {

        if(top2 == size) {

            flag2 = 0;
            return -1;
        }
        
        flag2 = 1;
        int data = stackArr[top2];
        top2++;

        return data;
    }

    void printStack1() {

        if(top1 == -1) {

            System.out.println("\nStack is empty...!!");
            return;
        }

        System.out.println();

        for(int i = 0; i <= top1; i++)   
            System.out.print(stackArr[i] + "  ");
        
        System.out.println();
    }

    void printStack2() {

        if(top2 == size) {

            System.out.println("\nStack is empty...!!");
            return;
        }

        System.out.println();

        for(int i = size-1; i >= top2; i--)   
            System.out.print(stackArr[i] + "  ");
        
        System.out.println();
    }
}

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("\nEnter size for stack :: ");
        int size = sc.nextInt();

        StackDemo stk = new StackDemo(size);

        boolean flag = true;

        do {

            System.out.println("\n1 : push element in stack 1");
            System.out.println("2 : push element in stack 2");
            System.out.println("3 : pop element from stack 1");
            System.out.println("4 : pop element from stack 2");
            System.out.println("5 : print stack 1");
            System.out.println("6 : print stack 2");
            System.out.println("7 : Exit");

            System.out.print("\nEnter your choice :: ");
            int choice = sc.nextInt();

            switch (choice) {

                case 1 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            stk.push1(data);
                        }
                        break;

                case 2 :
                        {
                            System.out.print("\nEnter data :: ");
                            int data = sc.nextInt();

                            stk.push2(data);
                        }
                        break;

                case 3 :
                        {
                            int ret = stk.pop1();

                            if(stk.flag1 == 0)
                                System.out.println("\nStack Underflow...!!");
                            else
                                System.out.println("\n" + ret + " popped from stack");
                        }
                        break;

                case 4 :
                        {
                            int ret = stk.pop2();

                            if(stk.flag2 == 0)
                                System.out.println("\nStack Underflow...!!");
                            else
                                System.out.println("\n" + ret + " popped from stack");
                        }
                        break;

                case 5 :
                        stk.printStack1();
                        break;

                case 6 :
                        stk.printStack2();
                        break;

                case 7 :
                        flag = false;
                        break;

                default:
                        System.out.println("\nWrong choice..!!!");
                        break;
            }

        } while(flag);

        sc.close();
    }
}