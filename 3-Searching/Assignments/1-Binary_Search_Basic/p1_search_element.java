/*
 *
 */

class Solution {

    static int searchElement_Recursion(int arr[], int start, int end, int key) {

        if (start > end)
            return -1;

        int mid = (start + end) / 2;

        if (arr[mid] == key)
            return mid;
        else if (arr[mid] > key)
            end = mid - 1;
        else
            start = mid + 1;

        return searchElement_Recursion(arr, start, end, key);
    }

    static int searchElement_Loop(int arr[], int key) {

        int start = 0;
        int end = arr.length - 1;

        while (start <= end) {

            int mid = (start + end) / 2;

            if (arr[mid] == key)
                return mid;
            else if (arr[mid] > key)
                end = mid - 1;
            else
                start = mid + 1;
        }

        return -1;
    }

    public static void main(String[] args) {

        int arr[] = new int[] { 1, 2, 3, 4, 5 };

        System.out.println(Solution.searchElement_Loop(arr, 1));
        System.out.println(Solution.searchElement_Recursion(arr, 0, arr.length-1,4 ));
    }
}