/*
 *
 */

class Solution {

    static int start = 0;
    static int end = 0;
    static int mid = 0;
    
    static int closestElement(int arr[], int target) {

        if(start > end) 
            return (int)Math.min(arr[mid], arr[mid+1]);

        mid = (start+end ) / 2;

        if(arr[mid] == target)
            return arr[mid];
        else if (arr[mid] > target)
            end = mid - 1;
        else 
            start = mid + 1;

        return closestElement(arr, target);
    }

    public static void main(String[] args) {

        int arr[] = new int[] {1,2,4,7,9};

        start = 0;
        end = arr.length-1;

        System.out.println(Solution.closestElement(arr, 5));
    }
}