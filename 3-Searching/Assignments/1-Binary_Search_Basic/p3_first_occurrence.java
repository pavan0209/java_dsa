/*
 *
 */

class Solution {

    static int start = 0;
    static int end = 0;

    static int findFirst(int arr[], int idx) {

        if (idx <= 0 || arr[idx] != arr[idx - 1])
            return idx;

        return findFirst(arr, idx - 1);
    }

    static int firstOccurrence(int arr[], int target) {

        if (start > end)
            return -1;

        int mid = (start + end) / 2;

        if (arr[mid] == target)
            return findFirst(arr, mid);
        else if (arr[mid] > target)
            end = mid - 1;
        else
            start = mid + 1;

        return firstOccurrence(arr, target);
    }

    public static void main(String[] args) {

        int arr[] = new int[] { 1, 2, 3, 4, 6, 7, 8 };

        start = 0;
        end = arr.length - 1;

        System.out.println(Solution.firstOccurrence(arr, 4));
    }
}