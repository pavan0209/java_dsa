/*
 *
 */

class Solution {

    static int start = 0;
    static int end = 0;

    static int findLast(int arr[], int idx) {

        if (idx >= arr.length || arr[idx] != arr[idx + 1])
            return idx;

        return findLast(arr, idx + 1);
    }

    static int lastOccurrence(int arr[], int target) {

        if (start > end)
            return -1;

        int mid = (start + end) / 2;

        if (arr[mid] == target)
            return findLast(arr, mid);
        else if (arr[mid] > target)
            end = mid - 1;
        else
            start = mid + 1;

        return lastOccurrence(arr, target);
    }

    public static void main(String[] args) {

        int arr[] = new int[] { 2, 4, 4, 4, 4, 7, 8 };

        start = 0;
        end = arr.length - 1;

        System.out.println(Solution.lastOccurrence(arr, 4));
    }
}