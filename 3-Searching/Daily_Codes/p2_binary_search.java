/*
    Binary Search
 */

class Solution {

    static int binarySearch(int arr[], int key) {

        int start = 0;
        int end = arr.length - 1;

        while (start <= end) {

            int mid = (start + end) / 2;

            if (arr[mid] == key)
                return mid;
            else if (arr[mid] > key)
                end = mid - 1;
            else
                start = mid + 1;
        }

        return -1;
    }

    public static void main(String[] args) {

        int arr[] = { 4, 7, 11, 24, 35, 57, 75, 87 };

        int ret = binarySearch(arr, 87);

        if (ret == -1)
            System.out.println("Element Not Found");
        else
            System.out.println("Element found at index :: " + ret);
    }
}