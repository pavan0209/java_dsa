/*  
 *  Linear Search
 */

class Solution {

    static int linearSearch(int arr[], int key) {

        for(int i = 0; i < arr.length; i++) {

            if(arr[i] == key)
                return i;
        }

        return -1;
    }

    public static void main(String[] args) {

        int arr[] = {1,2,3,4,5};

        int ret = linearSearch(arr, 3);

        if(ret == -1)
            System.out.println("Element Not Found");
        else
            System.out.println("Element found at index :: " + ret);
    }
}