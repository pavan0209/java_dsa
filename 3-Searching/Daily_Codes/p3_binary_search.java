/*
    Binary Search
 */

class Solution {

    static int binarySearch(int arr[], int key, int start, int end) {

        if(start > end)
            return -1;

            int mid = (start + end) / 2;

            if (arr[mid] == key)
                return mid;
            else if (arr[mid] > key)
                end = mid - 1;
            else
                start = mid + 1;
        

        return binarySearch(arr, key, start, end);
    }

    public static void main(String[] args) {

        int arr[] = { 4, 7, 11, 24, 35, 57, 75, 87 };

        int ret = binarySearch(arr, 87, 0, arr.length-1);

        if (ret == -1)
            System.out.println("Element Not Found");
        else
            System.out.println("Element found at index :: " + ret);
    }
}