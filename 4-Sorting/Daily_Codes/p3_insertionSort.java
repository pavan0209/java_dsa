/*
 *
 */

class Solution {

    static int count = 0;

    static void insertionSort(int arr[], int N) {

        for(int i = 1; i < N; i++) {

            count++;
            int element = arr[i];
            int j = i - 1;

            while (j >= 0 && arr[j] > element) {
                
                arr[j+1] = arr[j];
                j--;
            }

            arr[j+1] = element;
        }
    }

    public static void main(String[] args) {

        int arr[] = { 8, 3, 1, 7, 5, 4, 2 };

        System.out.print("Array before sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        Solution.insertionSort(arr, arr.length);

        System.out.print("\nArray  after sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        System.out.println();
        System.out.println(count);
    }
}