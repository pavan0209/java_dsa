/*
 *  Insertion Sort Recursion
 */

class Solution {

    static void insertionSort(int arr[], int i) {

        if(i == arr.length )
            return;

        int element = arr[i];
        int j = i - 1;

        while (j >= 0 && arr[j] > element) {
            
            arr[j+1] = arr[j];
            j--;
        }

        arr[j+1] = element;
        insertionSort(arr, i+1);
    }
    public static void main(String[] args) {
        int arr[] = { 8, 3, 1, 7, 5, 4, 2 };

        System.out.print("Array before sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        Solution.insertionSort(arr, 1);

        System.out.print("\nArray  after sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        System.out.println();
    }
}