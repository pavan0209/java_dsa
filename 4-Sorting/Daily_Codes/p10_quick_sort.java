/*
 *
 */

class Solution {

    static void swap(int arr[], int i, int j) {

        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static int partition(int arr[], int start, int end) {

        int i = start - 1;
        int pivot = arr[end];

        for(int j = start; j < end; j++) {

            if(arr[j] < pivot) {

                i++;
                swap(arr, i, j);
            }
        }

        i++;
        swap(arr, i, end);

        return i;
    }

    static void quickSort(int arr[], int start, int end) {

        if(start < end) {

            int pivotIdx = partition(arr, start, end);
            
            quickSort(arr, start, pivotIdx-1);
            quickSort(arr, pivotIdx+1, end);
        }
    }

    public static void main(String[] args) {

        int arr[] = { 9, 2, 7, 3, 8, 4, 1, 6 };

        System.out.print("Array before sort :: ");
        System.out.println(java.util.Arrays.toString(arr));

        Solution.quickSort(arr, 0, arr.length-1);
        
        System.out.print("Array after sort :: ");
        System.out.println(java.util.Arrays.toString(arr));
    }
}