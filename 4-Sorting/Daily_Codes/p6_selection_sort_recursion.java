/*
 *
 */

class Solution {

    static void selectionSort(int arr[], int i, int N) {

        int minIdx = i;

        for (int j = i + 1; j < N; j++) {

            if (arr[j] < arr[minIdx])
                minIdx = j;
        }

        if (minIdx != i) {

            int temp = arr[i];
            arr[i] = arr[minIdx];
            arr[minIdx] = temp;
        }

        selectionSort(arr, i + 1, N);
    }

    public static void main(String[] args) {

        int arr[] = { 9, 2, 7, 3, 8, 4, 1, 6 };

        System.out.print("Array before sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        Solution.selectionSort(arr, 0, arr.length);

        System.out.print("\nArray  after sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        System.out.println();
    }
}