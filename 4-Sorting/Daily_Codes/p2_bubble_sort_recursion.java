/*
 *
 */

class Solution {

    static int count = 0;
    
    static void bubbleSort(int arr[], int N) {

        if(N == 1)
            return;

        boolean swapped = false;

        for(int j = 0; j < N-1; j++) {

            count++;
            if(arr[j] > arr[j+1]) {

                int temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
                swapped = true;
            }
        }
        if(swapped == false)
            return;

        bubbleSort(arr, N-1);
    }

    public static void main(String[] args) {

        int arr[] = { 7, 3, 9, 4, 2, 5, 6 };
        //int arr[] = {2,  3,  4,  5,  6,  9,  7 };

        System.out.print("Array before sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        Solution.bubbleSort(arr, arr.length);

        System.out.print("\nArray  after sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        System.out.println();
        System.out.println("Total Iterations :: " + count);
    }
}