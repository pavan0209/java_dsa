/*
 *  Bubble Sort 
 */

class Solution {

    //Approach 1: BurteForce
    /*static void bubbleSort(int arr[], int N) {

        for (int i = 0; i < N; i++) {

            for (int j = 0; j < N - 1; j++) {

                if (arr[j] > arr[j + 1]) {

                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }*/
    
    //Approach-2 :
    /*static int count = 0;
    static void bubbleSort(int arr[], int N) {

        for (int i = 0; i < N; i++) {

            for (int j = 0; j < N - i - 1; j++) {

                count++;

                if (arr[j] > arr[j + 1]) {

                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }*/

    //Approach 3 : Optimized
    static int count = 0;
    static void bubbleSort(int arr[], int N) {

        for (int i = 0; i < N; i++) {
            int swapped = 0;

            for (int j = 0; j < N - i - 1; j++) {
                count++;

                if (arr[j] > arr[j + 1]) {

                    swapped = 1;

                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
            if(swapped == 0)
                return;
        }
    }

    public static void main(String[] args) {

        //int arr[] = { 7, 3, 9, 4, 2, 5, 6 };
        int arr[] = {2,  3,  4,  5,  6,  9,  7 };

        System.out.print("Array before sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        Solution.bubbleSort(arr, arr.length);

        System.out.print("\nArray  after sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        System.out.println();
        System.out.println("Total Iterations :: " + count);
    }
}