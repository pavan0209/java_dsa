/*
 *
 */

class Solution {

    static void merge(int arr[], int start, int mid, int end) {

        int ele1 = mid - start + 1;
        int ele2 = end - mid;

        int arr1[] = new int[ele1];
        int arr2[] = new int[ele2];

        for (int i = 0; i < ele1; i++)
            arr1[i] = arr[start + i];

        for (int i = 0; i < ele2; i++)
            arr2[i] = arr[mid + i + 1];

        int itr1 = 0;
        int itr2 = 0;
        int itr3 = start;

        while (itr1 < ele1 && itr2 < ele2) {

            if (arr1[itr1] < arr2[itr2]) {
                arr[itr3] = arr1[itr1];
                itr1++;
            } else {

                arr[itr3] = arr2[itr2];
                itr2++;
            }
            itr3++;
        }

        while (itr1 < ele1) {

            arr[itr3] = arr1[itr1];
            itr1++;
            itr3++;
        }

        while (itr2 < ele2) {

            arr[itr3] = arr2[itr2];
            itr2++;
            itr3++;
        }
    }

    static void mergeSort(int arr[], int start, int end) {

        if (start < end) {

            int mid = (start + end) / 2;

            mergeSort(arr, start, mid);
            mergeSort(arr, mid+1, end);

            merge(arr, start, mid, end);
        }
    }

    public static void main(String[] args) {

        int arr[] = { 8, 3, 1, 7, 5, 4, 2 };

        System.out.print("Array before sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        Solution.mergeSort(arr, 0, arr.length-1);

        System.out.print("\nArray  after sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        System.out.println();
    }
}