/*
 *
 */

class Solution {

    static void mSort(int arr[], int start, int mid, int end) {

        int n1 = mid - start + 1;
        int n2 = end - mid;

        int arr1[] = new int[n1];
        int arr2[] = new int[n2];

        for(int i = 0; i < n1; i++) 
            arr1[i] = arr[start+i];

        for(int i = 0; i < n2; i++)
            arr2[i] = arr[mid+i+1];

        int i = 0;
        int j = 0;
        int k = start;
        
        while(i < n1 && j < n2) {

            if(arr1[i] < arr2[j]) 
                arr[k] = arr1[i++];
            else 
                arr[k] = arr2[j++];
            k++;
        }

        while(i < n1) {

            arr[k] = arr1[i];
            i++;
            k++;
        }

        while (j < n2) {
            
            arr[k] = arr2[j];
            j++;
            k++;
        }
    }

    static void mergeSort(int arr[], int start, int end) {

        if(start < end) {

            int mid = (start + end) / 2;

            mergeSort(arr, start, mid);
            mergeSort(arr, mid+1, end);

            mSort(arr, start, mid, end);
        }
    }

    public static void main(String[] args) {

        int arr[] = { 9, 2, 7, 3, 8, 4, 1, 6 };

        System.out.print("Array before sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        Solution.mergeSort(arr, 0, arr.length-1);

        System.out.print("\nArray  after sort :: ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + "  ");

        System.out.println();
    }
}