/*
    Problem Statement 3:
    
        Given two sorted arrays arr1[] and arr2[] of sizes n and m in non-decreasing order. Merge them in sorted
    order without using any extra space. Modify arr1 so that it contains the first N elements and modify arr2 so
    that it contains the last M elements.
    
    Example 1:
    Input:
        n = 4, arr1[] = [1 3 5 7]
        m = 5, arr2[] = [0 2 6 8 9]
    Output:
        arr1[] = [0 1 2 3]
        arr2[] = [5 6 7 8 9]
    Explanation:
        After merging the two non-decreasing arrays, we get, 0 1 2 3 5 6 7 8 9.
    
    Example 2:
    Input:
        n = 2, arr1[] = [10 12]
        m = 3, arr2[] = [5 18 20]
    Output:
        arr1[] = [5 10]
        arr2[] = [12 18 20]
    Explanation:
        After merging two sorted arrays we get 5 10 12 18 20.
    
    Expected Time Complexity: O((n+m) log(n+m))
    Expected Auxiliary Space: O(1)
    
    Constraints:
        1 <= n, m <= 10^5
        0 <= arr1i, arr2i <= 10^7
 */

class Solution {

    static void merge(long arr1[], long arr2[], int n, int m) {

        long arr3[] = new long[n + m];
        int i = 0;
        int j = 0;
        int k = 0;

        while (i < n && j < m) {

            if (arr1[i] < arr2[j]) {

                arr3[k] = arr1[i];
                i++;
            } else {

                arr3[k] = arr2[j];
                j++;
            }
            k++;
        }

        while (i < n) {
            arr3[k] = arr1[i];
            i++;
            k++;
        }

        while (j < m) {
            arr3[k] = arr2[j];
            j++;
            k++;
        }

        for(int itr = 0; itr < n; itr++)
            arr1[itr] = arr3[itr];
            
        for(int itr = 0; itr < m; itr++) 
            arr2[itr] = arr3[n+itr];
    }

    public static void main(String[] args) {

        long arr1[] = { 10, 12 };
        long arr2[] = { 5, 18, 20 };

        merge(arr1, arr2, arr1.length, arr2.length);

        System.out.println(java.util.Arrays.toString(arr1));
        System.out.println(java.util.Arrays.toString(arr2));
    }
}