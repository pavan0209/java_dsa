/*
    Problem Statement 15:
    
    Given a random set of numbers, Print them in sorted order.
    Use : merge and quicksort
    
    Example 1:
    Input:
    N = 4
    arr[] = {1, 5, 3, 2}
    Output: {1, 2, 3, 5}
    Explanation: After sorting array will be like {1, 2, 3, 5}.
    
    Example 2:
    Input:
    N = 2
    arr[] = {3, 1}
    Output: {1, 3}
    Explanation: After sorting array will be like {1, 3}.
    
    Expected Time Complexity: O(N * log N)
    Expected Auxiliary Space: O(1)
    
    Constraints:
        1 ≤ N, A[i] ≤ 105
 */

import java.util.Arrays;

class Solution {

    static void merge(int arr[], int start, int end, int mid) {

        int n1 = mid - start + 1;
        int n2 = end - mid;

        int arr1[] = new int[n1];
        int arr2[] = new int[n2];

        for (int i = 0; i < n1; i++)
            arr1[i] = arr[start + i];

        for (int i = 0; i < n2; i++)
            arr2[i] = arr[mid + i + 1];

        int i = 0, j = 0, k = start;

        while (i < n1 && j < n2) {

            if (arr1[i] < arr2[j]) {

                arr[k] = arr1[i];
                i++;
            } else {

                arr[k] = arr2[j];
                j++;
            }
            k++;
        }

        while (i < n1) {

            arr[k] = arr1[i];
            i++;
            k++;
        }

        while (j < n2) {

            arr[k] = arr2[j];
            j++;
            k++;
        }
    }

    static void mergeSort(int arr[], int start, int end) {

        if (start < end) {

            int mid = (start + end) / 2;

            mergeSort(arr, start, mid);
            mergeSort(arr, mid + 1, end);

            merge(arr, start, end, mid);
        }
    }

    static void swap(int arr[], int i, int j) {

        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static int partition(int arr[], int start, int end) {

        int i = start - 1;
        int pivot = arr[end];

        for (int j = start; j < end; j++) {

            if (arr[j] < pivot) {
                i++;
                swap(arr, i, j);
            }
        }

        i++;
        swap(arr, i, end);

        return i;
    }

    static void quickSort(int arr[], int start, int end) {

        if (start < end) {

            int pivotIdx = partition(arr, start, end);

            quickSort(arr, start, pivotIdx - 1);
            quickSort(arr, pivotIdx + 1, end);
        }
    }

    public static void main(String[] args) {

        int arr[] = { 1, 5, 3, 2 };

        System.out.println("Before :: " + Arrays.toString(arr));
        mergeSort(arr, 0, arr.length - 1);
        System.out.println("After :: " + Arrays.toString(arr));

        int arr1[] = { 3, 1 };

        System.out.println("Before :: " + Arrays.toString(arr1));
        quickSort(arr1, 0, arr1.length - 1);
        System.out.println("After :: " + Arrays.toString(arr1));

    }
}