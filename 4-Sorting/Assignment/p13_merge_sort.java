/*
    Problem Statement 13:
    
    Given an array of integers nums, sort the array in ascending order and return it.
    You must solve the problem without using any built-in functions in O(nlog(n))
    time complexity and with the smallest space complexity possible. (Solve using merge sort)

    Example 1:
    Input: nums = [5,2,3,1]
    Output: [1,2,3,5]
    Explanation: After sorting the array, the positions of some numbers are not
    changed (for example, 2 and 3), while the positions of other numbers are changed (for example, 1 and 5).
    
    Example 2:
    Input: nums = [5,1,1,2,0,0]
    Output: [0,0,1,1,2,5]
    Explanation: Note that the values of nums are not necessarily unique.

    Constraints:
        1 <= nums.length <= 5 * 10^4
        -5 * 104 <= nums[i] <= 5 * 10^4
*/

class Solution {

    static void merge(int arr[], int start, int end, int mid) {

        int n1 = mid - start + 1;
        int n2 = end - mid;

        int arr1[] = new int[n1];
        int arr2[] = new int[n2];

        for (int i = 0; i < n1; i++)
            arr1[i] = arr[start + i];

        for (int i = 0; i < n2; i++)
            arr2[i] = arr[mid + i + 1];

        int i = 0, j = 0, k = start;

        while (i < n1 && j < n2) {

            if (arr1[i] < arr2[j]) {
                arr[k] = arr1[i];
                i++;
            } else {
                arr[k] = arr2[j];
                j++;
            }
            k++;
        }

        while (i < n1) {
            arr[k] = arr1[i];
            i++;
            k++;
        }

        while (j < n2) {
            arr[k] = arr2[j];
            j++;
            k++;
        }
    }

    static void mergeSort(int arr[], int start, int end) {

        if (start < end) {

            int mid = (start + end) / 2;

            mergeSort(arr, start, mid);
            mergeSort(arr, mid + 1, end);
            merge(arr, start, end, mid);
        }
    }

    public static void main(String[] args) {

        int arr[] = { 5,2,3,1 };

        System.out.println(java.util.Arrays.toString(arr));

        mergeSort(arr, 0, arr.length - 1);

        System.out.println(java.util.Arrays.toString(arr));
    }
}