/*
    Problem Statement 5:
    
        Given an array arr[], its starting position l and its ending position r. Sort the array using the
    Merge Sort algorithm.
    
    Example 1:
    Input:
        N = 5
        arr[] = {4 1 3 9 7}
    Output:
        1 3 4 7 9
    
    Example 2:
    Input:
        N = 10
        arr[] = {10 9 8 7 6 5 4 3 2 1}
    Output:
        1 2 3 4 5 6 7 8 9 10
    
    Expected Time Complexity: O(nlogn)
    Expected Auxiliary Space: O(n)

    Constraints:
        1 <= N <= 10^5
        1 <= arr[i] <= 10^5
 */

class Solution {

    static void merge(int arr[], int start, int end, int mid) {

        int n1 = mid - start + 1;
        int n2 = end - mid;

        int arr1[] = new int[n1];
        int arr2[] = new int[n2];

        for (int i = 0; i < n1; i++)
            arr1[i] = arr[start + i];

        for (int i = 0; i < n2; i++)
            arr2[i] = arr[mid + i + 1];

        int i = 0, j = 0, k = start;

        while (i < n1 && j < n2) {

            if (arr1[i] < arr2[j]) {
                arr[k] = arr1[i];
                i++;
            } else {
                arr[k] = arr2[j];
                j++;
            }
            k++;
        }

        while (i < n1) {
            arr[k] = arr1[i];
            i++;
            k++;
        }

        while (j < n2) {
            arr[k] = arr2[j];
            j++;
            k++;
        }
    }

    static void mergeSort(int arr[], int start, int end) {

        if (start < end) {

            int mid = (start + end) / 2;

            mergeSort(arr, start, mid);
            mergeSort(arr, mid + 1, end);
            merge(arr, start, end, mid);
        }
    }

    public static void main(String[] args) {

        int arr[] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };

        System.out.println(java.util.Arrays.toString(arr));

        mergeSort(arr, 0, arr.length - 1);

        System.out.println(java.util.Arrays.toString(arr));
    }
}