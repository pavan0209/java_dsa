/*
    Problem Statement 8:
    
    Given an array of integers nums, sort the array in ascending order and return it. You must solve the problem
    without using any built-in functions in O(nlog(n))time complexity and with the smallest space complexity
    possible.

    Example 1:
    Input: nums = [5,2,3,1]
    Output: [1,2,3,5]
    Explanation: After sorting the array, the positions of some numbers are not
    changed (for example, 2 and 3), while the positions of other numbers are changed (for example, 1 and 5).
    
    Example 2:
    Input: nums = [5,1,1,2,0,0]
    Output: [0,0,1,1,2,5]
    Explanation: Note that the values of nums are not necessarily unique.

    Constraints:
        1 <= nums.length <= 5 * 10^4
        -5 * 104 <= nums[i] <= 5 * 10^4
 */

import java.util.Arrays;

class Solution {

    static void swap(int arr[], int i, int j) {

        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static int partition(int arr[], int start, int end) {

        int i = start - 1;
        int pivot = arr[end];

        for (int j = start; j < end; j++) {

            if (arr[j] < pivot) {

                i++;
                swap(arr, i, j);
            }
        }

        i++;
        swap(arr, i, end);

        return i;
    }

    static void quickSort(int arr[], int start, int end) {

        if (start < end) {

            int pivotIdx = partition(arr, start, end);

            quickSort(arr, start, pivotIdx - 1);
            quickSort(arr, pivotIdx + 1, end);
        }
    }

    public static void main(String[] args) {

        int nums[] = { 5, 2, 3, 1 };

        System.out.println(Arrays.toString(nums));

        quickSort(nums, 0, nums.length - 1);

        System.out.println(Arrays.toString(nums));
    }
}