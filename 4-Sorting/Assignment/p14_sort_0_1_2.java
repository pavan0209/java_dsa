/*
    Problem Statement 14:
    
        Given an array of size N containing only 0s, 1s, and 2s; sort the array in ascending order.
    (sort using : Quicksort algorithm)
    
    Example 1:
    Input:
        N = 5
        arr[]= {0 2 1 2 0}
    Output:
        0 0 1 2 2
    Explanation:
    0s 1s and 2s are segregated into ascending order.
   
    Example 2:
    Input:
        N = 3
        arr[] = {0 1 0}
    Output:
        0 0 1
    Explanation:
    0s 1s and 2s are segregated into ascending order.

    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)

    Constraints:
        1 <= N <= 10^6
        0 <= A[i] <= 2
*/

import java.util.Arrays;

class Solution {

    static void swap(int arr[], int i, int j) {

        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static int partition(int arr[], int start, int end) {

        int i = start - 1;
        int pivot = arr[end];

        for (int j = start; j < end; j++) {

            if (arr[j] < pivot) {
                i++;
                swap(arr, i, j);
            }
        }

        i++;
        swap(arr, i, end);

        return i;
    }

    static void quickSort(int arr[], int start, int end) {

        if (start < end) {

            int pivotIdx = partition(arr, start, end);

            quickSort(arr, start, pivotIdx - 1);
            quickSort(arr, pivotIdx + 1, end);
        }
    }

    public static void main(String[] args) {

        int arr[] = { 0, 2, 1, 2, 0 };

        System.out.println("Before :: " + Arrays.toString(arr));
        quickSort(arr, 0, arr.length - 1);
        System.out.println("After :: " + Arrays.toString(arr));

    }
}