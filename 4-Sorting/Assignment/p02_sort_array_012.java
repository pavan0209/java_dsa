/*
    Problem Statement 2:
        
    Given an array of size N containing only 0s, 1s, and 2s; sort the array in ascending order.
    
    Example 1:
    Input:
        N = 5
        arr[]= {0 2 1 2 0}
    Output:
        0 0 1 2 2
    Explanation:
        0s 1s and 2s are segregated into ascending order.
    
    Example 2:
    Input:
        N = 3
        arr[] = {0 1 0}
    Output:
        0 0 1
    Explanation:
        0s 1s and 2s are segregated into ascending order.
    
    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)
 */

import java.util.Arrays;

class Solution {

    static void sort012(int arr[], int n) {

        int temp[] = { 0, 0, 0 };

        for (int i = 0; i < n; i++)
            temp[arr[i]]++;

        int itr = 0;
        int i = 0;
        while (i < temp.length && itr < arr.length) {

            if (temp[i] == 0)
                i++;

            arr[itr] = i;
            temp[i]--;
            itr++;
        }
    }

    public static void main(String[] args) {

        int arr[] = { 2,2,1,1,0,0};

        System.out.println(Arrays.toString(arr));
        sort012(arr, arr.length);
        System.out.println(Arrays.toString(arr));
    }
}